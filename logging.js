
var npmlog = require('npmlog');
const fs = require('fs-extra');
const file = require('./file.js');


/**
 * logging class based on [npmlog]{@link https://www.npmjs.com/package/npmlog}.
 * 
 * patches unexpected behaviour of npmlog: log of Error object results in duplicated output.
 * see: https://github.com/npm/npmlog/issues/62 and https://github.com/npm/npmlog/blob/master/log.js#L182
 * 
 * Uses fs stream to write logs to logfile
 */

class Log {
  constructor() {
    npmlog.addLevel("debug",1000,{fg:"blue"})
    npmlog.addLevel("trace",500,{fg:"grey"})
    Object.assign(this, npmlog)

    this.error = function(prefix, message, ...args) {
      if(message instanceof Error && message.stack) {
        message = message.stack
      }
      npmlog.error(prefix, message, ...args)
    }
  }
	
  /**
	* initialise log write stream
	*/
  init(log_level, logfile_dir) {
    return new Promise((resolve, reject) => {
      npmlog.level = log_level
      file.mkdir(logfile_dir)
      let date = this.getPrettyDate()
      date = date.replace(/\:/gm, "-").slice(0, -4)
      let logfile = logfile_dir + '/' + date + '.log'
			
      var logstream = fs.createWriteStream(logfile, {flags:'a'})
      logstream.on('open', res => {
        npmlog.on('log', l => {
          if(this.socket) {
            this.socket.emit('log', {level:l.level, topic:l.prefix, message: l.message})
          }
          logstream.write(this.getPrettyDate() + ' ' + l.level.toUpperCase() + ' ' + l.prefix + ' ' + l.message + '\n')
        })
        return resolve("log initialized")
      })
    })
  }

  setSocket(socket) { 
    this.socket = socket
  }
    
  /**
     * change current log level
     * - trace (show everything)
     * - debug
     * - info
     * - warn
     * - error
     * - silent (logging off)
     * @param {string} log_level 
     */
  setLevel(log_level) {
    npmlog.level = log_level
    console.log("log level changed to '" + log_level + "'")
  }
    
  /**
     * return current date time in following string format:
     * [year]-[month]-[day] [hour]-[minute]-[second]
     */
  getPrettyDate() {
    let tzoffset = (new Date()).getTimezoneOffset() * 60000 //offset in milliseconds
    return (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1).replace('T', ' ')
  }

  /**
     * get log functions that prepends context info.
     * context log is itself a function that uses 'debug' log
     * 
     * @param {string} context - log context that is prepended to each log message
     * @returns {Object} - log functions that aut prepend context
     */
  getContextLog(context) {
    let context_log = msg => {
      this.debug(context, msg)
    }

    ['trace', 'debug', 'info', 'warn', 'error'].forEach(log_level => {
      context_log[log_level] = msg => {
        this[log_level](context, msg)
      }
    })

    return context_log
  }
}


module.exports = Log
