/* eslint-disable global-require */
const argv = require('minimist')(process.argv.slice(2));
const cors = require('cors');
const path = require('path');
const express = require('express');
const http = require('http');
const sharp = require("sharp");

const SQL = require('./db/postgresql');
const Log = require('./logging');

const log = new Log();
const file = require('./file');
const fileUpload = require('express-fileupload');

const auth = require('./authentication');

const helpers = require('./helpers');

const { v4: uuidv4 } = require('uuid');
const { readdir } = require('node:fs/promises');

const sql = new SQL();

var activeCaptchas = [];

const {pluginEventHandler} = require('./pluginEventHandler');
var pluginHandler;
var pluginList = []; 
try{
  // load plugins dynamically from plugins dir:
  readdir('./plugins').then((plugins)=>{
    console.log("plugins detected:",plugins);
    for (let plugin of plugins){
      console.log("plugin loading:",plugin);
      const p = require('./plugins/'+plugin+'/index.js')
      pluginList.push(p);
    }
    pluginHandler = new pluginEventHandler(sql,pluginList);
    console.log("plugins initialized");
    console.log("////////////////////////////////////////");
  })
  //const calendarPlugin = require('./plugins/Reservations2gCalendar/index.js'); 
  //pluginHandler = new pluginEventHandler(sql,[calendarPlugin]);
}catch(e){
  log.error("could not instanciate pluginHandler",e)
  pluginHandler={
    triggerPluginEvent:function(){log.warn("pluginHandler inactive. no pluginEvents will be dispatched");}
  }
}

/** ******************************************************************************************
* load config and check options
*
******************************************************************************************** */

/** path to and name of config file. If file is not found config will be created based on options
* @default
*/
let config_file = './data/config.json';

/** content of config file.
* @default
*/
let config = require("./startconfig.json");
const { version } = require('os');
const { JWT } = require('google-auth-library');
const { send } = require('process');
// {
//   host: 'http://0.0.0.0',
//   port: 8080,
//   data: './data',
//   database: { type: 'postgres' },
//   debuglevel: 'trace',
//   authentication: 'disabled',
//   whitelist: ['http://localhost:8080'],
// };

setConfig(config_file);

global.log = log;
global.config = config;

function setConfig(filename) {
  config_file = filename;
  try {
    // eslint-disable-next-line import/no-dynamic-require
    config = require(filename);
  } catch (e) {
    if (e.code !== 'MODULE_NOT_FOUND') {
      log.error(0, e);
    }
    log.warn(0, `config file missing. Will create ${filename}`);
    file.mkdir(config.data);
  }
}

if (!file.exists(config_file)){
  file.saveJSON(config, config_file)
  .then(() => {
    file.mkdir(config.data);
    return log.init(config.level, `${config.data}/log`);
  })
  .then(() => {
    init(config);
  })
  .catch((error) => {
    log.error('config', error);
  });
}
else{
  log.init(config.level, `${config.data}/log`);
  init(config);
}

// const { createCanvas } = require('canvas'); // until further notice. see https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend/-/issues/44

function checkCaptcha(captcha){
  return true // until further notice. see https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend/-/issues/44
  console.log("checking captcha",captcha,activeCaptchas,activeCaptchas.includes(captcha));
  if (activeCaptchas.includes(captcha) == true){
    log.trace("captcha valid", captcha, "will remove from activeCaptchas")
    activeCaptchas = activeCaptchas.filter((cap)=>{
      return cap === captcha
    });
    return true
  }

  return false
}

/**
 * creates a captcha image and adds the code to the global activeCaptchas.
 * also sets up timers to delete captchas if not used within 90 seconds
 * and tidies up if more than 10 captchas active at a time
 * 
 * inspired by https://github.com/makeuseofcode/CAPTCHA-Validator 
 *  MIT License Copyright (c) 2022 Yuvraj Chandra
 */
async function addCaptchaCode(){
  //const captchaCanvas = createCanvas(180, 40); // until further notice. see https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend/-/issues/44
  const alphaNums = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 
    'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 
    'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9','!',
    '?','-',":"
  ];
  let refreshArr = [];
  for (let j = 1; j <= 7; j++) {
      refreshArr.push(alphaNums[Math.floor(Math.random() * alphaNums.length)]);
  }
  let signupCaptcha = refreshArr.join('');
  setTimeout(()=>{ // remove signupCaptcha after 90 seconds
    activeCaptchas = activeCaptchas.filter((cap)=>{
      return cap === signupCaptcha
    });
    log.debug("disabled captcha",signupCaptcha,activeCaptchas);
  },90*1000)
   // add to activeCaptchas and remove oldest captcha if more than 10;
  if (activeCaptchas.unshift(signupCaptcha) > 10) activeCaptchas.pop();
  const captchaImage = 'disabled until further notice.'//drawTextToCaptcha(signupCaptcha,captchaCanvas); // until further notice. see https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend/-/issues/44
  log.debug("added captcha",signupCaptcha,activeCaptchas);
  return captchaImage
}

async function drawTextToCaptcha(c,captchaCanvas){
  let ctx = captchaCanvas.getContext('2d');
  ctx.font = "40px monospace";
  ctx.fillStyle = "darkgreen";
  //await ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height); // until further notice. see https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend/-/issues/44
  await ctx.fillText(c,0,30);
  return captchaCanvas.toDataURL()
}

async function init() {
  ////// INIT ////////////
  log.init(config.debuglevel,`${config.data}/log`)

  const app = express();
  const server = http.createServer(app);

  const api = express.Router();

  const history = require('connect-history-api-fallback');

  app.use('/api', api);
  
  app.use(history()).use('/', express.static('./data/ui'))

  server.listen(config.port);

  server.on('error', (e) => {
    if (e.code === 'EADDRINUSE') {
      log.error('http server', `Port ${config.port
      } is already in use. Close any other dingsda2mex instance or other server that uses the same port and start again.`);
      process.exit(0);
    } else {
      log.error('server error', e);
    }
  });

  file.mkdir(`${config.data}/uploads`);

  api.use(cors({ credentials: true, origin: isWhitelisted }));

  api.use(express.json());
  api.use(express.urlencoded({ extended: true }));

  api.use('/upload',fileUpload());
  api.use('/items/_csv',fileUpload({useTempFiles:true,tempFileDir:'./data/tmp'}));

  api.use((err, req, res, next) => {
    console.log(req.url);
    if (err instanceof SyntaxError) {
      log.error('API', `${req.method} request to ${req.originalUrl} failed`);
      log.error('API', err.message);
      return res.status(400).send({ name: err.name, message: err.message });
    }
    log.error('API', err.message);
    next(err);
  });

  ////////// API AUTH ////////////
  ////////////////////////////////

  let apiAuth = {}; // object holding possible userlogins and/or methods to access and authenticate those
  // TODO: replace with function fetching specific user credentials from DB to compare
  if (config.authentication === 'Token') {
    apiAuth = new auth.TokenAuth(sql);
  }
  else if (config.authentication === 'disabled') {
    apiAuth = new auth.NoAuth({ login: 'authDisabledUser', password: '' });
  } else {
    log.warn(0, 'No authentication method specified');
    apiAuth = new auth.NoAuth({ login: 'authDisabledUser', password: '' });
  }

  api.use(apiAuth.authenticate.bind(apiAuth));
  
  // DEFAULT CACHING RULES FOR API //
  ///////////////////////////////////

  api.use((req, res, next) => {
    log.info('API MIDDLEWARE', `${req.method} request to ${req.originalUrl} from ${req.user?.login}`);
    res.set('Cache-Control', 'private, max-age=5') 
    next();
  });


  ///////// API ROUTES ////////////
  /////////////////////////////////

  api.use('/uploaded', express.static(path.join(__dirname,'data','uploads')));

  const {version} = require('./package.json');
  console.log('dingsda2mex VERSION',version);

  api.get('/',async(req,res)=>{
    log.info("API /","got get request")
    res.set('Cache-Control', 'No-Cache');
    let usergroups = await sql.getUserGroupsByUser(req.user.login);
    let groupmembers = await sql.getMembersByGroup(req.user.login);
    let userconfig = await sql.getUserConfig(req.user.login);
    log.info("API /","usergroups",usergroups)
    return res.send({
      message:`dingsda2mex api here. you are ${req.user.login}`,
      username:req.user.login,
      usergroups: usergroups,
      groupmembers: groupmembers,
      userconfig: userconfig,
      version: version
    })
  })

  api.post('/captcha',async (req,res)=>{
    res.send({captcha: await addCaptchaCode()});
  })

  const fs = require('node:fs/promises');

  // TODO: test on live server
  // TODO: build the same for places


  /**
   * endpoint to garbage collect unreferenced places from the dB
   * it is intended to be called by an external local daemon/scheduling service e.g. cron
   * this is why it will only accept triggers from localhost.
   * when scaling up dingsda2mex servers this might need to be build into its own script and process
   * but for now this is the easiest implementation
   */
  api.get('/tidyupPlaces',async(req,res)=>{
    console.log("tidyup places");
    if (await denyRequestIfFromUntrustedIp(req,res)) return;
    let done = await sql.removeUnusedPlaces();
    return res.send(done)
  })
  /**
   * endpoint to garbage collect uploaded images from the server's file-system folder ./data/uploads/
   * it is intended to be called by an external local daemon/scheduling service e.g. cron
   * this is why it will only accept triggers from localhost.
   * when scaling up dingsda2mex servers this might need to be build into its own script and process
   * but for now this is the easiest implementation
   */
  api.get('/tidyupImages',async(req,res)=>{ // this function should block all requests from non ADMIN users and/or from 
    if (await denyRequestIfFromUntrustedIp(req,res)) return;
    console.log("tidying up");
    // TODO: first get list of all images on this server
    const userdirs =  await fs.readdir('./data/uploads')
    console.log("userdirs",userdirs,userdirs.length);
    let userfiles = [];
    for (let i=0; i < userdirs.length;i++){
      let dir = userdirs[i];
      try{
      let files = await fs.readdir(path.join('./data/uploads',dir),{withFileTypes:true})
      for (let j=0; j < files.length;j++){
        let userfile = files[j]
        userfile.dir = dir
      }
      userfiles = userfiles.concat(files)
      }catch(e){}
    }
    console.log("userfiles before tidy",userfiles.length);
    // TODO: second get list of all items incl. images associated.
    const items = await sql.getAllItems()
    let unconnected = []
    for (let i=0; i < userfiles.length;i++){  // for every image found in user directories...
      for (let item of items){ // ... go through every item on the server ...
        for (let j=0; j < item.pictures.length;j++){ // ... and check for every picture in every item ...
          let pictureReference = item.pictures[j];
          if (userfiles[i] && pictureReference.endsWith(userfiles[i].name)){ // ... if it matches the name of the image being checked
            userfiles[i] = null; // item uses this, so it is fine and can be sorted out
          }
        }
      }  
    }
    userfiles = userfiles.filter((file)=>file != null)
    console.log("userfiles to be removed",userfiles.length);
    for (let i=0; i < userfiles.length; i++){
      let file = userfiles[i]
      file.fullpath = path.join('./data/uploads',file.dir,file.name)
      // move to toBeRemoved Directory
      let newFileLocation = path.join('./data/toBeRemoved/',file.dir,file.name);
      try{
        await fs.access(path.dirname(newFileLocation))
      }catch(e){
        await fs.mkdir(path.dirname(newFileLocation), {recursive: true})  
      }
      await fs.rename(file.fullpath,newFileLocation)
    }
    return res.send(userfiles); 
  })

  async function denyRequestIfFromUntrustedIp(req,res){
    const trusted = ['127.0.0.1','::1','::ffff:127.0.0.1'];
    const ip = req.socket.remoteAddress;
    if(!trusted.includes(ip)) {
        console.log('request refused for ip',ip);
        res.sendStatus(404);
        return true
    }
    return false
  }

  api.get('/groups/myself',async(req,res)=>{
    let userconfig;
    try{
      userconfig = await sql.getUserConfig(req.user.login)
    }
    catch(err){
      log.error('api','updateUserConfig','denied:',err);
      return res.status(400).send({name:err.name,message:err.message}); 
    }
    
    return res.json({config:userconfig})
  })

  api.post('/groups/myself',async(req,res)=>{
    if (!req.body?.config) {
      return res.status(400).send('No config provided. Expected: {config:{...}}');
    }
    log.info("API","updateUserConfig",`user/group ${req.user.login} tries to update their config`)
    log.debug("API","updateUserConfig config:",req.body.config);
    let updated = await sql.updateUserConfig(req.body.config, req.user.login)
      .catch((err)=>{
        log.error('api','updateUserConfig','denied:',err);
        return res.status(400).send({name:err.name,message:err.message}); 
      });
  
    if (!updated ){
      log.debug("api","updateUserConfig","denied","reason: unclear")
      return res.status(400).send({name:'InvalidError',message:'user could not be updated. Are you sure they exist?'});
    }
    return res.json({ok:true})
  })

  api.post('/groups/myself/password',async(req,res)=>{
    if (!req.user.login) return res.status(403).send({name:'ForbiddenError',message:'you need to be logged in'});
    
    try{
      await sql.changeUserPassword(req.user.login, req.body.password)
      return res.send({ok:true})
    }
    catch(e){
      if(e.name == "InvalidError"){
        return res.status(400).send({name:e.name, message:e.message, stack:e.stack})
      }
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
      }
      log.error("api","error in change password",e)
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })

  api.post('/groups/myself/members', async (req,res)=>{
    let {memberid} = req.body;
    log.debug('addMemberToMyself',memberid)
    if(!memberid){
      log.debug("api","/groups/myself/_addMember","denied","reason: no memberid provided")
      return res.status(400).send({name:'InvalidError',message:'memberid missing'});
    }
    let userAdded = await sql.addUserToGroup(memberid,req.user.login)
      .catch((err)=>{
        log.error('api','addMember','denied:',err);
        return res.status(400).send({name:err.name,message:err.message}); 
      });
    
    if (!userAdded ){
      log.debug("api","addMember","denied","reason: unclear")
      return res.status(400).send({name:'InvalidError',message:'member could not be added. Are you sure they exist?'});
    }
    res.json({userAdded:userAdded})
    return
  })

  // CONTINUE HERE: add deleteMembersFromGroup Endpoint
  api.delete('/groups/myself/members', async (req,res)=>{
    let {memberid} = req.body;
    log.debug('deleteMemberFromMyself',memberid)
    if(!memberid){
      log.debug("api","/groups/myself/_deleteMember","denied","reason: no memberid provided")
      return res.status(400).send({name:'InvalidError',message:'memberid missing'});
    }
    let userDeleted = await sql.removeUserFromGroup({userid:memberid,groupid:req.user.login})
      .catch((err)=>{
        log.error('api','deleteMember','denied:',err);
        return res.status(400).send({name:err.name,message:err.message}); 
      });
    
    if (!userDeleted ){
      log.debug("api","deleteMember","denied","reason: unclear")
      return res.status(400).send({name:'InvalidError',message:'member could not be deleted. Are you sure they exist?'});
    }
    res.json({userDeleted:userDeleted})
    return
  })

  let webhookids = [];

  // this is the link users are send after requesting a password forgotten fix
  api.get('/forgotpassword/:webhookid',(req,res)=>{
    const webhook = webhookids.find((w)=>w.key == req.params.webhookid && w.user == req.query.user);
    if (!webhook) return res.sendStatus(404)
    console.log("reset password for webhookid",req.params.webhookid,req.query.user,req.query.email);
    webhookid = webhook.key;
    res.send(`
    <form method='POST' target="_self" action='${config.externalBaseUrl}/forgotpassword/${webhookid}'>
      <input type="hidden" value="${req.query.user}" name="user">
      <input type="hidden" value="${req.query.email}" name="email">
      <input type="password" placeholder="new password" name="password">
      <br>
      <input type="password" placeholder="repeat new password" name="password2">
      <button>Submit</button>
    </form>
    `)
  })

  // this receives messages from the above get form
  api.post('/forgotpassword/:webhookid',async(req,res)=>{
    const webhook = webhookids.find((w)=>w.key == req.params.webhookid && w.user == req.body.user);
    if (!webhook) return res.sendStatus(404)
    if (req.body.password !== req.body.password2) return res.send('passwords didnt match. try again!')
    if (req.body.password.length < 8) return res.send('passwords need to have 8 or more characters. try again!')
    console.log('POST resetting attempt for webhook',req.params.webhookid);
    webhookids = webhookids.filter((w)=>w.key !== webhookid);
    console.log("removed webhook",webhookids);
    try{
      let resetRes = await sql.changeUserPassword(webhook.user,req.body.password)
      console.log("password reset res",resetRes);
      return res.send(`Success! Go to <a href='${config.mainWebClientUrl}'>${config.mainWebClientUrl}</a> and login again`)
    }catch(e){
      log.error("error resetting password",e);
      return res.status(500).send('something went wrong. try again.');
    }
  })

  const crypto = require('node:crypto');

  // first request to get a reset password webpage (e.g. via email or manually by admin reading logs)
  api.get('/forgotpassword',async (req,res)=>{
    console.log("/forgotpassword",req.query,req.body);
    let {user,email} = req.query;
    if (!user || !email) return res.sendStatus(400)
    const userconfig = await sql.getUserConfig(user);
    if (!userconfig || userconfig.name !== user || userconfig.email !== email) {
      log.trace("API","forgotpassword","username and/or email didnt match. but will send fake 200 status")
      return res.sendStatus(200)
    }
    console.log('got forgot password request',user,email,userconfig);
    if (!userconfig) return res.status(404).send('user not found') // TODO: TEST!
    const webhookid = crypto.createHmac('sha256', process.env.WEBHOOKSECRET || Math.random()+'').digest('hex');
    if (webhookids.unshift({user,key:webhookid}) > 10 ) webhookids.pop(); // if more active than 10, pop last one
    setTimeout(()=>{
      webhookids = webhookids.filter((w)=>w.key !== webhookid)
      console.log("removed webhookid after timeout",webhookids);
    },5*60*1000)
    console.log(`webhook for user ${user}: ${config.externalBaseUrl}/forgotpassword/${webhookid}?user=${user}`);
    // this event can lead to a plugin sending emails, notifying an admin via telegram etc.
    pluginHandler.triggerPluginEvent({domain:'forgotpassword',event:"requestWebhook",data:{user,email,webhook:`${config.externalBaseUrl}/forgotpassword/${webhookid}?user=${user}`}})
    //pluginHandler.triggerPluginEvent({domain:'email',event:"requestWebhook",data:{user,email,webhook:`${config.externalBaseUrl}/forgotpassword/${webhookid}?user=${user}`}})
    res.sendStatus(200)
  })

  api.post('/signup', async (req,res)=>{
    let {id,email,name,password,captcha} = req.body;
    log.debug('signup',id,name,captcha)
    if (!captcha || !checkCaptcha(captcha)){
      log.debug("api","/signup","denied","reason: captcha invalid")
      return res.status(400).send({name:'InvalidError',message:'captcha invalid'});
    }
    if(!id || !email){
      log.debug("api","/signup","denied","reason: username and email need to be provided")
      return res.status(400).send({name:'InvalidError',message:'this user could not be added. please choose a username and provide an email'});
    }
    const forbiddenChars = [" ",",","%","#","=","\"","\'","<",">"]
    if(forbiddenChars.some((c)=>id.includes(c))){
      log.debug("api","/signup","denied","reason: id contains forbidden character")
      return res.status(400).send({name:'InvalidError',message:'user id contains forbidden character'});
    }
    let userAdded = await sql.addUser({id,email,name,password}).catch((e)=>{
      log.debug("api","/signup","denied","reason: error from database:",e)
      return res.status(400).send({name:'InvalidError',message:'this user could not be added. use a longer password'});
    })
    let userfolder = await file.mkdirPromise(path.join(__dirname,'data','uploads',id))
    
    if (!userAdded || !userfolder){
      // TODO: deleteUser in case !userfolder
      log.debug("api","/signup","denied","reason: user or userfolder already exists")
      return res.status(400).send({name:'InvalidError',message:'this user could not be added. use a different id/username'});
    }
    res.json({userAdded:userAdded})
    return
  })

  api.post('/login',async (req,res)=>{
    res.send("ok")
  })
   
  api.post('/upload',
    async (req,res)=>{
      if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
      }
      log.debug("all files", req.files)
      let allUploadsPromises = [];
      let filepaths = [];

      for (let file in req.files){

        let file2upload = req.files[file];
        let promise = new Promise((resolve, reject) => {
        
          let newfilename = file2upload.name;
          let uploadPath = `${config.data}/uploads/` + req.user.login +'/'+ newfilename;
          let externalPath = `${config.externalBaseUrl}/uploaded/` + req.user.login +'/'+ newfilename;
          log.info("fileupload file", uploadPath,file2upload);

          // Use the mv() method to place the file somewhere on your server
          // file2upload.mv(uploadPath, function(err) {
          //   if (err) {reject("error saving file to server")}
          // });
          sharp(file2upload.data)
            .withMetadata() // important to keep image orientation?
            .resize(1000,1000,{fit:'inside'})
            .toFormat("jpeg")
            .jpeg({ quality: 90 })
            .toFile(uploadPath)
            .then(()=>{
              filepaths.push(externalPath);
              resolve();
            }).catch((err)=>{
              log.error("API","upload sharp",err)
              reject(err)
            })         
        })
        allUploadsPromises.push(promise);
      }

      let done = await Promise.all(allUploadsPromises)
        .catch(e =>log.error("could not upload pictures",e))
      if (done) {
        log.info("API","uploaded files to user directory",filepaths)
        return res.send({result:"files uploaded", files:filepaths});
      }
      else {
        return res.status(500).send(done);
      }
    })

  api.get('/printer',async(req,res)=>{
    res.set('Cache-Control', 'No-Cache');
    res.send("please be more specific.")
  })


  api.get('/printer/:id/_queue',async(req,res)=>{
    res.set('Cache-Control', 'No-Cache');
    log.info("API",'getPrinter',req.params.id)
    let prints = await sql.getPrintsByPrinterId(req.params.id, req.user.login)
    if (!prints || !prints.prints) {
      res.statusCode = 400; return res.send('not found');
    }
    res.send(prints.prints)
  })

  api.put('/printer/:id/_queue',async(req,res)=>{
    res.set('Cache-Control', 'No-Cache');
    // check if user has permissions:
    const printerControlled = await sql.getPrintsByPrinterId(req.params.id,req.user.login);
    if (!printerControlled || !printerControlled.prints) {
      res.statusCode = 400; return res.send('not found');
    }
    // then update queue of printer:
    let success = await sql.setPrinterQueue(req.params.id,req.body.prints);
    if (!success) {res.statusCode = 400; return res.send('not found');}
    res.json({ok:true})
  })

  api.delete('/printer/:id',async(req,res)=>{
    res.set('Cache-Control', 'No-Cache');
    // check if user has permissions:
    const printerControlled = await sql.getPrintsByPrinterId(req.params.id,req.user.login);
    if (!printerControlled || !printerControlled.prints) {
      res.statusCode = 400; return res.send('not found');
    }
    // TODO: create printer via sql.createPrinter()
    let created = await sql.deletePrinter(req.params.id);
    if (!created) {
      res.statusCode = 400; return res.send('could not add printer');
    }
    res.json({ok:true})
  })

  api.put('/printer/:id',async(req,res)=>{
    res.set('Cache-Control', 'No-Cache');
    // TODO: create printer via sql.createPrinter()
    let created = await sql.createPrinter(req.params.id,req.user.login)
      .catch((e)=>{
        res.statusCode = 500;
        return res.send(e.message);
      });
    if (!created) {
      res.statusCode = 400; return res.send('could not add printer');
    }
    res.json({ok:true})
  })

  api.get('/items', async (req, res) => {
    // TODO: change .getItems() to include more item props and include items
    res.set('Cache-Control', 'private, max-age=4')
    if (req.query.all === "true"){
      const items = await sql.getItems(req.user.login,req.query.lastItemOnPage,true);
      return res.send(items.rows);  
    }
    const itemsAndTags = await sql.getItems(req.user.login,req.query.lastItemOnPage);
    if(!itemsAndTags){return}
    return res.send({items:itemsAndTags.items,tags:itemsAndTags.tags});
  });

  api.get('/places', async (req,res) =>{
    log.info('api','get places',req.query);
    let places = await sql.getLocations({latitude:req.query.latitude,longitude:req.query.longitude,radius:req.query.radius})
      .catch((e)=>{
        log.warn('api','error in get places',e)
        res.statusCode = 400;
        return res.send(e);
      })
    log.info('api','get places got from db',places.rows)
    return res.send(places.rows)
  });

  api.get('/places/_rootContainer', async (req,res)=>{
    res.set('Cache-Control', 'private, max-age=10');
    res.redirect('/api/items/_rootContainer');
  })

  api.get('/places/:placeID', async (req,res) =>{
    log.info('api','get place',req.params.placeID);
    let place = await sql.getLocation({id:req.params.placeID})
      .catch((e)=>{
        log.warn('api','error in get place',e)
        res.statusCode = 400;
        return res.send(e);
      })
    log.info('api','get place got from db',place)
    return res.send(place)
  });

  api.put('/places',async(req,res) => {
    log.info('api','put', '/places');
    
    let {id, name, geojson} = req.body;
    
    if (!geojson) return res.status(400).send({name:'InvalidError',message:'api cannot add location without at geojson'})

    if (!name){
      name = await helpers.getLocationNameFromGeoJSON(geojson);
      if (!name) return res.status(400).send({name:'InvalidError',message:'api: no name provided and reverse geocoding did not resolve any named features. please provide a name for this location'})
    }

    let newid = await sql.addLocation({id, name, geojson}).catch((e)=>{
      log.warn('api','error in get place',e)
      res.statusCode = 400;
      return res.send(e);
    })

    return res.status(200).send(newid)
  })
  
  api.put('/items', async (req, res) => {
    res.set('Cache-Control', 'No-Cache');
    log.info('api','put item', req.body);
    let newItem;
    try{
      newItem = await sql.addItem(req.body,req.user.login)
    }catch(e){
      if(e.name == "InvalidError"){
        return res.status(400).send({name:e.name, message:e.message, stack:e.stack})
      }
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
      }
      log.error("api","error in put /items",e)
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
    if(!newItem){return res.status(500).send({name:'UnknownError', message:'reason unkown. please contact the developer'})}
    return res.send(newItem);
  });

  api.post('/items/_move',async (req,res)=>{
    log.info("API",'items/_move',req.body);
    let result = await sql.moveItems(req.body.items,req.user.login)
      .catch((err)=>{
        log.error("API","error in moveItems",err)
        if(err.name == "InvalidError"){
          return res.status(400).send({name:err.name, message:err.message, stack:err.stack})
        }
        log.error("api","error in put /items/_move",err)
        return res.status(500).send({name:err.name, message:err.message, stack:err.stack})
      });
    if(result.statusCode > 399){return}
    res.send(result)
  })

  api.post('/items/_update',async (req,res)=>{
    log.info("API",'items/update',req.body,req.body.items)
    let result = await sql.editItems(req.body.items,req.user.login)
      .catch((err)=>{
        if(err.name == "ForbiddenError"){
          return res.status(403).send({name:err.name, message:err.message, stack:err.stack})
        }
        log.error("API","error in editItems",err)
      });
    res.send(result)
  })

  api.get('/items/_search', async (req,res)=>{
    log.info("API","items/_search", req.query);

    if (!req.query.text){return res.status(403).send('missing query')}
    if(req.query.text?.length < 3){ return res.send('search pattern must be at least 3 characters long')}
    if (req.query.text){
      const names = await sql.findDataByPattern({
        tablename:"items",
        columnname:"name",
        pattern:"%"+req.query.text+"%",
        limit:10,
        user:req.user.login
      })
        .catch((e) => {
          log.error("error searching name col",e);
          res.statusCode = 500;
          return {error:e,message:"error in getting item _search"};
        });
      let tags;
      if (req.query.tag && !req.query.tag.includes(" ")){
        tags = await sql.findDataByPattern({
          tablename:"tags",
          columnname:"tag",
          pattern:"%"+req.query.text+"%",
          limit:10,
          user:req.user.login
        })
          .catch((e) => {
            res.statusCode = 404;
            res.send(e);
          });        
      }
      if (!names && !tags){return}
      res.set('Cache-Control', 'No-Cache')
      return res.send({items:names,tags:tags})
    }
  })

  api.get('/items/_searchByName', async (req,res)=>{
    log.info("API","items/_searchByName", req.query);
    // todo: check if at least text or location or storage
    
    //if (!req.query.text){return res.status(403).send('missing query')}
    //if(req.query.text?.length < 3){ return res.send('search pattern for names must be at least 3 characters long')}
    
    if (req.query.text || req.query.area || req.query.containers || req.query.tags || req.query.availableFrom || req.query.availableTill){
      const found = await sql.getItemIdsAndTagsByName({
        name: `%${req.query.text}%`,
        tags: req.query.tags,
        containers: req.query.containers,
        area: req.query.area ? JSON.parse(req.query.area) : null,
        user: req.user.login,
        max_weight: req.query.max_weight, min_weight: req.query.min_weight,
        max_length: req.query.max_length, min_length: req.query.min_length,
        max_width: req.query.max_width, min_width: req.query.min_width,
        max_depth: req.query.max_depth, min_depth: req.query.min_depth,
        max_value: req.query.max_value, min_value: req.query.min_value,
        max_price: req.query.max_price, min_price: req.query.min_price,
        lastItemOnPage: req.query.lastItemOnPage,
        availableFrom: req.query.availableFrom,
        availableTill: req.query.availableTill
      })
      .catch((e) => {
        log.error("error searching by name",e);
        if (e.message.includes('range lower bound must be less than or equal to range upper bound')){
          res.statusCode = 400;
          return {error:e,message:"available till value must be AFTER available from"};  
        }
        res.statusCode = 500;
        return {error:e,message:"error in item searchbyname"};
      });
      //log.trace("api","searchByName returning:",found)
      if (!found){return}
      res.set('Cache-Control', 'No-Cache')
      return res.send(found)
    }
    else{
      return res.status(400).send({name:"InvalidError", message:'you need to specify at least a searchterm OR area OR storage location OR tag', stack:'api'})
    }
  })

  
  api.get('/items/_geosearch', async (req, res) => {
    log.info("API","/items/_geosearch", req.query)
    if (!req.query.latitude || !req.query.longitude || !req.query.radius) return res.status(403).send( "cannot search without latitude/longitude or radius");
    if (req.query.radius > 100000) return res.status(403).json({error:true,reason:"Search radius over 100km is not allowed. Get closer!"});
    let items = await sql.findItemsWithinRadius(req.query.latitude,req.query.longitude,req.query.radius,req.user.login)
      .catch((e) => {
        log.error("API","error doing geosearch",e)
        res.statusCode = 500;
        return {error:e,message:"error in geosearch"};
      });
    if (!items){
      res.statusCode = 404;
      return res.send('nothing found');
    }
    if (req.user.login === "*"){
      items = await filterItemsForPublic(items,{outputformat:"commons-minimal-api-v0.0.3"});
    }
    return res.send(items);
  });

  api.get('/items/_rootContainer', async (req,res)=>{
    log.info("API","/items/_rootContainer",req.user.login);
    let result = await sql.getAllPossibleRootContainersFromUser(req.user.login);
    return res.send(result.rows)
  })

  api.get('/items/:item/_rootContainer', async (req, res) => {
    log.debug("ITEM ROOTCONTAINER!!!")
    const items = await sql.getRootContainerFromItem(req.params.item, req.user.login)
      .catch((e) => {
        log.error('error in detting item rootcontainer',e)
        res.statusCode = 500;
        return {error:e,message:"error in getting item rootcontainer"};
      });
    if (!items){
      res.statusCode = 404;
      return res.send('not found');
    }
    return res.send(items);
  });

  api.get('/items/:item', async (req, res) => {
  
    log.debug("ehm",req.headers.accept);
    if (!req.xhr && req.headers.accept.indexOf('html') > -1 && !req.query.forceJson && process.env.SHOW_PUBLIC_ITEM_HTML == 'true'){
      res.set('Cache-Control', 'No-Cache');
      return res.sendFile(path.join(__dirname, './itemReferer.html'));
    }

    let item = await sql.getItemIfVisible2UserNoClient(req.params.item, req.user.login)
      .catch((e) => { // todo: throw specific errors
        res.statusCode = 500;
        return {error:e,message:"error in getting item"};
      });
    if (!item){
      res.statusCode = 404;
      return res.send('not found');
    }
    if(req.user.login === "*"){
      item = await filterItemsForPublic([item])
      item = item[0];
    }
    return res.send(item);
  });

  api.get('/items/:item/_contents', async(req,res)=>{
    log.debug('api','getItemContenst');
    let contents = await sql.getItemsInsideItem(req.params.item,req.query.deep)
      .catch((e) => {
        log.error("API","/items/:item/_contents",e)
        if(e.name == "InvalidError"){
          return res.status(400).send({name:e.name, message:e.message, stack:e.stack})
        }
        if(e.name == "ForbiddenError"){
          return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
        }
        res.statusCode = 500;
        return {error:e,message:"error in getting item contents"};
      });
    if (!contents){ return }
    return res.send(contents);

  })

  api.post('/items/_reserve',async (req,res)=>{
    log.debug('api',`user ${req.user.login} tries to reserve items`);
    try{
      let reserved = await sql.reserveItems(req.body,req.user.login);
      if (reserved){
        pluginHandler.triggerPluginEvent({
          domain:'reservations',event:'created',
          data:{
            id:reserved[0].id,
            name:req.body.name,
            notes:req.body.notes,
            from:req.body.from,till:req.body.till,
            items:req.body.items
          }
        })
        return res.send(reserved)
      }
    }
    catch(e) {
      log.error('error in reservation request',e);
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })
  api.get('/reservations', async (req,res)=>{
    try{
      const onlyOwnedItems = req.query.onlyOwnedItems ? req.query.onlyOwnedItems == 'true' : undefined;
      const excludePastReservations = req.query.excludePastReservations ? req.query.excludePastReservations == 'true' : undefined;
      const onlyOwnedReservations = req.query.onlyOwnedReservations ? req.query.onlyOwnedReservations == 'true' : undefined
      let reservations = await sql.getReservations(
          req.user.login,
          {
            onlyOwnedItems,
            excludePastReservations,
            onlyOwnedReservations,
            searchReservationsName: req.query.searchReservationsName,
            searchReservationsText: req.query.searchReservationsText,
          }
      );
      if (reservations) return res.send({reservations})
    }
    catch(e){
      log.error('error in getReservations request',e);
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })
  api.get('/reservations/:id', async (req,res)=>{
    try{
      let reservation = await sql.getReservationById(
          req.params.id,req.user.login
      );
      if (reservation) return res.send(reservation)
    }
    catch(e){
      log.error('error in getReservations request',e);
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })
  api.post('/reservations/:id/_removeItems',async(req,res)=>{
    try{
      let updated = await sql.removeItemsFromReservation({reservationid:req.params.id,items:req.body.items,confirmedby:req.user.login})
      pluginHandler.triggerPluginEvent({domain:'reservations',event:'updated',data:{id:req.params.id,userRequesting:req.user.login}})
      return res.send(updated);
    }
    catch(e){
      log.error('error in removeItemsFromReservation request',e);
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })
  api.post('/reservations/:id/_addItems',async(req,res)=>{
    try{
      let updated = await sql.addItemsToReservation({reservationid:req.params.id,items:req.body.items,confirmedby:req.user.login})
      pluginHandler.triggerPluginEvent({domain:'reservations',event:'updated',data:{id:req.params.id,userRequesting:req.user.login}})
      return res.send(updated);
    }
    catch(e){
      log.error('error in AddItemsToReservation request',e);
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })
  api.post('/reservations/:id',async(req,res)=>{
    try{
      let updated = await sql.editReservation({reservationid:req.params.id,reservation:req.body.reservation,confirmedby:req.user.login})
      pluginHandler.triggerPluginEvent({domain:'reservations',event:'updated',data:{id:req.params.id,userRequesting:req.user.login}})
      return res.send(updated);
    }
    catch(e){
      log.error('error in editReservationMetaInformation request',e);
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })
  api.delete('/reservations/:id',async(req,res)=>{
    log.debug('api',`user ${req.user.login} tries to delete reservation ${req.params.id}`);
    try{
      let deleted = await sql.deleteReservation(req.params.id,req.user.login); // CONTINUE HERE: move to /reservations
      if (deleted){
        pluginHandler.triggerPluginEvent({domain:'reservations',event:'deleted',data:{id:req.params.id,userRequesting:req.user.login}})
        return res.send(deleted)
      }
    }
    catch(e) {
      log.error('error in reservation delete request',e);
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })
  api.get('/items/_reserve',async (req,res)=>{
    // redirect
   res.redirect('/reservations')
  })
  api.get('/items/:item/_reserve',async (req,res)=>{ 
    try{
      let reservations = await sql.getReservationsByItem(req.params.item,req.user.login);
      if (reservations) return res.send({reservations})
    }
    catch(e) {
      log.error('error in getReservation request',e);
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })

  api.get('/items/:item/_ask2borrow', async(req,res)=>{
    log.debug('api',`user ${req.user.login} asks for item ${req.params.item}`);
    try{
      let contactDetailsOfOwner = await sql.getContactDetailsFromOwners(req.params.item,req.user.login)
      log.debug("API","contactDetailsOfOwner",contactDetailsOfOwner.rows)
      if (!contactDetailsOfOwner.rows){ return}
      return res.send(contactDetailsOfOwner.rows);
    }
    catch(e){
      log.error("API","/items/:item/_ask2borrow",e)
      if(e.name == "InvalidError"){
        return res.status(400).send({name:e.name, message:e.message, stack:e.stack})
      }
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
      }
      res.statusCode = 500;
      return {error:e,message:"error in ask2borrow item"};
    }
  

  })

  api.post('/items/:item/_handover', async(req,res)=>{
    log.debug('api',`user ${req.user.login} hands over item ${req.params.item} to ${req.body.to}`);
    try{                                                                                                      // TODO: make all async await catch blocks into try catch as done here.
      let dbRes = await sql.handoverItem({item:req.params.item,owner:req.user.login,receivers:req.body.to})
      if (!dbRes){ return}
      return res.send(dbRes);
    }
    catch(e){
      log.error("API","/items/:item/_handover",e)
      if(e.name == "InvalidError"){
        return res.status(400).send({name:e.name, message:e.message, stack:e.stack})
      }
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
      }
      res.statusCode = 500;
      return {error:e,message:"error in _handover item"};
    }

  })

  api.post('/items/_handover',async(req,res)=>{
    log.debug('api',`user ${req.user.login} hands over items ${req.body.items} to ${req.body.to}`);
    try{
      let handedout = await sql.handoverItems({items:req.body.items,owner:req.user.login,receivers:req.body.to})
      if (!handedout){ return }
      return res.send({handover:true});
    }
    catch(e){
      log.error("API","/items/_handout",e)
      if(e.name == "InvalidError"){
        return res.status(400).send({name:e.name, message:e.message, stack:e.stack})
      }
      return res.status(500).send({name:e.name, message:e.message, stack:e.stack})
    }
  })

  api.post('/items/_csv',async (req, res) => {
    if (!req.files) return res.sendStatus(400);
    //console.log('api','csv upload request',req.files.csvfile.data);
    // best would be to get the req stream and pipe the csv file directly into the sql method, but out of time and research constraints went here with tmp file (which will limit the max size and speed)
    try{
      let file = req.files.csvfile.tempFilePath;
      await sql.importCSVfile(file,req.user.login)
      return res.send('ok');
    }
    catch(e){
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
      }
      
      return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
    }
    
  })

  api.post('/items/:item', async (req, res) => {
    if (!req.params.item){ throw new Error("no item id provided")}
    req.body.id = req.params.item;
    log.debug("API","UPDATEITEM");
    try{
      let updated = await sql.editItem(req.body, req.user.login)
      if (!updated){ return }
      return res.send({id:req.params.id, updated:true});
    }
    catch(e){
      log.error("API","/items/:item",e)
      if(e.name == "InvalidError"){
        return res.status(400).send({name:e.name, message:e.message, stack:e.stack})
      }
      if(e.name == "ForbiddenError"){
        return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
      }
      res.statusCode = 500;
      return {error:e,message:"error in updating item"};
    }
   
  });

  api.get('/tags/:tag', async (req,res) =>{
    log.info("API","/tags/:tag = ", req.params.tag)
  
    if(req.params.tag?.length < 3 ){ 
      return res.status(403).send({name:"char minimum",message:'search pattern must be at least 3 characters long'})
    } // TODO: throw proper error with name and message
    let items = null;
    if (req.params.tag.includes(" ")){
      log.debug("API","GET /tags/:tag tag includes a space. tags are not allowed spaces.")
      return res.status(403).send({name:"no spaces",message:`tag includes space at position ${req.params.tag.indexOf(' ')} which is not allowed`})
    }
    if (!req.params.tag.includes(" ")){
      items = await sql.findDataByPattern({
        tablename:"tags",
        columnname:"tag",
        pattern:"%"+req.params.tag+"%",
        limit: 10
      })
        .catch((e) => {
          log.error("API",e)
          res.statusCode = 500;
          return {error:e,message:"error in getting tag"};
        });
      log.info("API","tags",items)
    }

    if (!items){
      return
    }
    res.set('Cache-Control', 'No-Cache')
    return res.send(items);
  })

  api.delete('/items/:item', async (req, res) => {
    try{
      let deleted = await sql.deleteItem({id:req.params.item,user:req.user.login});
      if (!deleted){return}
      log.info("API",`deleted`,deleted)
      res.set('Cache-Control', 'No-Cache')
      return res.send({deleted:deleted});
    }
    catch(e) {
        if(e.name == "InvalidError"){
          return res.status(400).send({name:e.name, message:e.message, stack:e.stack})
        }
        if(e.name == "ForbiddenError"){
          return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
        }
        if(e.name == "ForeignKeyRelationError"){
          return res.status(403).send({name:e.name, message:e.message, stack:e.stack})
        }
        res.statusCode = 500;
        log.error("API",e) 
        return {error:e,message:"error in deleting item"};
    }
  });

  // api.get('/sql/search', async (req, res) => {
  //   log.info("API", req.query)
  //   const items = await sql.findDataByTextSearch({
  //     tablename:req.query.table,
  //     columnname:req.query.column,
  //     searchquery:req.query.searchquery
  //   })
  //     .catch((e) => {
  //       res.statusCode = 500;
  //       return {error:e,message:"error in sql search"};
  //     });
  //   if (!items){return}
  //   return res.send(items);
  // });

  // api.get('/sql/pattern', async (req, res) => {
  //   log.info("API", req.query)
  //   if (req.query.pattern?.length < 4){
  //     res.statusCode = 403;
  //     res.send("minimal length of pattern must be 4 characters")
  //     return
  //   }
  //   const items = await sql.findDataByPattern({
  //     tablename:req.query.table,
  //     columnname:req.query.column,
  //     pattern:req.query.pattern
  //   })
  //     .catch((e) => {
  //       res.statusCode = 500;
  //       return {error:e,message:"error in sql pattern"};
  //     });
  //   if (!items){return}
  //   return res.send(items);
  // });
}



/*/////////////////////////
HELPER FUNCTIONS
/////////////////////////*/

/**
 * check whitelist to see if cross origin access is allowed for origin url
 *
 * @param {*} origin
 * @param {*} callback
 */
function isWhitelisted(origin, callback) {
  if (typeof origin === 'undefined') {
    callback(null, true);
  } else if (origin.indexOf('//localhost') !== -1) {
    callback(null, true);
  } else if (config.whitelist) {
    const originIsWhitelisted = config.whitelist.indexOf(origin) !== -1;
    if (!originIsWhitelisted) {
      log.warn(0, `Rejected request from remote ${origin} because it is not in whitelist.`);
    }
    callback(null, originIsWhitelisted);
  } else {
    callback(null, false);
  }
}

async function filterItemsForPublic(items,configLocal){
  if(!config.publicItemsOutsidePlatform){
    return []
  }
  let output;
  let outputformat;
  if (configLocal){
    ({outputformat} = configLocal);
  }
  if (outputformat && outputformat === "commons-minimal-api-v0.0.3"){
    output = {items:[]}
    output.items = await Promise.all(
      items.map(async (item)=>{
        item.ISCENSORED = true;
        item.format = outputformat;
        item.policy = "other";
        item.reuseCategory = "depends";

        // if (config.mainWebClientUrl && config){
        //   item.url = config.mainWebClientUrl + '/Detail?id=' + item.id;
        // }
        // else{
          item.url = (config.mainWebClientUrl || config.externalBaseUrl)+ '/api/items/'+ item.id;
        //}


        if (item.insideof?.length > 0){
          let rootLocation = await sql.getRootContainerFromItem(item.id);
          //item['location.id']=rootLocation[0]['location.id'];
          let newlocation = {type:"Feature", geometry:rootLocation[0]['location.geoJSON'],properties:{zip:null, name:rootLocation[0].name}};
          delete item.location;
          item.location = newlocation;
        }
        else{
          let newlocation = {type:"Feature", geometry:item['location.geoJSON'],properties:{zip:null,name:item['location.name']}};
          delete item.location;
          item.location = newlocation;
        }

        delete item.id;
        delete item.pictures;
        delete item.owned;
        delete item.count;
        delete item.iscontainer;
        delete item.height;
        delete item.length;
        delete item.width;
        delete item.depth,
        delete item.weight;
        delete item.tags;
        delete item.insideof;
        delete item['location.insideofArray'];
        delete item['location.geoJSON'];
        delete item['location.placename'];
        delete item['location.id'];
        delete item['location.geography'];
        delete item.visible2;
        delete item.possessedby;
        delete item.billingreference;
        delete item.invoicedate;
        delete item.countryoforigin;
        delete item.value;
        delete item.price;
        delete item.lastmodified;
        delete item.lastmodifiedby;
  
        return item
      })
    )
  }
  else{
    output = await Promise.all(
      items.map(async (item)=>{
        item.ISCENSORED = true;
  
        if (item.insideof?.length > 0){
          let rootLocation = await sql.getRootContainerFromItem(item.id);
          item['location.id']=rootLocation[0]['location.id'];
          item['location.geoJSON']=rootLocation[0]['location.geoJSON'];
          //item['location.placename']=rootLocation[0]['location.placename']; // this is too precise. so should be zipcode maximum or cityname
        }
        
        let contactDetailsOfOwners = await sql.getContactDetailsFromOwners(item.id,"*");
        if (contactDetailsOfOwners?.rows?.length > 0){
          item.contactDetailsOfOwners = "";
          for (let contactDetail of contactDetailsOfOwners.rows){
            item.contactDetailsOfOwners += ''+
            contactDetail.public_contact_details
            +','
          }
        }
  
        delete item.insideof;
        delete item['location.insideofArray'];
        delete item['location.placename'];
        delete item.visible2;
        delete item.possessedby;
        delete item.billingreference;
        delete item.invoicedate;
        delete item.countryoforigin;
        delete item.value;
        delete item.price;
        delete item.lastmodified;
        delete item.lastmodifiedby;
  
        return item
      })
    ) 
  }

  return output
}


