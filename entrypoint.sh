#!/bin/sh

# echo "waiting 90 sec...";

# sleep 90;

PGPASSWORD="$POSTGRES_PASSWORD";
PGUSER="${POSTGRES_USER:-postgres}";
PGDATABASE="${POSTGRES_DB:-postgres}";
maptilerKey="$maptilerKey";

echo "starting $PGDATABASE as $PGUSER with pw $PGPASSWORD...";

# check if db is already inititalized

echo "trying to connect to database: $PGDATABASE with user $PGUSER and pw $PGPASSWORD";

while [ "$( psql postgresql://$PGUSER:$PGPASSWORD@$PGHOST/template1 -tAc "SELECT 1 FROM pg_database WHERE datname='$PGDATABASE'")" != '1' ]
do
 echo "waiting"
 sleep 3
done

echo 'database exists, initializing if needed...';

if [ "$( psql postgresql://$PGUSER:$PGPASSWORD@$PGHOST/template1  -tAc "SELECT EXISTS (SELECT * FROM items)")" != 'True' ]
then
   echo "Database not initialized, writing initial structure...";
   echo "psql postgresql://$PGUSER:$PGPASSWORD@$PGHOST/$PGDATABASE  -f ./db/sql/sql_code/CreateDatabase/createDBFunctions.sql";
   psql postgresql://$PGUSER:$PGPASSWORD@$PGHOST/$PGDATABASE  -f ./db/sql/sql_code/CreateDatabase/createDBFunctions.sql;
   psql postgresql://$PGUSER:$PGPASSWORD@$PGHOST/$PGDATABASE -f ./db/sql/sql_code/CreateDatabase/createDBTables.sql;
   echo "adding PUBLIC usergroup";
   psql postgresql://$PGUSER:$PGPASSWORD@$PGHOST/$PGDATABASE -c "INSERT INTO users VALUES ('*','','PUBLIC',crypt('$PGPASSWORD', gen_salt('bf', 10)));";
   echo "adding ADMIN user";
   psql postgresql://$PGUSER:$PGPASSWORD@$PGHOST/$PGDATABASE -c "INSERT INTO users VALUES ('ADMIN','','ADMIN',crypt('$PGPASSWORD', gen_salt('bf', 10)));";
fi


echo "database set";

PGHOST=$PGHOST PGUSER=$PGUSER PGPASSWORD=$PGPASSWORD PGDATABASE=$PGDATABASE node index.js