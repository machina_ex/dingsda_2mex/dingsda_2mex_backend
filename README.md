# dingsda 2meX Backend and APIs
this repository is part of the MACHINA COMMONS project <a href="https://gitlab.com/machina_ex/dingsda_2mex">dingsda 2mex</a>. 

This repository contains the backend/server, including database and RESTful API. For the user interface please checkout the <a href="https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend">webclient</a>

[TOC]

## user documentation
user documentation about how to use the app after install can be found at https://machina_ex.gitlab.io/dingsda_2mex/dingsda2mex_tutorials/

## installation

To install your own instance of dingsda2meX you have to install dingsda2mex server and dingsda2mex client:

To install the dingsda 2mex server you have 2 possibilities:

- install from source (needs database setup etc.)
- install via docker-compose (will setup database etc for you)

### install database

    (if you run Ubuntu and need more details, see below for the installation example)

1. install postgresql (see https://www.postgresql.org/download/ for more information on your specific platform. On MacOS we recommend the listed PostgresApp)
2. install postgis extension to postgresql server (see https://postgis.net/install/)

### setup database
1. connect to the database server.
    > tip: there are many ways to connect to a postgresql database. You can do it for example via the [terminal program psql](https://www.postgresqltutorial.com/postgresql-getting-started/connect-to-postgresql-database/) or (our favorite way) with a client application like [pgAdmin](https://www.pgadmin.org/) 
    
    > if you are connecting to a remote server, you will also need to connect via ssh first or use the option ssh tunnel in pgadmin
2. [create a database](https://www.tutorialsteacher.com/postgresql/create-database) (call it `dingsda2mex` or sth similar, so you can identify it)
3. and run `/db/sql/sql_code/createDatabase/createDBFunctions.sql`
4. then run `/db/sql/sql_code/createDatabase/createDBTables.sql`
5. add PUBLIC and ADMIN user as described in the [db sql documentation](./db/sql/README.md#the-public-usergroup)
6. (optional) (development/testing) run `/db/sql/sql_code/populateWithTestData.sql` if you want to have some basic items and fake users to play with. Another option is to install all nodejs dependencies (see below) and then run `node /db/sql/populateDB.js` if you prefer larger sets of test data.

### pgAdmin

- Create server

- Port 5432, Maintanence database postgres, username postgres, password: your postgresql password

- Server Auswählen (einwählen)

### Install and configure postgres example for Ubuntu/Debian

Update apt resources

```bash
sudo apt update
```

Make sure that git is installed

```terminal
sudo apt-get -y install git;
```

if you haven't installed curl and nodejs 14 on your system, do so now:

```terminal
sudo apt-get -y install curl;

curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash - ;

sudo apt-get install -y nodejs;
```

install dingsda2mex:

```terminal
git clone https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_backend.git;

cd dingsda_2mex_backend/ ;

npm i;
```

Install postgresql version 13 using apt repository

```terminal
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list';

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - ;

sudo apt-get update;

sudo apt-get -y install postgresql-13;
```

install postgres' postGIS extension for postgres 13 (from tutorial https://computingforgeeks.com/how-to-install-postgis-on-ubuntu-linux/)

```terminal
sudo apt -y install gnupg2;

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - ;

echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" |sudo tee  /etc/apt/sources.list.d/pgdg.list;

sudo apt update;
sudo apt -y install postgis postgresql-13-postgis-3;
```

Connect to postgres user and set a password for the postgresql user 'postgres' (the default user of every postgresql server comes without a password).

Connect as user postgres with psql:
```terminal
sudo -u postgres psql postgres;
```
then, within psql's terminal, type the following to change/add a password for the user 'postgres'
```terminal
\password postgres
```
then provide new password twice when prompted

more infos why see [here](https://serverfault.com/questions/110154/whats-the-default-superuser-username-password-for-postgres-after-a-new-install)


then, within psql's terminal, create a new database named 'dingsda2mex'

```
CREATE DATABASE dingsda2mex;
```

Leave psql with `exit`

Then run dingsda2mex' database configuration files:

```bash
PGUSER=postgres PGPASSWORD=<yoursecretnewpassword> PGHOST=127.0.0.1 psql -d dingsda2mex -U postgres -f ./db/sql/sql_code/CreateDatabase/createDBFunctions.sql;
```
where `<yoursecretnewpassword>` needs to be replaced with your new password for the user

and

```bash
PGUSER=postgres PGPASSWORD=<yoursecretnewpassword> PGHOST=127.0.0.1 psql -d dingsda2mex -U postgres -f ./db/sql/sql_code/CreateDatabase/createDBTables.sql;
```

now your database is empty but ready to be used.

> note: if postgres does not start on system startup use `sudo service postgresql restart`

### install from source

#### install nodejs dependencies

if you haven't installed nodejs on your system, do so.

if not yet happened install all dependencies

```terminal
npm i;
```


#### create config file
create `startconfig.json` on root level and fill it with your preferred configuration. 

for example:
```json
{
    "host": "http://0.0.0.0",
    "port": 8080,
    "data": "./data",
    "database": { "type": "postgres" },
    "debuglevel": "trace",
    "authentication": "Token",
    "externalBaseUrl": "http://localhost:8080/api",
    "maptilerKey": "YOUR KEY FOR THE MAPTILER API"
}
```

If you want the reverse geocoding endpoints of the api to work, you will also need to optain an account and a api key from https://cloud.maptiler.com/ and set the value of `maptilerKey` in `startconfig.json` or `data/config.json` to the api key optained.

this config file will only be used if no `/data/config.json` file is present. the server script will create `/data/config.json` and then use this.

#### create .env file

create an .env file in the root directory. this will contain your admin secrets and should have the following minimal content:

```
TOKEN_SECRET=YOURLONGTOKENSECRETTOSIGNJWTS
SALTROUNDS=10
PGUSER=postgres
PGPASSWORD=<yoursecretnewdatabasepassword>
PGDATABASE=dingsda2mex
DIRECT_TAG_ADDING_ALLOWED=true
WEBHOOK_SECRET=<yoursecretforpasswordresets>
SHOW_PUBLIC_ITEM_HTML=false
```

PGUSER must be the user that you want to connect to the postgresql with ('postgres' if you followed the instructions above).
PGPASSWORD is the password to that postgresql user.
PGDATABASE is the name of the dingsda2mex database that you created above ('dingsda2mex').

TOKEN_SECRET is used to sign <a href="https://jwt.io/">JSON Web Tokens</a>, which are used in user authentication.
Your TOKEN_SECRET can be any string secret to only you/the admins.
If you want to know more about how to create your JWT TOKEN_SECRET, see: https://www.npmjs.com/package/jsonwebtoken 


DIRECT_TAG_ADDING_ALLOWED sets the whole server to allow or disallow users from adding new tags on sql.addItem() or sql.editItem()

WEBHOOK_SECRET (RECOMMENDED) is used as a secret for the password reset function (see index.js `api.get('/forgotpassword')`). If not provided, a randomized number will be used (not recommended).

SHOW_PUBLIC_ITEM_HTML sets the whole server to display the page itemReferer.html (meant to be adapted to your needs) in case someboday accesses a public item via the browser. All public items (aka items that are visible2 "*") will be displayed in a censored way including their owners public_contact_details to everybody with the link to the item (e.g. `https://example.com/api/items/25ebe654-866e-42a5-b078-5143ebcf7d01`)

#### create PUBLIC and ADMIN user
see [db sql documentation](./dingsda_2mex_backend/db/sql/README.md#the-public-usergroup).

#### start server

`npm run serve` (restarts automatically using nodemon) or `node index.js` (no auto restart)

The server should be running now and the endpoints be reachable at `/api/`

> If you like you can host the <a href="https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend">dingsda 2mex client</a> on `/` by building it and then moving the contents of the frontend's `/dist` into the `/data/ui/` in this repository's directory structure.

#### configure server and install client

after the server is running and exposes the api, we can now attach clients like the <a href="https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend">dingsda 2mex client</a>: 

- Make sure the clients api class' baseurl points to your `/api` endpoint by setting it correctly in the client's .env files. 

- Add the clients location to the whitelisted domains in `/data/config.json` (this is to not end up with CORS error messages). Also add you `maptilerKey` (see above) to the client's `config.json`

- We also recommend enabling authentication, by changing `authentication` in `startconfig.json` or `data/config.json` to value `"Token"` if not already done.

### install via docker-compose

1. install docker and docker-compose
2. clone this repo 
3. create dingsda2mex.env file as shown in "create .env file" section but name the variables `POSTGRES_USER` instead of `PGUSER`, `POSTGRES_PASSWORD` instead of `PGPASSWORD` and `POSTGRES_DB` instead of `PGDATABASE`.
4. create config file in data_dd2m/config.js as described in "create config file" section (you can also do that after the install)
5. run `docker-compose up -d` to start the backend.
6. your server instance and its API should be running now at port 8080 at the endpoint `/api`
7. build and install the [dingsda2mex frontend client](https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend) to access the api via webapp.
> You can host your frontend client whereever you want. but you can also let the docker instance host it directly by copying all content from [dingsda2mex frontend client's](https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend) build (`dist/`) into `data_dd2m/ui/`. It will be then reachable under the same port as the api but on the endpoint `/`.

## database

For more information about the Database design, see `/db/sql/README.md`

All communication with the postgres database is done through the module `postgres.js` which handles pooling clients and connections.
For all upcoming additional databases a module with the same API should be created.
To see it in action, see `db/sql/populateDB.js` (which popoulates the database with randomized fake data to play with) or `db/sql/testSqlModule.js` (which is a simpler testscript to checkout single methods)


## APIs

dingsda 2mex has a RESTful API with classic HTTP endpoints.
The API is described and documented in the openapi spec in the file `doc/api/openapi.yaml`
This file aims to be used to create client code to streamline the communication of client applications with the dingsda 2 server(s).

### Authentication

To authenticate with the API a client needs to send userId and password within a <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization#basic">Basic Authentication Header</a> with its HTTP request.
If the server can verify the user existing and their password, it will include an Authorization Header with a Bearer Token that the client can use after this to identify themselves in all their subsequent requests by sending it in its Headers.

In case that the Token Authentication fails, a 403 error will be send and the client has to try to login again with username and password.

### File Upload

an optional API endpoint is `/api/upload`. 
It allows clients to upload images from their devices to the dingsda 2mex server, if they have a directory called after their user uuid within `/data/uploads/`. All images from the client will be uploaded there. Depending on your use case and access rights to your dingsda 2mex instance, you might want to restrict this endpoint further and adapt the client code accordingly.

It default behaviour will accept all file types and link them in the database's item column pictures.

### Cloud Printer

dingsda2mex server provides it's own simple cloud printing API, to make it possible to print QR Labels of items directly from the smartphone via a [raspberri pi bridge](https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_printer) that will talk to the printer API of the dingsda2mex server to pull all item ids that



## Contributing

We are very much open to contributions.

Feel free to submit issues or merge requests with changes you think make sense.

## License

Source code licensed MIT

## Authors and acknowledgment

This is a machina commons project by machina eX.  

gefördert durch Senatsverwaltung für Kultur und Europa Berlin  
<img src="https://machinacommons.world/img/Logo_Senat_Berlin.png" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="400"/>
