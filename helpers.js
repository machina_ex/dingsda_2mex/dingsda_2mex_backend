const turf = require('@turf/turf');
const fetch = require('node-fetch');

//const config = require("./data/config.json");
//let config = global.config

/**
 * gets a name/description by doing a reverse geosearch on a geojson geometries centerpoint
 * 
 * @param {object} geojson one (!) geojson geometry describing the area of the location. Not allowed are higher geojson levels (e.g. Feature Collections etc)
 * @returns string describing the geojsons geometry or (if nothing found) null
 */
async function getLocationNameFromGeoJSON(geojson) {
  let name;
  let center = await turf.center(geojson);
  
  name = await reverseGeoCode({ longitude: center.geometry.coordinates[0], latitude: center.geometry.coordinates[1] });
  return name;
}
  
async function reverseGeoCode({longitude,latitude}){
  const tileproviderKey = global.config.maptilerKey;
  console.log("tileproviderKey",tileproviderKey);
  const reverseGeocodingUrl = `https://api.maptiler.com/geocoding/${longitude},${latitude}.json?key=${tileproviderKey}`
      
  let res = await fetch(reverseGeocodingUrl).then((response) => {
    return response.json()
  }).catch((e)=>console.warn(e))
      
  if (res && res.features && res.features.length > 0){
    console.log(res.features[0].place_name);
    return res.features[0].place_name
  }
  return null
}


module.exports = {
  getLocationNameFromGeoJSON,
  reverseGeoCode,
}