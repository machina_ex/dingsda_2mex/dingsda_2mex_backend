# Uberspace Setup tutorial
(based on v1.5.0)

Because lots of the times we and other users have been using uberspace.de as hosting service, we thought it might be worthwhile to add some documentation about the setup and installation process on a fresh uberspace instance, in addition to the other ways (native install and or docker-compose) of installing dingsda2mex on a server.

## Setup
uberspace has a postgresql instance already running and setup for use. 

To create a database cluster with credentials etc, 

1. simply follow the [uberlab guide for postgres](https://lab.uberspace.de/guide_postgresql/). 
    
    >beginner tip: when the tutorial tells you to edit or create a file, you can for example use [`nano`](https://linuxize.com/post/how-to-use-nano-text-editor/#opening-and-creating-files), the command line text editor, which is already installed.
2. you might encounter (depending on your cli) an error on the step initializing the database cluster (unless [this pull request](https://github.com/Uberspace/lab/pull/1468) has been resolved):
    ```terminal
    initdb --pwfile ~/pgpass.temp --auth=scram-sha-256 -E UTF8 -D ~/opt/postgresql/data/
    The files belonging to this database system will be owned by user "yourusername".
    This user must also own the server process.

    initdb: error: invalid locale settings; check LANG and LC_* environment variables
    ```
    This means you need to set your locale correctly (which you can do with the `--locale=<localeIDString>`). For example if you want your locale to be German add `--locale=de_DE.UTF-8`. You can also set `--no-locale`.

3. finish the setup as described in the uberlab guide chapters `Initialization` and `Configuration`
    > if you are using pgAdmin to connect to your database: Right Click on Servers > `Register` > `Server` and fill out under Connection: `Hostname: localhost`, `Username: <your uberspace username>`,`Password: <your ~/.pgpass password (after the last colon)>`. Also don't forget to fill out the SSH Tunnel connection details: `Tunnel host: <youruberspace server's domain>`, `Username: your username` and add your ssh key or password.
4. Now follow the steps [README.md Section 'setup database'](./README.md#setup-database) but skip the last step. 
5. Make sure you are using node18: `uberspace tools version use node 18`
6. Now clone the backend repo, (checkout whatever branch you need) and `npm i`;
7.  decide what domain you want to reach your dingsda2mex webapp at and configure your uberspace routes accordingly:
    - point to your dingsda2mex backend api port as described in the [uberspace web-backends](https://manual.uberspace.de/web-backends/). For example:
    ```terminal
    uberspace web backend set / --http --port 8080
    ```
    will point the main url of your uberspace instance to your dingsda2mex service.
8. Get a maptiler.com and a mapbox.com account and api keys set to your uberspace domain
9. Download, configure and build the [dingsda2mex client](https://gitlab.com/machina_ex/dingsda_2mex/dingsda_2mex_frontend) as described in its README (checkout whatever branch you need).
    - Don't forget, if you use maptiler, to get a maptiler logo and add it to `src/assets/maptiler-logo.svg` before you build!
    - you will change the .env.production file to sth like this
    ```.env
    NODE_ENV=production
    VUE_APP_TITLE=<your projects name> dingsda2mex
    VUE_APP_API_URL=https://youraccount.dingsda.uber.space/api
    NODE_ENV=production
    VUE_APP_SIGNUPACTIVE=true
    VUE_APP_RESETPASSWORDACTIVE=true
    VUE_APP_FORGOTPASSWORD_TEXT=Request received. Your admin can provide you a restore link for the next 5 minutes. Please contact them.
    ```
    and a config.json file like this
    ```json
    {
    "maptilerKey":"yourmaptilerKey",
    "mapboxAccessToken":"yourMapboxToken"
    }
    ```
    - After using `npm run build` in dingsda2mex_frontend, modify `manifest.json`, copy **all** content from `dist/` to `dingsda2mex_backend/data/ui/` to let the dingsda2mex_backend serve the client
10. create a [config file](./README.md#create-config-file) inlcuding all geocoding API keys as described in the README.md
It Should roughtly look like this:
    ```json
    {
    "host": "http://0.0.0.0",
    "port": 8080,
    "data": "./data",
    "database": {
        "type": "postgres"
    },
    "debuglevel": "info",
    "authentication": "Token",
    "whitelist": [
        "https://youraccount.uber.space"
    ],
    "externalBaseUrl": "https://youraccount.uber.space/api",
    "maptilerKey": "yoursecretmaptilerkey"
    }
    ```
11. create an [.env file](./README.md#create-env-file) as described in the README.md. Probably sth like this:
    ```.env
    TOKEN_SECRET=<YOURLONGTOKENSECRETTOSIGNJWTS>
    SALTROUNDS=10
    PGUSER=<your uberspace account name>
    PGPASSWORD=<your password from ~/.pgpass>
    PGDATABASE=dingsda2mex
    ```
12. configure or deactivate all present plugins (Reservations2gCalendar, email etc.) as described in their READMEs
12. now everything should be setup and ready to go.
13. you only need to start a daemon (background service) running dingsda2mex: For that you have several options (here the two most common to us):
    - a. you can use the [screen terminal multiplexer](https://www.howtogeek.com/662422/how-to-use-linuxs-screen-command/): 
        - from your dingsda2mex_backend directory, start screen: 
        ```terminal
        screen
        ```
        - a fresh screen process window opens. From here type: 
        ```terminal
        npm run serve
        ```
        to start dingsda2mex.
        - if everything is running, detach from the screen session with `CTRL+A+D`. It is running now in the background.
    - b. you can use the [supervisor daemon](https://manual.uberspace.de/daemons-supervisord/).
