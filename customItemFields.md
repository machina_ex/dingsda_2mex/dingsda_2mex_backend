# custom item fields plugin API

***proposal***

places in backend to setup:

- [ ] postgresql.js
    - [ ] addItem()
    - [ ] editItem()
- [ ] /db/sql/ sql init scripts
    - [ ] general sql/db init script?
- [ ] index.js
    - [ ] filterItemsForPublic()
    - [ ] '/items/_searchByName' (filter limits)

places in frontend to setup:

- [ ] /src/views/AddDing.vue
    - [ ] html/vue 
    - [ ] js/vue
- [ ] /src/views/EditDing.vue
    - [ ] html/vue 
    - [ ] js/vue
- [ ] /src/store/actions.js
    - [ ] addItem (parser from item[key])
    - [ ] editItem (parser from item[key])
    - [ ] the above should be unified to take input from one map
