const dingsdaconfig = require('../../data/config.json')
let config = {disabled:true}

try{
 config = require('./config.json');    
}catch(e){
    console.warn("email plugin present but no config")
}


const plugin = class {
    constructor(plugindata,sql) {
        this.name = 'calendarPlugin';
        if (config.disabled){
            this.disabled = true;
            return
        }
        console.log("constructing calendarPlugin");
        // Google APIs (auth etc.)
        const {google} = require('googleapis');
        this.google = google;
        // service-account key file
        this.privatekey = require('./service-key.json');
        // Google Calendar API
        this.calendar = google.calendar('v3');

        this.topic = 'reservations';
        this.calendarId = config.calendarId;
        this.sql = sql;
        // must also have methods: init() and handler(inputevent) (see below)
    }
    
    ///////////////////////////////////
    // mandatory lifecycle methods: ///
    ///////////////////////////////////

    async init(){
        let auth = await this.authenticate().catch((e)=>{
            console.log("calendarPlugin","error authenticating calendarPlugin with google servers",e);
        });
        try{
            if (!await this.calendarListIncludesCalendar(this.calendarId)){
                await this.addCalendarById(this.calendarId)
            }
        }catch(e){
            console.log('calendarPlugin','error',"could not add calendar",this.calendarId,e)
        }
         
        return auth
    }

 /**
     * handler for input events into plugin. handles all incoming events 
     * by checking input.event (eventType) and possible input.data (eventData).
     * Recommended to wrap into try/catch to not crash dingsda2mex on plugin errors, 
     * unless wanted of course.
     * @param {*} input 
     * @returns 
     */
 async handler(input){
    console.log("calendarPlugin received input");
    try{
        const eventType = input.event;
        const event = input.data;
  
        if( eventType == 'created'){
            let itemList = '';
            if (event.items){
                for (const item of event.items){
                    itemList = itemList + 
                    `${dingsdaconfig.mainWebClientUrl ? dingsdaconfig.mainWebClientUrl+'/Detail?id=' :''}${item.id}\n\n`
                }
            }
            const gCalEvent = {
                id:event.id ? event.id.replace(/-/g,'') : null,
                summary:event.name,
                description:'link: \n'+dingsdaconfig.mainWebClientUrl+'/Reservations?search='+event.id+'\n\n'+(event.notes ? event.notes+'\n\nITEMS:\n'+itemList : null),
                start: {
                    dateTime: event.from,
                    //timeZone: 'GMT+1',
                  },
                  end: {
                    dateTime: event.till,
                    //timeZone: 'GMT+1',
                  },
            }
            const calEventAdded = await this.insertEventToCalendar(this.calendarId,gCalEvent);
            console.log('created gCal Event',calEventAdded)
            if (calEventAdded) return true
            else return false
        }
        if( eventType == 'updated'){
            // fetch original reservation from db
            const res = await this.sql.getReservationById(event.id,event.userRequesting);
            if (!res || res.length < 1) {
                console.log("could not update gCalReservation because reservation does not exist");
                return false
            }
            const dbReservation = res[0];
            // turn dbReservation into gCalEvent
            let updatedgCalEvent = await this.turnDBReservation2gCalEvent(dbReservation);
            await this.updateEventInCalendar(this.calendarId,updatedgCalEvent)
        }
        if( eventType == 'deleted'){
            const reservationDeleted = await this.deleteEventFromCalendar(this.calendarId,{id:event.id.replace(/-/g,'')})
            console.log("reservationDeleted",reservationDeleted)
        }
    }catch(e){console.log("Reservations2gCalendar Plugin had an error",e);}
    }

    ///////////////////////////////////
    ///// PLUGIN SPECIFIC METHODS /////

    async turnDBReservation2gCalEvent(reservation){
        let itemList = '';
        if (reservation.items){
            for (const itemid of reservation.items){
                itemList = itemList + 
                `${dingsdaconfig.mainWebClientUrl ? dingsdaconfig.mainWebClientUrl+'/Detail?id=' :''}${itemid}\n\n`
            }
        }
        return {
            id:reservation.id ? reservation.id.replace(/-/g,'') : null,
            summary:reservation.name,
            description:'link: \n'+dingsdaconfig.mainWebClientUrl+'/Reservations?search='+reservation.id+'\n\n'+(reservation.notes ? reservation.notes+'\n\nITEMS:\n'+itemList : null),
            start: {
                dateTime: reservation.reserved[0].replace(" ","T")+"Z", // reformatting to match proper timestamp without timezone
                //timeZone: 'GMT+1',
              },
              end: {
                dateTime:  reservation.reserved[1].replace(" ","T")+"Z", // reformatting to match proper timestamp without timezone
                //timeZone: 'GMT+1',
              }
        }
    }

    async authenticate(){
        let jwtClient = new this.google.auth.JWT(
            this.privatekey.client_email,
            null,
            this.privatekey.private_key,
            [
                //  'https://www.googleapis.com/auth/spreadsheets','https://www.googleapis.com/auth/drive',
                'https://www.googleapis.com/auth/calendar'
            ]);

        await new Promise((resolve,reject)=>{
            // authenticate request
            jwtClient.authorize(function (err, tokens) {
                if (err) {
                    console.log(err);
                    reject(err);
                } else {
                    console.log("calendarPlugin","Successfully connected!");
                    resolve();
                }
            });
        })
        

        // bind auth object to all following requests (avoids having to hand auth into every request)
        this.google.options({ auth: jwtClient });

        return
    }

    async insertEventToCalendar(calendarId,event){
        try{
            const res = await this.calendar.events.insert({
                //auth: jwtClient,
                calendarId: calendarId,
                resource:event
            });
            return true
        }catch(e){
            if (e.code == 409) {return console.log("insertEventToCalendar",`could not create event ${event.id} because ${e.message}`)}
            throw e
        }
    }

    async updateEventInCalendar(calendarId,event){
        try{
            const res = await this.calendar.events.update({calendarId:calendarId,eventId:event.id,requestBody:event});
            return true
        }catch(e){
            throw e
        }
    }

    async deleteEventFromCalendar(calendarId,event){
        try{
            await this.calendar.events.delete({
                calendarId,
                eventId:event.id, 
                sendUpdates: 'all'
            })
            console.log(`reservation '${event.summary}' (${event.id}) got deleted`);
            return true
        }catch(e){
            if(e.code == 410){return console.log("deleteEventFromCalendar","could not delete item because",e.message)}
            console.log("deleteEventFromCalendar","error",e);
            throw e
        }
    }

    /**
     * list all calendars of the service account connected
     */
    async listCalendars(){
        try{
            let res = await this.calendar.calendarList.list()
            return res.data
        }catch(e){
            console.log("listCalendars","error",e)
        }  
    }

    async getEventFromCalendar(calendarId,eventId){
        try{
            let res = await this.calendar.events.get({calendarId,eventId});
            console.log("getCalendars","got event",res);
        }catch(e){
            console.log("getCalendars","error")
        }
    }

    async listAllEventsByCalendar(calendarId){
        try{
            const res = await this.calendar.events.list({
                //auth: jwtClient,
                calendarId: calendarId
            });
            //console.log(res);
            let events = res.data.items;
            if (!events){return console.log("no events found. events returned",events);}
            if (events.length == 0) {
                console.log('No events found.');
            } else {
                console.log('Event from Google Calendar:');
                for (let event of events) {
                    console.log('Event name: %s, Creator name: %s, Create date: %s', event.summary, event.creator.displayName, event.start.date);
                    console.log('event',event)
                }
                return events
            }
        }catch(e){
            console.log("listAllEventsByCalendar","error",e);
            throw e
        }   
    }

    /**
     * add calendar by id after being invited to it (must need human to add service-account email to calendar)
     * @param {string} id - id of calendar created for the service account to manage. must have been created 
     *                      manually and must have added service account email with full permissions
     */
    async addCalendarById(id){
        try{
            let res = await this.calendar.calendarList.insert({requestBody:{id}});
            console.log('addCalendarById result:',res);
            return res
        }catch(e){
            console.log('addCalendarById error:',e);
            throw e
        }
    }


    async calendarListIncludesCalendar(calendarId){   
        try{
            let foundCalendar = await this.calendar.calendarList.get({calendarId})
            //console.log("checkIfCalendarListIncludesCalendar",foundCalendar.data)
            if (foundCalendar.status == 200) return true
            throw new Error('unknown error in checkIfCalendarListIncludesCalendar')
        }catch(e){
            console.log("checkIfCalendarListIncludesCalendar","error",e.response.status,e.response.stausText);
            if (e.response.status == 404) return false
            throw e
        }    
    }

}

module.exports = plugin;