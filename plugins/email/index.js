
let config = {disabled:true}

try{
 config = require('./config.json');    
}catch(e){
    console.warn("email plugin present but no config")
}

const plugin = class {
    constructor(plugindata,sql) {
        this.name = 'emailPlugin';
        if (config.disabled){
            this.disabled = true;
            return
        }
        console.log("constructing",this.name);
        this.nodemailer = require('nodemailer');

        this.topic = ['email','forgotpassword'];
        this.sql = sql;
        this.transporter = this.nodemailer.createTransport(config.emailconfig);
        // must also have methods: init() and handler(inputevent) (see below)
    }
    
    ///////////////////////////////////
    // mandatory lifecycle methods: ///
    ///////////////////////////////////

    async init(){
        return true
    }

 /**
     * handler for input events into plugin. handles all incoming events 
     * by checking input.event (eventType) and possible input.data (eventData).
     * Recommended to wrap into try/catch to not crash dingsda2mex on plugin errors, 
     * unless wanted of course.
     * @param {*} input 
     * @returns 
     */
    async handler(input){
        console.log(this.name,"received input",input);
        if (input.domain == 'forgotpassword' && input.data.email && input.data.webhook){
            this.sendMail(`Hi ${input.data.user}!`+
            `Visit ${input.data.webhook} to change your password. (Valid for max. 5 min)`
            ,input.data.email,(r)=>console.log(r),(e)=>console.error("could not send mail",e))
        }
    }

    ///////////////////////////////////
    ///// PLUGIN SPECIFIC METHODS /////

    
    sendMail(content="That was easy!",to,cb,err)
    {
        console.log("trying to send mail");
        let mailOptions = {
            from: config.fromemail,
            to,
            subject: 'dingsda2mex your request',
            text: content
        };
        
        this.transporter.sendMail(mailOptions, function(error, info){
            if (error) {
            console.log(error);
            err(error)
            } else {
            console.log('Email sent: ' + info.response);
            cb(info.response)
            }
        });

    }

}

module.exports = plugin;