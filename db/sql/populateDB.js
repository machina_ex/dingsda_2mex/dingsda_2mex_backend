// populates postgresql DB with random mockdata

const faker = require('faker');
const SQL = require("../postgresql.js");
const lodash = require('lodash');
const Log = require('../../logging');
const log = new Log();
const sql = new SQL();
global.log = log;

faker.locale = "de";

const exampleTagArray = [ // TODO: test this and then test _searchByName with some thousand items in the DB
  "cable",
  "XLR",
  "DMX",
  "costume",
  "size:s",
  "size:m",
  "size:l",
  "size:xl",
  "size:42",
  "size:40",
  "size:38",
  "size:36",
  "ontour:LoL",
  "ontour:ROP",
  "ontour:Endgame",
  "ontour"
]

async function populate(){

  for(var i=1;i<=10; i++){
    let newuserId = faker.internet.userName()//faker.datatype.uuid();

    await sql.addUser(
      {
        id: newuserId,
        email: faker.internet.email(),
        name: newuserId,
        password: "test1234"
      }
    )
      .catch(err => console.log(err.stack))
    
    for(var j=1;j<=100; j++){
      let itemname = faker.commerce.product();
      await sql.addItem({
        name: itemname,
        owners: [newuserId],
        //atLocation: {id:"germany_uuid"},
        // atLocation: {
        //   new:{
        //     name:faker.address.city(),
        //     geography:{
        //       type:"Point",
        //       coordinates: faker.address.nearbyGPSCoordinate([13.698819938754799, 54.38264812066428],10,true).map(Number) , // TODO: use gpsCoordinate near x, x
        //     },
        //     radius:50
        //   }
        // },
        insideof:'4',
        count: faker.datatype.number(3),
        weight: faker.datatype.float(2),
        description:faker.random.words(7),
        tags: getRandomlySizedArrayOf(()=>getRandomItemFromArray(exampleTagArray),1,5),
        //inPossessionOf:"uuid_bene",
        visibleTo:["uuid_machina_ex"],
        pictures:[faker.image.imageUrl(null,400,itemname.toLowerCase(),true)]
      })
    }
        
  }

}

populate();



function getRandomlySizedArrayOf(itemcreatingfunction,min,max){
  let outputArray = [];
  for (let i = lodash.random(min,max) ; i > 0 ; i--){
    outputArray.push(itemcreatingfunction())
  }
  return outputArray
}

function getRandomItemFromArray(array){
  let randomIndex = lodash.random(array.length-1)
  return array[randomIndex]
}



/*
- create 500 places with visible to at least 1 but up to 200 users. (why not all to all?) 
--> shouldnt it be implicit that all places are publicly visible unless specified otherwise?
    - create usergroup "all" which will be associated to all users automatically on user creation

- Make 1000 fake users 
- with a random set of items between 0 to 2000
    - of which they can be inside of another item up to max 4 containers.
*/
