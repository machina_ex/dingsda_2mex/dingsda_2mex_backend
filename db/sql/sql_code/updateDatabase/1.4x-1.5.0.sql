
-- DROP UNIQUE constraint on availability to allow multi item reservations:
ALTER TABLE availability
DROP CONSTRAINT availability_id_key;
