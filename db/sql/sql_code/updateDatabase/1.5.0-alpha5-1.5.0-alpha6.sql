
-- UPDATE FROM v.1.5.0-alpha5-v.1.5.0-alpha6

ALTER TABLE reservations ADD COLUMN IF NOT EXISTS confirmedby TEXT;

UPDATE reservations SET confirmedby = a.confirmedby  FROM availability a WHERE a.reservationid = reservations.id;

ALTER TABLE reservations ALTER COLUMN confirmedby  SET NOT NULL;

ALTER TABLE availability DROP COLUMN confirmedby CASCADE;

--- SIMPLE BUT EFFECTIVE
CREATE OR REPLACE VIEW complete_reservations_with_itemidArray AS
	SELECT r.*,ARRAY_AGG(a.itemid) as items FROM reservations r
	JOIN availability a ON (a.reservationid = r.id)
	GROUP BY r.id, a.reserved, r.name, r.notes, r.reservedfor, r.confirmedby
;
--- SAME BUT WITH FULL ITEM INFOS IN JSON ARRAY:
CREATE OR REPLACE VIEW complete_reservations_with_itemsJSON AS
	SELECT r.*,JSON_AGG(i) as items FROM reservations r
	JOIN availability a ON (a.reservationid = r.id)
	JOIN all_items i ON (a.itemid = i.id)
	GROUP BY r.id, a.reserved, r.name, r.notes, r.reservedfor, r.confirmedby
;

-- RECREATE all_reservations
CREATE OR REPLACE VIEW all_reservations
AS
	SELECT * FROM reservations
	JOIN availability ON reservations.id = availability.reservationid
;

-- SELECT * FROM availability