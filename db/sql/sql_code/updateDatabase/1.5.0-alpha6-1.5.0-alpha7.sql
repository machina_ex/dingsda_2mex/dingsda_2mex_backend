-- UPDATE from 1.5.0-alpha6 to 1.5.0-alpha7


ALTER TABLE reservations ADD COLUMN reserved TSRANGE;
-- move first reserved from availability to reservations
UPDATE reservations SET reserved = a.reserved  FROM availability a WHERE a.reservationid = reservations.id;
ALTER TABLE reservations ALTER COLUMN reserved  SET NOT NULL;
-- TODO: rename availability table to item2reservation
ALTER TABLE IF EXISTS availability RENAME TO item2reservation;
ALTER TABLE IF EXISTS item2reservation DROP column reserved CASCADE;

ALTER TABLE item2reservation DROP CONSTRAINT IF EXISTS availability_id_key; -- this came from the old name availability and is not needed

ALTER TABLE item2reservation ADD CONSTRAINT "item2reservation_reservationid-reservations_id" FOREIGN KEY ("reservationid") REFERENCES "reservations"("id");
ALTER TABLE item2reservation ADD CONSTRAINT "item2reservation_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE item2reservation ADD CONSTRAINT "unique_itemid-reservationid" UNIQUE (itemid,reservationid);

-- RECREATE ALL VIEWS that depended on the above tables:
DROP VIEW IF EXISTS all_reservations;
CREATE VIEW all_reservations
AS
	SELECT * FROM reservations
	JOIN item2reservation ON reservations.id = item2reservation.reservationid
;
-- SIMPLE BUT EFFECTIVE COMPLETE RESERVATIONS VIEW
CREATE OR REPLACE VIEW complete_reservations_with_itemidArray AS
	SELECT r.*,ARRAY_AGG(a.itemid) as items FROM reservations r
	JOIN item2reservation a ON (a.reservationid = r.id)
	GROUP BY r.id, r.reserved, r.name, r.notes, r.reservedfor, r.confirmedby
;
--- COMPLETE RESERVATIONS VIEW WITH FULL ITEM INFOS IN JSON ARRAY:
CREATE OR REPLACE VIEW complete_reservations_with_itemsJSON AS
	SELECT r.*,JSON_AGG(i) as items FROM reservations r
	JOIN item2reservation a ON (a.reservationid = r.id)
	JOIN all_items i ON (a.itemid = i.id)
	GROUP BY r.id, r.reserved, r.name, r.notes, r.reservedfor, r.confirmedby
;

-- create view availability with layout of old availability
CREATE OR REPLACE VIEW availability AS 
SELECT i.itemid, r.reserved, i.reservationid, i.created FROM item2reservation i JOIN reservations r ON i.reservationid = r.id
;

-- ADD function/trigger for updates on reservations: Check if all items of this reservation available for update time, otherwise block.
CREATE OR REPLACE FUNCTION check_all_reservation_items_available()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
declare
    blockingReservations TEXT[] = ARRAY[''];
BEGIN
	IF -- DOES A ROW IN AVAILABILITY EXIST THAT BLOCKS ANY OF THE ITEMS BEING ASSOCIATED WITH THIS RESERVATION?
		EXISTS(
			SELECT * FROM (SELECT a.itemid FROM availability a WHERE a.reservationid = NEW.id) as a1 -- GET ALL itemids of reservation to be updated
			JOIN availability a2 ON (a2.itemid = a1.itemid) -- check all availabilities for those items
			WHERE NEW.id != a2.reservationid -- WHERE its not the RESERVATION TO BE UPDATED BUT...
			AND NEW.reserved && a2.reserved -- ...it OVERLAPS WITH IT's TIMEFRAME
		)
	THEN
		blockingReservations = (SELECT array_agg('reservation: '||a2.reservationid || ',   item:'|| a2.itemid) FROM (SELECT a.itemid FROM availability a WHERE a.reservationid = NEW.id) as a1 -- GET ALL itemids of reservation to be updated
			JOIN availability a2 ON (a2.itemid = a1.itemid) -- check all availabilities for those items
			WHERE NEW.id != a2.reservationid -- WHERE its not the RESERVATION TO BE UPDATED BUT...
			AND NEW.reserved && a2.reserved -- ...it OVERLAPS WITH IT's TIMEFRAME
						);
		RAISE EXCEPTION 'some items are already reserved in this timeframe by reservations:' USING DETAIL=blockingReservations;
	END IF;
	RETURN NEW;	
END;
$$
;
-- add UPDATE/INSERT Trigger Blocking item2reservation adding if item is already reserved in same timeframe.
CREATE OR REPLACE FUNCTION item_already_reserved()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
declare
    blockingTime TSRANGE = tsrange('1/1/9999, 9:12:00 AM','1/1/9999, 9:12:01 AM','[]');
BEGIN
	IF
	-- IS THE NEW ITEM RESERVED ALREADY RESERVED IN THE GIVEN TIMEFRAME?
		(SELECT EXISTS (
			SELECT a.reservationid,a.itemid, a.reserved from availability a -- using availability VIEW
			WHERE (
				a.itemid = NEW.itemid
				AND a.reservationid != NEW.reservationid
			)
			AND (SELECT reserved FROM reservations WHERE id = NEW.reservationid) && a.reserved
		))
	THEN
		-- FIXME: REDUNDANT!!!! this should be done only once and used in IF clause as well CTE? other way?
		blockingTime = (SELECT reserved FROM availability a WHERE (a.itemid = NEW.itemid
				AND a.reservationid != NEW.reservationid
			)
			AND (SELECT reserved FROM reservations WHERE id = NEW.reservationid) && a.reserved  LIMIT 1); 
	
		RAISE EXCEPTION 'item % already reserved',NEW.itemid USING DETAIL='['||blockingTime||']';
	END IF;

	RETURN NEW;
END;
$$
;
CREATE TRIGGER TR_itemAlreadyReserved BEFORE INSERT OR UPDATE
  ON item2reservation
  FOR EACH ROW
  EXECUTE PROCEDURE item_already_reserved()
;

CREATE TRIGGER TR_checkAllReservationItemsAvailable BEFORE UPDATE
  ON reservations
  FOR EACH ROW
  EXECUTE PROCEDURE check_all_reservation_items_available()
;

-- create trigger to let us delete reservations via the VIEW:
create trigger DELETE_RESERVATION INSTEAD OF DELETE
  on all_reservations
  for each row
  execute procedure deleteReservation_fn();
