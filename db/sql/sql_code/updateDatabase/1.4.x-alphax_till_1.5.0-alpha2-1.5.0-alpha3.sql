-- REFACTORING RESERVATIONS/AVAILABILITY

------ REFACTOR FROM 1.4.x-alphax or 1.5.0-alpha2 to 1.5.0-alpha3

CREATE EXTENSION IF NOT EXISTS "uuid-ossp"; -- for uuid_generate_v4()

CREATE TABLE "reservations" (
	"id" TEXT UNIQUE DEFAULT uuid_generate_v4(),
	"notes" TEXT,
	"reservedfor" TEXT,
	"name" TEXT
);

ALTER TABLE reservations ADD CONSTRAINT "reservations_reservedFor-users_id" FOREIGN KEY ("reservedfor") REFERENCES "users"("id");

INSERT INTO reservations (SELECT DISTINCT id, notes, reservedfor FROM availability);
ALTER TABLE availability RENAME COLUMN id TO reservationid;
ALTER TABLE availability ALTER COLUMN reservationid TYPE TEXT;
ALTER TABLE availability 
	DROP COLUMN notes,
	DROP COLUMN reservedfor;
ALTER TABLE availability ADD CONSTRAINT "availability_reservationid-reservations_id" FOREIGN KEY ("reservationid") REFERENCES "reservations"("id");
	
SELECT * FROM reservations;
SELECT * FROM availability;

-- SELECT IN VIEW SUMMARIZING RESERVATIONS AND ITEMS AS BEFORE SELECT * FROM availability would have done:
DROP VIEW IF EXISTS all_reservations;
CREATE VIEW all_reservations
AS
	SELECT * FROM reservations
	JOIN availability ON reservations.id = availability.reservationid
;
-- create trigger to let us delete reservations via the VIEW:
CREATE OR REPLACE FUNCTION deleteReservation_fn()
  RETURNS trigger AS '
BEGIN
  DELETE FROM availability WHERE reservationid=OLD.reservationid;
  DELETE FROM reservations WHERE id=OLD.reservationid;
  RETURN NULL;
END; ' language plpgsql;

create trigger DELETE_RESERVATION INSTEAD OF DELETE
  on all_reservations
  for each row
  execute procedure deleteReservation_fn();

-- -- FROM NOW ON, INSERTS NEED TO BE TWOFOLD AND MOST OF THE TIMES WITHIN ONE TRANSACTION:

-- --- NEW RESERVATION (reservationid does not yet exist):

-- INSERT into reservations VALUES
-- ('blabla','these are freebla notes',null);

-- INSERT into availability VALUES 
-- ('02364957-8219-44fb-97e1-955a1281ea2d',tsrange('2019-12-17T16:57:00.000Z','2019-12-18T16:57:00.000Z','[]'),'philip','blabla');

-- --- ADD ITEMS to RESERVATION (reservation id exists already)

-- INSERT into availability VALUES 
-- ('101d63c2-4a08-4587-a93d-9460dcee24b9',tsrange('2019-12-17T16:57:00.000Z','2019-12-18T16:57:00.000Z','[]'),'philip','blabla');

-- DELETE COMPLETE RESERVATION CAN BE DONE WITH all_reservations directly:

-- DELETE FROM all_reservations 
-- WHERE reservationid='blabla'

-- -- UPDATES HAVE TO BE DONE ON THE CORRECT TABLE(S) DEPENDING ON CONTEXT:
-- SELECT * from all_reservations

-- UPDATE reservations
-- 	SET notes = 'new notes for an old reservation'
-- 	WHERE id = 'blabla';

-- UPDATE availability
-- 	SET reserved = tsrange('2001-12-17T16:57:00.000Z','2001-12-18T16:57:00.000Z')
-- 	WHERE reservationid = 'blabla';


-- -- TIDY UP AFTER REMOVING ITEMS FROM RESERVATIONS
-- DELETE FROM reservations r
-- WHERE NOT EXISTS
-- (
--   SELECT * 
--     FROM availability a
--    WHERE a.reservationid = r.id
-- )

-- -- etc...



