-- drop unique constraint on column billingreference in table items

ALTER TABLE IF EXISTS items 
  DROP CONSTRAINT IF EXISTS items_billingreference_key;