DROP TABLE IF EXISTS "printers";
DROP TABLE IF EXISTS "printers2users";

CREATE TABLE "printers" (
"id" TEXT PRIMARY KEY NOT NULL UNIQUE,
"prints" TEXT[] DEFAULT ARRAY[]::text[]
);

CREATE TABLE "printers2users" (
"printerid" TEXT NOT NULL,
"userid" TEXT NOT NULL
);


INSERT INTO printers
VALUES 
('testid',ARRAY['www.machinaex.com']),
('machinaprinter',ARRAY['www.commons.com']),
('benesprinter',ARRAY['www.bene.com'])
;

INSERT INTO printers2users
VALUES 
('testid','uuid_philip'),
('machinaprinter','uuid_machina_ex'),
('benesprinter','uuid_bene')
;
