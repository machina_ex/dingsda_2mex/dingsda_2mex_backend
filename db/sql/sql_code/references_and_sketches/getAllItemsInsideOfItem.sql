-- QUERY: get all items inside of Item with id of the first container (tree traversal forward)

WITH RECURSIVE cte_name(depth,itemid,itemname,itemInsideOf) AS(
    SELECT 0, items.id, items.name, items.insideof -- non-recursive term
		FROM items
		WHERE items.id = '4' -- start item
    UNION
    SELECT depth+1,items.id, items.name,items.insideof -- recursive term
	 FROM cte_name, items
	WHERE items.insideof=itemid
	AND depth < 5 -- limits depth on request but should rather be done on CREATE/UPDATE
) SELECT * FROM cte_name;


-- TODO: CHECK constraint against loop on CREATE and UPDATE of items.
