-- get distance in meters between 2 geographies

SELECT ST_Distance(gg1, gg2) AS distance 
FROM (
	SELECT (SELECT geography from places WHERE name = 'home philip') as gg1,
	 (SELECT geography from places WHERE name = 'lager') as gg2  
) AS foo; 