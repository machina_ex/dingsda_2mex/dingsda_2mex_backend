SELECT *
    FROM(
        -- get AllReservations of an item with all days per reservation aggregated into array days:

        SELECT itemid, id, array_agg(days) as days, reserved, notes, created, reservedfor,confirmedby FROM (

          SELECT id, (generate_series(ft.fro,ft.till,'1 day'::interval)) as days, itemid, reserved, notes, created, reservedfor,confirmedby from(
            SELECT id,lower(reserved) fro, upper(reserved) till, itemid, reserved, notes, created, reservedfor,confirmedby FROM availability av
            WHERE 
			  itemid = '0364108b-bc15-47b6-b4b3-bf49924865a1'
			AND 
			  upper(reserved)::Date <= NOW() -- Only if does not end in the past
            GROUP BY id, itemid, reserved, fro, till, notes, created, reservedfor,confirmedby
          ) as ft
        GROUP BY id, fro, till, itemid, reserved, notes, created, reservedfor,confirmedby
        ) as ft2
        GROUP BY id, itemid, reserved, notes, created, reservedfor,confirmedby
    ) as ft3
	
	
-- 	SELECT lower(reserved), upper(reserved), reserved from availability
