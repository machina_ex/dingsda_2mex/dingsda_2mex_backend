-- get item records from geography

select i.*, p.name "location.placename",
		--ST_AsGeoJSON(p.geography) "location.geoJSON", 
		p.geography "location.geography"
		, getContainerPath(i.id)"location.insideofPath" -- optional
from items i
full join item2place b
on b.itemid = i.id
left join places p -- left to exclude places that have no items attached to them
on b.placeid = p.id
WHERE ST_DWithin(
	p.geography, 
	ST_MakePoint(9.195806991415235, 48.904612588045254)::geography, -- center of search radius
	30000 -- radius in meters
)

; 
-- possible addon/variation depending on query needs would be
---- to add getcontainerPath(p.id) to SELECT