--- RETURN PERMISSIONS/ROLE (FULL,MOVE,EDIT,NONE)
--- TODO: ADD EDIT (via extra Table and entry in all_items)

SELECT 
	(
		CASE 
			WHEN 
			(
				owned && (SELECT array_agg(id) FROM getUserAliases('uuid_philip'))
			) 
			THEN 
			'FULL' 
			WHEN
			(
				possessedby && (SELECT array_agg(id) FROM getUserAliases('uuid_philip'))
			)
			THEN
			'MOVE'
			ELSE
			'NONE' 
		END) 
	AS
	role from all_items 
	WHERE id = '6'