SELECT i.* FROM findItemsWithIntersectingGeography(ST_GeomFromGeoJSON(
'
{"type":"Polygon","coordinates":[[[13.389167537097821,52.51989748316265],[13.388710821491417,52.51985346641213],[13.388298814916592,52.51972572507647],[13.387971849008544,52.51952676402138],[13.387761929468342,52.51927605974922],[13.38768960312202,52.51899815359455],[13.38776194725045,52.518720249197784],[13.387971877780599,52.518469549527794],[13.388298843688649,52.518270594161315],[13.388710839273525,52.51814285742785],[13.389167537097821,52.5180988424352],[13.38962423492212,52.51814285742785],[13.390036230506997,52.518270594161315],[13.390363196415047,52.518469549527794],[13.390573126945196,52.518720249197784],[13.390645471073624,52.51899815359455],[13.390573144727302,52.51927605974922],[13.390363225187102,52.51952676402138],[13.390036259279052,52.51972572507647],[13.389624252704225,52.51985346641213],[13.389167537097821,52.51989748316265]]]}
'
)) as found

INNER JOIN (
	SELECT * FROM all_items
	WHERE
	( owned && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- check if owned by user or group user is part of
      --OR visible2 && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
      OR possessedby && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
    ) 
)  as i
 ON i.id = found.id

-- -- LIMIT 1000



-- SELECT i.* FROM (SELECT * FROM all_items 
-- WHERE
-- ( owned && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- check if owned by user or group user is part of
--   --OR visible2 && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
--   OR possessedby && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
-- ) )as i

-- RIGHT JOIN (
-- 	SELECT * FROM findItemsWithIntersectingGeography(ST_GeomFromGeoJSON(
-- 	'
-- 	{"type":"Polygon","coordinates":[[[13.389167537097821,52.51989748316265],[13.388710821491417,52.51985346641213],[13.388298814916592,52.51972572507647],[13.387971849008544,52.51952676402138],[13.387761929468342,52.51927605974922],[13.38768960312202,52.51899815359455],[13.38776194725045,52.518720249197784],[13.387971877780599,52.518469549527794],[13.388298843688649,52.518270594161315],[13.388710839273525,52.51814285742785],[13.389167537097821,52.5180988424352],[13.38962423492212,52.51814285742785],[13.390036230506997,52.518270594161315],[13.390363196415047,52.518469549527794],[13.390573126945196,52.518720249197784],[13.390645471073624,52.51899815359455],[13.390573144727302,52.51927605974922],[13.390363225187102,52.51952676402138],[13.390036259279052,52.51972572507647],[13.389624252704225,52.51985346641213],[13.389167537097821,52.51989748316265]]]}
-- 	'
-- 	))
-- )  as found
-- ON i.id = found.id