
-- INSERT INTO all_items  (id, name,owned,insideof,tags,visible2) 
-- 				VALUES ('testid2','deleteme',ARRAY['philip'],'02364957-8219-44fb-97e1-955a1281ea2d',ARRAY['testit','test'],ARRAY['machina_ex']);

-- UPDATE all_items SET name = 'Ubiquity ToughSwitch 6 Port LAN', tags =ARRAY['network/netzwerk'], count =1 WHERE id = '0364108b-bc15-47b6-b4b3-bf49924865a1'

-- INSERT INTO all_items VALUES ('testid2','testname');
-- DELETE FROM all_items WHERE id='testid2';

CREATE OR REPLACE FUNCTION insertItem() 
	RETURNS TRIGGER 
	LANGUAGE PLPGSQL
AS 
$$
DECLARE owner TEXT;
DECLARE tagtext TEXT;
DECLARE tagid TEXT;
DECLARE possessor TEXT;
DECLARE visible TEXT;
BEGIN
   -- trigger logic
   IF TG_OP = 'INSERT' THEN
   	IF NEW.owned IS NULL THEN RAISE EXCEPTION 'cannot create item without owners (must be array in column owned)'; END IF;
	IF (NEW.insideof IS NULL AND NEW."location.id" IS NULL) THEN RAISE EXCEPTION 'cannot create item without location or insideof'; END IF;
	
	IF (NEW.count IS NULL) THEN NEW.count = 1;END IF;
	IF (NEW.weight IS NULL) THEN NEW.weight = 0;END IF;
	IF (NEW.value IS NULL) THEN NEW.value = 0;END IF;
	IF (NEW.price IS NULL) THEN NEW.price = 0;END IF;
	IF (NEW.length IS NULL) THEN NEW.length = 0;END IF;
	IF (NEW.width IS NULL) THEN NEW.width = 0;END IF;
	IF (NEW.depth IS NULL) THEN NEW.depth = 0;END IF;
	
	INSERT INTO 
	items (id,name,description,count,weight,insideof,lastmodifiedby,pictures,value,price,length,width,depth,countryoforigin,billingreference,invoicedate) 
	VALUES (NEW.id,NEW.name,NEW.description,NEW.count,NEW.weight,NEW.insideof,NEW.lastmodifiedby,NEW.pictures,NEW.value,NEW.price,NEW.length,NEW.width,NEW.depth,NEW.countryoforigin,NEW.billingreference,NEW.invoicedate);

	IF (NEW.insideof IS NULL AND NEW."location.id" IS NOT NULL AND NEW."location.id" <> '') 
	THEN 
		INSERT INTO item2place (itemid,placeid) VALUES (NEW.id,NEW."location.id");
	END IF;
	
	FOREACH owner IN ARRAY NEW.owned LOOP
		INSERT INTO owners2item (userid,itemid) VALUES (owner,NEW.id);
	END LOOP;
	
	IF (NEW.possessedby IS NOT NULL) THEN
		FOREACH possessor IN ARRAY NEW.possessedby LOOP
			INSERT INTO item2possessors (userid,itemid) VALUES (possessor,NEW.id);
		END LOOP;
	END IF;
	
	IF (NEW.visible2 IS NOT NULL) THEN
		FOREACH visible IN ARRAY NEW.visible2 LOOP
			INSERT INTO itemvisible2users (userid,itemid) VALUES (visible,NEW.id);
		END LOOP;
	END IF;
	
	IF (NEW.tags IS NOT NULL) THEN
		FOREACH tagtext IN ARRAY NEW.tags LOOP
			tagid = (SELECT id FROM tags WHERE tags.tag = tagtext);
			IF (tagid IS NULL) THEN 
				tagid = uuid_generate_v4();
				INSERT INTO tags (id,tag) VALUES (tagid,tagtext);
			END IF;
			
			INSERT INTO tags2item (tagid,itemid) VALUES (tagid,NEW.id);
		END LOOP;	
	END IF;
	
	RETURN NEW;
	
  ELSIF TG_OP = 'UPDATE' THEN
  
	  UPDATE items SET 
	  	name = COALESCE(NEW.name,OLD.name),
		insideof = COALESCE(NEW.insideof,OLD.insideof),
		count = COALESCE(NEW.count,OLD.count),
		weight = COALESCE(NEW.weight,OLD.weight),
		price = COALESCE(NEW.price,OLD.price),
		value = COALESCE(NEW.value,OLD.value),
		description = COALESCE(NEW.description,OLD.description),
		lastmodified = CURRENT_TIMESTAMP,
		lastmodifiedby = COALESCE(NEW.lastmodifiedby,OLD.lastmodifiedby),
		pictures = COALESCE(NEW.pictures,OLD.pictures),
		length = COALESCE(NEW.length,OLD.length),
		width = COALESCE(NEW.width,OLD.width),
		depth = COALESCE(NEW.depth,OLD.depth),
		countryoforigin = COALESCE(NEW.countryoforigin,OLD.countryoforigin),
		billingreference = COALESCE(NEW.billingreference,OLD.billingreference),
		invoicedate = COALESCE(NEW.invoicedate,OLD.invoicedate)
	  WHERE id = NEW.id;
	  
	  IF (NEW.possessedby IS NOT NULL) THEN
	  	-- remove all old possessors
		DELETE FROM item2possessors WHERE itemid = NEW.id;
		-- add new possessors
		FOREACH possessor IN ARRAY NEW.possessedby LOOP
			INSERT INTO item2possessors (userid,itemid) VALUES (possessor,NEW.id);
		END LOOP;
	  END IF;
	  
	  IF (NEW.visible2 IS NOT NULL) THEN
	  	-- remove all old visible permits
		DELETE FROM itemvisible2users WHERE itemid = NEW.id;
		-- add new visible permits
		FOREACH visible IN ARRAY NEW.visible2 LOOP
			INSERT INTO itemvisible2users (userid,itemid) VALUES (visible,NEW.id);
		END LOOP;
	  END IF;
	  
	  IF (NEW.owned IS NOT NULL) THEN
	  	IF (cardinality(NEW.owned)=0) THEN 
			RAISE EXCEPTION 'owned cannot be empty';
		END IF;
	  	-- remove all old owners
		DELETE FROM owners2item WHERE itemid = NEW.id;
		-- add new owners
		FOREACH owner IN ARRAY NEW.owned LOOP
			INSERT INTO owners2item (userid,itemid) VALUES (owner,NEW.id);
		END LOOP;
	  END IF;
	  
	  IF (NEW.tags IS NOT NULL) THEN
	  	-- remove all old tags
		DELETE FROM tags2item WHERE itemid = NEW.id;
		-- add new tags
		FOREACH tagtext IN ARRAY NEW.tags LOOP
			tagid = (SELECT id FROM tags WHERE tags.tag = tagtext);
			IF (tagid IS NULL) THEN 
				tagid = uuid_generate_v4();
				INSERT INTO tags (id,tag) VALUES (tagid,tagtext);
			END IF;

			INSERT INTO tags2item (tagid,itemid) VALUES (tagid,NEW.id);
		END LOOP;
	  END IF;
	  
   RETURN NEW;
   
  ELSIF TG_OP = 'DELETE' THEN
  	DELETE FROM item2place WHERE itemid = OLD.id;
	DELETE FROM owners2item WHERE itemid = OLD.id;
	DELETE FROM item2possessors WHERE itemid = OLD.id;
	DELETE FROM item2place WHERE itemid = OLD.id;
	DELETE FROM tags2item WHERE itemid = OLD.id;
	DELETE FROM itemvisible2users WHERE itemid = OLD.id;
	DELETE FROM items WHERE id= OLD.id;
   RETURN NULL;
  END IF;
  
  RETURN NEW;
  
END;
$$
;

CREATE OR REPLACE TRIGGER trigger_all_items_insert 
	INSTEAD OF 
		INSERT OR UPDATE OR DELETE
	ON all_items
		FOR EACH ROW 
	EXECUTE PROCEDURE insertItem()
;