-- QUERY: get all parent items of startitem (reverse tree)
WITH RECURSIVE cte_name(depth,itemid,itemname,itemcontainer) AS(
    SELECT 0, items.id, items.name, items.insideof -- non-recursive term
		FROM items
		WHERE items.id = '3' -- start item
    UNION
    SELECT depth+1,items.id, items.name,items.insideof -- recursive term
	 FROM cte_name, items
	WHERE items.id=itemcontainer
	AND depth < 5 -- limits depth on request but should rather be done on CREATE/UPDATE
) SELECT * FROM cte_name;

-- problem: if recursion in tree might run forever
-- possible solution for development: limit depth of n
-- possible solution for production: 
------ see: https://stackoverflow.com/a/31745768/2602592
------ but: looks expensive. so should be rather used on db update, create