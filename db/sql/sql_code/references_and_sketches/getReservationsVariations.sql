DELETE FROM all_reservations

SELECT * FROM all_reservations

SELECT * FROM reservations
SELECT * FROM availability


-- ALL ITEMS BLOCKED/RESERVED FOR A GIVEN TIMEFRAME:

SELECT a.reserved, i.* FROM all_items i
LEFT JOIN availability a ON a.itemid=i.id -- ONLY IF AVAILABILTY FILTER PRESENT
WHERE
(
	a.reserved IS NOT NULL -- ONLY IF AVAILABILTY FILTER PRESENT:
	AND 
	(
	  a.reserved && '[2021-01-13T12:49,2021-01-18T18:52)'::tsrange  -- reservation is overlap with filtered range ==> items are reserved
	  -- NOT a.reserved && '[2023-01-13T18:49,2023-01-18T18:52)'::tsrange -- reservation is NOT overlapping with filtered range ==> items are available
	)
	AND -- ONLY IF EXCLUDE PAST RESERVATIONS
	(
	  upper(a.reserved)::Date >= NOW() -- exclude past reservations
	)
)

---------------
----------------------

-- ALL ITEMS RESERVED VISIBLE TO USER or their groups
SELECT 
a.id as reservationid, a.notes, a.reserved, a.reservedfor, a.confirmedby,
i.id as itemid, i.name, i.owned, i.visible2, i.possessedby, i.description, 
  i.count, i.weight, i.length, i.depth, i.value, i.price, i.billingreference, i.invoicedate, i.countryoforigin, i.insideof, 
  i."location.insideofArray", i.pictures,i.iscontainer,i.tags, i."location.placename", i."location.id", i"location.geoJSON"
FROM availability a
JOIN all_items i ON a.itemid=i.id
WHERE
( 
  i.owned && (SELECT array_agg(id) FROM getUserAliases('philip')) 
  OR i.visible2 && (SELECT array_agg(id) FROM getUserAliases('philip')) 
  OR i.possessedby && (SELECT array_agg(id) FROM getUserAliases('philip'))
)
AND upper(a.reserved)::Date >= NOW() - INTERVAL '1 day'

-- INSERT into availability VALUES 
-- ('02364957-8219-44fb-97e1-955a1281ea2d',tsrange('2022-12-17T16:57:00.000Z','2022-12-18T16:57:00.000Z','[]'),null,'philip','test','2646f16d-ba13-4209-80b2-d973e4d471d9')
-- RETURNING *

-- DELETE FROM availability

-- SELECT * FROM availability 


-- ALL ITEMS RESERVED ON INSTANCE
SELECT * FROM availability a
JOIN all_items i ON a.itemid=i.id

-- ALL ITEMS RESERVED VISIBLE TO USER or their groups
SELECT * FROM availability a
JOIN all_items i ON a.itemid=i.id
WHERE	
( 
	i.owned && (SELECT array_agg(id) FROM getUserAliases('lasse')) -- check if owned by user or group user is part of
	OR i.visible2 && (SELECT array_agg(id) FROM getUserAliases('lasse')) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
	OR i.possessedby && (SELECT array_agg(id) FROM getUserAliases('lasse')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
)

-- ALL ITEMS RESERVED OWNED OR POSSESSED BY USER or their groups
SELECT * FROM availability a
JOIN all_items i ON a.itemid=i.id
WHERE	
( 
	i.owned && (SELECT array_agg(id) FROM getUserAliases('philip')) -- check if owned by user or group user is part of
	OR i.possessedby && (SELECT array_agg(id) FROM getUserAliases('philip')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
)
