-- get all possible rootContainers
SELECT * FROM all_items --might be a lot faster using items and location tables instead

--where id = '3'
WHERE insideof is null
AND "location.geoJSON" is not null
AND owned && (SELECT array_agg(id) FROM getUserAliases('uuid_philip'))
OR possessedby && (SELECT array_agg(id) FROM getUserAliases('uuid_philip'))
--OR visible2 && (SELECT array_agg(id) FROM getUserAliases('uuid_philip'))