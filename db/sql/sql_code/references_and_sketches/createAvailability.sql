CREATE EXTENSION if not exists btree_gist;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP table if exists availability;

CREATE TABLE "availability" (
	"itemid" TEXT NOT NULL, 
	"reserved" TSRANGE NOT NULL,
	"reservedfor" TEXT, 
	"confirmedby" TEXT NOT NULL, -- TODO: ADD CHECK IF owner (on nodejs level not here)
	"notes" TEXT,
	"id" UUID UNIQUE DEFAULT uuid_generate_v4(),
	"created" TIMESTAMP DEFAULT NOW()
);

ALTER TABLE availability ADD EXCLUDE USING gist (itemid WITH =, reserved WITH &&);
ALTER TABLE availability ADD CONSTRAINT "availability_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE availability ADD CONSTRAINT "availability_reservedFor-users_id" FOREIGN KEY ("reservedfor") REFERENCES "users"("id");
ALTER TABLE availability ADD CONSTRAINT "availability_confirmedBy-users_id" FOREIGN KEY ("confirmedby") REFERENCES "users"("id");

-- INSERT into availability VALUES 
-- 	('03ebbcb1-4369-4d2e-aa5c-a586831ea764',tsrange('NOW','TOMORROW','[]'),'machina_ex','philip',null),
-- 	('03ebbcb1-4369-4d2e-aa5c-a586831ea764',tsrange('YESTERDAY','YESTERDAY','[]'),null,'philip','reserved for abigail (no account)')
-- ;

-- SELECT * FROM availability;

-- DELETE FROM availability WHERE true