
-- get AllReservations of an item with all days per reservation aggregated into array days:

SELECT * FROM(
SELECT itemid, id, array_agg(days) as days, reserved, notes, created, reservedfor,confirmedby FROM (

	SELECT id, (generate_series(ft.fro,ft.till,'1 day'::interval)) as days, itemid, reserved, notes, created, reservedfor,confirmedby from(
		SELECT id,lower(reserved) fro, upper(reserved) till, itemid, reserved, notes, created, reservedfor,confirmedby FROM availability av
		WHERE itemid = '0364108b-bc15-47b6-b4b3-bf49924865a1'
		GROUP BY id, itemid, reserved, fro, till, reserved, notes, created, reservedfor,confirmedby
	) as ft
GROUP BY id, fro, till, itemid, reserved, notes, created, reservedfor,confirmedby
) as ft2
GROUP BY id, itemid, reserved, notes, created, reservedfor,confirmedby
) as ft3
-- get AllReservations of an item in one large aggregated array:

SELECT array_agg(days) as days FROM (

	SELECT id, (generate_series(ft.fro,ft.till,'1 day'::interval)) as days from(
		SELECT id,lower(reserved) fro, upper(reserved) till  FROM availability av
		WHERE itemid = '0364108b-bc15-47b6-b4b3-bf49924865a1'
		
	) as ft
	
) as ft2


-- select (generate_series('2012-06-29', '2012-07-03', '1 day'::interval)::date);

