-- get item records including location data
---- uses items2place for joining places table with items table


select i.*, p.name "location.placename",
		--ST_AsGeoJSON(p.geography) "location.geoJSON", 
		p.geography "location.geography"
		, getContainerPath(i.id)"location.insideofPath" -- optional
from items i
full join item2place b
on b.itemid = i.id
left join places p -- left to exclude places that have no items attached to them
on b.placeid = p.id
-- without the following line/lines this table gives a view of ALL items including geodata
--where b.placeid='2'
--where b.geography

-- TODO: add full item data
-- TODO: same query based on ST_CONTAINS of geography
-- TODO: get geography from parent container if not present. 
---- (This should probably be done on the client by just checking if location.geography == null
----  and then get location from first id in location.insideofPath. Not really DBs job...)