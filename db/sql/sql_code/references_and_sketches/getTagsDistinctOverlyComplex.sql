SELECT DISTINCT t.tag as tag
    FROM tags t

    INNER JOIN tags2item
      ON tags2item.tagid = t.id
    LEFT JOIN all_items as items
      ON items.id = tags2item.itemid

    WHERE 
      ( owned && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- check if owned by user or group user is part of
        OR visible2 && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
        OR possessedby && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
      )
	 AND 
      items.name ILIKE
      '%Kabel%'
    

    GROUP BY t.tag,items.id, items.name