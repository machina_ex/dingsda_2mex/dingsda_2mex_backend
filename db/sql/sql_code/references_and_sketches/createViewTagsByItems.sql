-- test sql. should also be found (and might be more up to date) inside of createDB.sql

DROP VIEW IF EXISTS tags_by_items;

CREATE VIEW tags_by_items
AS
----------------------------------

SELECT items.id as itemid, array_agg(t.tag) as tags
FROM tags t

INNER JOIN tags2item
	ON tags2item.tagid = t.id
LEFT JOIN items
	ON items.id = tags2item.itemid
	
GROUP BY items.id
	
-----------------------------------