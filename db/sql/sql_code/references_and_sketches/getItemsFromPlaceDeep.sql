WITH RECURSIVE cte_name(entryid) AS(
		-- get item records from geography -- non-recursive termin
	select i.id
	from items i
	full join item2place b
	on b.itemid = i.id
	left join places p -- left to exclude places that have no items attached to them
	on b.placeid = p.id
	WHERE ST_DWithin(
		p.geography, 
		ST_MakePoint(9.195806991415235, 48.904612588045254)::geography, -- center of search radius
		300000 -- radius in meters
	)
      -- recursive term
	UNION
	SELECT itemid FROM cte_name, getAllItemsInsideOf(entryid)
) SELECT * FROM cte_name;

-- TODO: Put into DB function
-- TODO: get full item records as in getItemRecordsIncludingLocation.sql