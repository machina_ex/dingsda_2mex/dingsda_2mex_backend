-- QUERY: get all parent items of startitem (reverse tree) and return one value string in path form
WITH RECURSIVE cte_name(depth,itemid,itemcontainer,path) AS(
    SELECT 0, items.id, items.insideof, '' -- non-recursive term
		FROM items
		WHERE items.id = '3' -- start item
    UNION
    SELECT depth+1,items.id, items.insideof, CONCAT_WS('/',itemcontainer,path) -- recursive term
	 FROM cte_name, items
	WHERE items.id=itemcontainer
	AND depth < 5 -- limits depth on request but should rather be done on CREATE/UPDATE
) SELECT path FROM cte_name -- only path
WHERE itemcontainer IS NULL -- only last path
;

-- TODO: find out if this can be turned into a function or otherwise be called by another query

-- problem: if recursion in tree might run forever
-- possible solution for development: limit depth
-- possible solution for production: 
------ see: https://stackoverflow.com/a/31745768/2602592
------ but: looks expensive. so should be rather used on db update, create

-- TODO: same query backwards: from item downwards: what do i contain? 
---- for that: search for manager tree cte 