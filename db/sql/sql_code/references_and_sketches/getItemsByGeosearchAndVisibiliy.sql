SELECT i.* FROM findItemWithinRadius(ST_MakePoint(13.453544998069274, 52.50567738202224)::geography,100000) as found

INNER JOIN (
	SELECT * FROM all_items
	WHERE
	( owned && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- check if owned by user or group user is part of
      --OR visible2 && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
      OR possessedby && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
    ) 
)  as i
 ON i.id = found.id

-- LIMIT 1000