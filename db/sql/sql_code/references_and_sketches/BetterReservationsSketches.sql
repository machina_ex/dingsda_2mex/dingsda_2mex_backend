-- CONTINUE HERE: add this to getReservations and getReservationsById!

-- WOW this could be it: 
---- Get all reservations that include at least one item owned or possessed by user/their groups or where reservation itself is for or by user/group
-- TODO: NICE TO HAVE: add all items owned as json ? keep all itemids tho no matter who owns ! 


SELECT DISTINCT c.* FROM complete_reservations_with_itemidArray c
INNER JOIN all_items i ON (i.id = ANY (c.items))
WHERE 
	ARRAY[confirmedby] <@ ANY(SELECT array_agg(id) FROM getUserAliases('philip'))
OR
	ARRAY[reservedFor] <@ ANY(SELECT array_agg(id) FROM getUserAliases('philip'))
OR
(
	owned && (SELECT array_agg(id) FROM getUserAliases('philip'))
	OR possessedby && (SELECT array_agg(id) FROM getUserAliases('philip'))
	-- OR visible2 && (SELECT array_agg(id) FROM getUserAliases($1))
)



-- UPDATE FROM v.1.5.0-alpha5-v.1.5.0-alpha6

ALTER TABLE reservations ADD COLUMN IF NOT EXISTS confirmedby TEXT;

UPDATE reservations SET confirmedby = a.confirmedby  FROM availability a WHERE a.reservationid = reservations.id;

ALTER TABLE reservations ALTER COLUMN confirmedby  SET NOT NULL;

ALTER TABLE availability DROP COLUMN confirmedby CASCADE;

--- SIMPLE BUT EFFECTIVE
CREATE OR REPLACE VIEW complete_reservations_with_itemidArray AS
	SELECT r.*,ARRAY_AGG(a.itemid) as items FROM reservations r
	JOIN availability a ON (a.reservationid = r.id)
	GROUP BY r.id, a.reserved, r.name, r.notes, r.reservedfor, r.confirmedby
;
--- SAME BUT WITH FULL ITEM INFOS IN JSON ARRAY:
CREATE OR REPLACE VIEW complete_reservations_with_itemsJSON AS
	SELECT r.*,JSON_AGG(i) as items FROM reservations r
	JOIN availability a ON (a.reservationid = r.id)
	JOIN all_items i ON (a.itemid = i.id)
	GROUP BY r.id, a.reserved, r.name, r.notes, r.reservedfor, r.confirmedby
;