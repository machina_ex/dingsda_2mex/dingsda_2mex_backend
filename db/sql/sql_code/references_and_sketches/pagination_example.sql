-- PAGINATION VIA ORDERED COLUMNS (even better with index for the ordered column)

-- STEP 1:

SELECT * FROM all_items
      WHERE name
      ILIKE
      '%%'
-- 	  AND all_items.id > 'bf26b818-400b-4fbc-ad7a-3cbb75a8d1b7'
	  ORDER BY all_items.id
	  LIMIT 100
	  
	  
-- STEP 2:
SELECT * FROM all_items
      WHERE name
      ILIKE
      '%%'
	  AND all_items.id > 'bf26b818-400b-4fbc-ad7a-3cbb75a8d1b7' -- where this was the id of the 100th item from step 1
	  ORDER BY all_items.id
	  LIMIT 100
	  