-- SEARCH FULL TEXT FOR TAG ontour with project "lol"

SELECT * 
FROM 
	tags 
WHERE 
	to_tsvector(tag) 
	@@ to_tsquery('ontour: & lol');