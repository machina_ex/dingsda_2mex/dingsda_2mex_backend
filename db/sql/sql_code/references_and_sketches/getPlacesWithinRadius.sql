-- get places within radius



SELECT p.id inPlace, p.geography geography, p.name placeName FROM places p
WHERE ST_DWithin( 
	p.geography, 
	ST_MakePoint(13.443541727580785, 52.59586689100561)::geography, -- center of search radius
	30000 -- radius in meters
)
; 