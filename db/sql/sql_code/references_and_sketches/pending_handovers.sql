SELECT * FROM all_items 

SELECT * FROM pending_handovers


-- CREATE PENDING HANDOVER
INSERT INTO pending_handovers
VALUES 
('arg','7','uuid_philip','uuid_lasse',CURRENT_TIMESTAMP)

-- GET ALL PENDING HANDOVERS BY USER
SELECT * FROM pending_handovers 
WHERE from = 'uuid_philip'

-- GET ALL PENDING HANDOVERS THAT ARE ADDRESSED TO USER
SELECT * FROM pending_handovers 
WHERE to = 'uuid_philip'

-- CANCEL PENDING HANDOVER BY HANDOVER ID
DELETE FROM pending_handovers
WHERE id = 'arg'

-- CANCEL PENDING HANDOVER BY USER
DELETE FROM pending_handovers
WHERE from = 'uuid_philip'

-- CANCEL PENDING HANDOVER BY ITEM
DELETE FROM pending_handovers
WHERE item_id = '7'