-- SELECT * FROM places WHERE id NOT IN (
-- 	SELECT placeid from item2place
-- )

-- DELETE places that are not linked to any items

DELETE FROM places WHERE id NOT IN (
	SELECT placeid from item2place
)