select * from all_items i -- this is a view!
	--where owned && ARRAY['uuid_philip', 'uuid_machina_ex']
	where owned && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- check if owned by user or group user is part of
     -- OR visible2 && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
    OR possessedby && (SELECT array_agg(id) FROM getUserAliases('uuid_philip')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 

-- TODO: check if smarter to do a join here first with array_agg(getUserAliases()) 
-------- might be more efficient for querying getUserAliases() several times seems expensive.