--DROP FUNCTION getContainerArray(startitem text);

CREATE OR REPLACE FUNCTION getContainerArray(startitem text)
--RETURNS text
RETURNS table (
		depth int,
		itemid text,
		itemcontainer text
) 


AS $$
DECLARE ret TEXT; -- var to be returned later
BEGIN
	-- QUERY: get all parent items of startitem (reverse tree) and return one value string in path form
	RETURN QUERY 
	WITH RECURSIVE cte_name(_depth,_itemid,_itemcontainer) AS(
		SELECT 0, items.id, items.insideof -- non-recursive term
			FROM items
			WHERE items.id = startitem -- start item
		UNION
		SELECT _depth+1,items.id, items.insideof -- recursive term
		 FROM cte_name, items
		WHERE items.id=_itemcontainer
		AND _depth < 50 -- limits depth on request but should rather be done on CREATE/UPDATE
	) 
	SELECT * FROM cte_name as c
	ORDER BY c._depth DESC
	;
END;

$$
LANGUAGE plpgsql
;