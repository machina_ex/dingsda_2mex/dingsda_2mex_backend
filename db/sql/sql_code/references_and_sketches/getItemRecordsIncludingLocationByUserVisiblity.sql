-- get item records including location data
---- uses items2place for joining places table with items table

select distinct i.*, 
		getContainerPath(i.id)"location.insideofPath",
		array_remove(array_agg(owners2item.userid),NULL) as owned, -- array of owners. needs GROUPBY at bottom. possibly replaceable by view but unclear if that would be smart
		array_remove(array_agg(itemvisible2users.userid),NULL) as visible2, --  array of users this is visible for. needs GROUPBY at bottom. possibly replaceable by view but unclear if that would be smart
		array_remove(array_agg(item2possessors.userid),NULL) as possessedBy,
		p.name "location.placename",
		ST_AsGeoJSON(p.geography) "location.geoJSON"
		--p.geography "location.geography",
from items i
full join item2place b
on b.itemid = i.id
left join places p -- left to exclude places that have no items attached to them
on b.placeid = p.id

left join owners2item
	on owners2item.itemid = i.id
left join 
	itemvisible2users
	on itemvisible2users.itemid = i.id
left join
	item2possessors
	on item2possessors.itemid = i.id
where 
  --owners2item.userid='uuid_philip'
  owners2item.userid in (SELECT * FROM getUserAliases('uuid_philip')) -- check if owned by user or group user is part of
-- OR itemvisible2users.userid in (SELECT * FROM getUserAliases('uuid_philip')) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
OR item2possessors.userid in (SELECT * FROM getUserAliases('uuid_philip')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 

-- without the following line/lines this table gives a view of ALL items including geodata
--where b.placeid='2'
--where b.geography
GROUP BY i.id, p.name, p.geography;
-- TODO: add columns to indicate if owned, borrowed or only visible

-- TODO: add full item data
-- TODO: same query based on ST_CONTAINS of geography
-- TODO: get geography from parent container if not present. 
---- (This should probably be done on the client by just checking if location.geography == null
----  and then get location from first id in location.insideofPath. Not really DBs job unless geosearch...)