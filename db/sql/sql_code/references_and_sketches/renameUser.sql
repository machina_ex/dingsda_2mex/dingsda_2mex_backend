
ALTER TABLE "items" DROP CONSTRAINT IF EXISTS "items_insideof-items_id" ;
ALTER TABLE "items" DROP CONSTRAINT IF EXISTS "items_lastmodifiedby-users_id";
ALTER TABLE "owners2item" DROP CONSTRAINT IF EXISTS "owners2item_userid-users_id";
ALTER TABLE "owners2item" DROP CONSTRAINT IF EXISTS "owners2item_itemid-items_id";
ALTER TABLE "owners2item" DROP CONSTRAINT IF EXISTS "owners2item_user_not_public";
ALTER TABLE "item2possessors" DROP CONSTRAINT IF EXISTS "item2possessors_userid-users_id";
ALTER TABLE "item2possessors" DROP CONSTRAINT IF EXISTS "item2possessors_itemid-items_id";
ALTER TABLE "item2possessors" DROP CONSTRAINT IF EXISTS "item2possessors_user_not_public";
ALTER TABLE "users2knownplaces" DROP CONSTRAINT IF EXISTS "users2knownplaces_idUsers-users_id";
ALTER TABLE "users2knownplaces" DROP CONSTRAINT IF EXISTS "users2knownplaces_idPlace-places_id";
ALTER TABLE "item2place" DROP CONSTRAINT IF EXISTS "item2place_itemid-items_id" ;
ALTER TABLE "item2place" DROP CONSTRAINT IF EXISTS "item2place_placeid-places_id";
ALTER TABLE "itemvisible2users" DROP CONSTRAINT IF EXISTS "itemvisible2users_userid-users_id" ;
ALTER TABLE "itemvisible2users" DROP CONSTRAINT IF EXISTS "itemvisible2users_itemid-items_id";
ALTER TABLE "member2group" DROP CONSTRAINT IF EXISTS "member2group_groupid-users_id";
ALTER TABLE "member2group" DROP CONSTRAINT IF EXISTS "member2group_userid-users_id";
ALTER TABLE "tags2item" DROP CONSTRAINT IF EXISTS "tags2item_itemid-items_id";
ALTER TABLE "tags2item" DROP CONSTRAINT IF EXISTS "tags2item_tagid-tags_id" ;


ALTER TABLE "items" ADD CONSTRAINT "items_insideof-items_id" FOREIGN KEY ("insideof") REFERENCES "items"("id");
ALTER TABLE "items" ADD CONSTRAINT "items_lastmodifiedby-users_id" FOREIGN KEY ("lastmodifiedby") REFERENCES "users"("id") ON UPDATE CASCADE;
ALTER TABLE "owners2item" ADD CONSTRAINT "owners2item_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id") ON UPDATE CASCADE;
ALTER TABLE "owners2item" ADD CONSTRAINT "owners2item_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id") ON UPDATE CASCADE;
ALTER TABLE "item2possessors" ADD CONSTRAINT "item2possessors_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id") ON UPDATE CASCADE;
ALTER TABLE "item2possessors" ADD CONSTRAINT "item2possessors_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id") ON UPDATE CASCADE;
ALTER TABLE "users2knownplaces" ADD CONSTRAINT "users2knownplaces_idUsers-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id") ON UPDATE CASCADE;
ALTER TABLE "users2knownplaces" ADD CONSTRAINT "users2knownplaces_idPlace-places_id" FOREIGN KEY ("placeid") REFERENCES "places"("id") ON UPDATE CASCADE;
ALTER TABLE "itemvisible2users" ADD CONSTRAINT "itemvisible2users_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id") ON UPDATE CASCADE;
ALTER TABLE "itemvisible2users" ADD CONSTRAINT "itemvisible2users_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id") ON UPDATE CASCADE;
ALTER TABLE "member2group" ADD CONSTRAINT "member2group_groupid-users_id" FOREIGN KEY ("groupid") REFERENCES "users"("id") ON UPDATE CASCADE;
ALTER TABLE "member2group" ADD CONSTRAINT "member2group_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id") ON UPDATE CASCADE;


UPDATE users
SET id = 'elisa'
WHERE id = 'uuid_elisa';


ALTER TABLE "items" DROP CONSTRAINT IF EXISTS "items_insideof-items_id" ;
ALTER TABLE "items" DROP CONSTRAINT IF EXISTS "items_lastmodifiedby-users_id";
ALTER TABLE "owners2item" DROP CONSTRAINT IF EXISTS "owners2item_userid-users_id";
ALTER TABLE "owners2item" DROP CONSTRAINT IF EXISTS "owners2item_itemid-items_id";
ALTER TABLE "owners2item" DROP CONSTRAINT IF EXISTS "owners2item_user_not_public";
ALTER TABLE "item2possessors" DROP CONSTRAINT IF EXISTS "item2possessors_userid-users_id";
ALTER TABLE "item2possessors" DROP CONSTRAINT IF EXISTS "item2possessors_itemid-items_id";
ALTER TABLE "item2possessors" DROP CONSTRAINT IF EXISTS "item2possessors_user_not_public";
ALTER TABLE "users2knownplaces" DROP CONSTRAINT IF EXISTS "users2knownplaces_idUsers-users_id";
ALTER TABLE "users2knownplaces" DROP CONSTRAINT IF EXISTS "users2knownplaces_idPlace-places_id";
ALTER TABLE "item2place" DROP CONSTRAINT IF EXISTS "item2place_itemid-items_id" ;
ALTER TABLE "item2place" DROP CONSTRAINT IF EXISTS "item2place_placeid-places_id";
ALTER TABLE "itemvisible2users" DROP CONSTRAINT IF EXISTS "itemvisible2users_userid-users_id" ;
ALTER TABLE "itemvisible2users" DROP CONSTRAINT IF EXISTS "itemvisible2users_itemid-items_id";
ALTER TABLE "member2group" DROP CONSTRAINT IF EXISTS "member2group_groupid-users_id";
ALTER TABLE "member2group" DROP CONSTRAINT IF EXISTS "member2group_userid-users_id";
ALTER TABLE "tags2item" DROP CONSTRAINT IF EXISTS "tags2item_itemid-items_id";
ALTER TABLE "tags2item" DROP CONSTRAINT IF EXISTS "tags2item_tagid-tags_id" ;



ALTER TABLE "items" ADD CONSTRAINT "items_insideof-items_id" FOREIGN KEY ("insideof") REFERENCES "items"("id");
ALTER TABLE "items" ADD CONSTRAINT "items_lastmodifiedby-users_id" FOREIGN KEY ("lastmodifiedby") REFERENCES "users"("id");
ALTER TABLE "owners2item" ADD CONSTRAINT "owners2item_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "owners2item" ADD CONSTRAINT "owners2item_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "owners2item" ADD CONSTRAINT "owners2item_user_not_public" CHECK(userid != '*');
ALTER TABLE "item2possessors" ADD CONSTRAINT "item2possessors_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "item2possessors" ADD CONSTRAINT "item2possessors_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "item2possessors" ADD CONSTRAINT "item2possessors_user_not_public" CHECK(userid != '*');
ALTER TABLE "users2knownplaces" ADD CONSTRAINT "users2knownplaces_idUsers-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "users2knownplaces" ADD CONSTRAINT "users2knownplaces_idPlace-places_id" FOREIGN KEY ("placeid") REFERENCES "places"("id");
ALTER TABLE "item2place" ADD CONSTRAINT "item2place_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "item2place" ADD CONSTRAINT "item2place_placeid-places_id" FOREIGN KEY ("placeid") REFERENCES "places"("id");
ALTER TABLE "itemvisible2users" ADD CONSTRAINT "itemvisible2users_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "itemvisible2users" ADD CONSTRAINT "itemvisible2users_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "member2group" ADD CONSTRAINT "member2group_groupid-users_id" FOREIGN KEY ("groupid") REFERENCES "users"("id");
ALTER TABLE "member2group" ADD CONSTRAINT "member2group_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "tags2item" ADD CONSTRAINT "tags2item_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "tags2item" ADD CONSTRAINT "tags2item_tagid-tags_id" FOREIGN KEY ("tagid") REFERENCES "tags"("id");


-- afterwards: rename data/uploads/directory from username_old to username_new AND/OR make a symlink
-- if symlink is not possible: you will also have to relink all item pictures column array items 
-- containing username_old in their path
-- you can find those with this:

-- SELECT id, picture
-- FROM (
--     SELECT id, unnest(pictures) picture
--     FROM items) x
-- WHERE picture LIKE '%uuid_machina_ex%'



