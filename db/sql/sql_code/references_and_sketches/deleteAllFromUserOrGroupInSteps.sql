-- ATTENTION: this is a work in progress. this needs to be tested and thought through.
-- this might delete your database instead. do not use

DELETE FROM item2place -- 1st
WHERE EXISTS
(
	SELECT * FROM all_items 
	JOIN items I ON I.id = all_items.id

	WHERE 
-- 	visible2 && (SELECT array_agg(id) FROM getUserAliases('*')) 
-- 	AND
	owned && ARRAY['rimini']
)

DELETE FROM owners2item --2nd
WHERE EXISTS
(
	SELECT * FROM all_items 
	JOIN items I ON I.id = all_items.id

	WHERE 
-- 	visible2 && (SELECT array_agg(id) FROM getUserAliases('*')) 
-- 	AND
	owned && ARRAY['rimini']
)

DELETE FROM tags2item --3rd
WHERE EXISTS
(
	SELECT * FROM all_items 
	JOIN items I ON I.id = all_items.id

	WHERE 
-- 	visible2 && (SELECT array_agg(id) FROM getUserAliases('*')) 
-- 	AND
	owned = '{}'
)

DELETE FROM itemvisible2users --4th
WHERE EXISTS
(
	SELECT * FROM all_items 
	JOIN items I ON I.id = all_items.id

	WHERE 
-- 	visible2 && (SELECT array_agg(id) FROM getUserAliases('*')) 
-- 	AND
	owned = '{}'
)

DELETE FROM item2reservation --5th
WHERE EXISTS
(
	SELECT * FROM all_items 
	JOIN items I ON I.id = all_items.id

	WHERE 
-- 	visible2 && (SELECT array_agg(id) FROM getUserAliases('*')) 
-- 	AND
	owned = '{}'
)

DELETE FROM item2possessors --5th
WHERE EXISTS
(
	SELECT * FROM all_items 
	JOIN items I ON I.id = all_items.id

	WHERE 
-- 	visible2 && (SELECT array_agg(id) FROM getUserAliases('*')) 
-- 	AND
	owned = '{}'
)


DELETE FROM items -- 5th
WHERE EXISTS
(
	SELECT * FROM all_items 
	JOIN items I ON I.id = all_items.id

	WHERE
-- 	visible2 && (SELECT array_agg(id) FROM getUserAliases('*')) 
-- 	AND
	owned = '{}'
	OR 
	I.lastmodifiedby = 'rimini'
)

-- SELECT * FROM member2group

DELETE FROM member2group
WHERE groupid = 'rimini' OR userid = 'rimini'

DELETE FROM users
WHERE id = 'rimini'

-- SELECT * FROM users