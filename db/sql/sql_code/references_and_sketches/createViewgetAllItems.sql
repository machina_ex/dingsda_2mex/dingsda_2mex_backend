-- test sql. should also be found (and might be more up to date) inside of createDB.sql

DROP VIEW IF EXISTS all_items;

CREATE VIEW all_items
AS
----------------------------------
	
select distinct i.*, 
 		(SELECT array_agg(itemcontainer) FROM getContainerArray(i.id) WHERE itemcontainer IS NOT NULL) as insideofArray,
-- 		getContainerPath(i.id)"location.insideofPath",
-- 		(SELECT array_agg(itemid) FROM getAllItemsInsideOf(i.id) WHERE itemid != i.id) as containingTotal,
-- 		(SELECT array_agg(id) FROM (SELECT * FROM items WHERE items.insideof = i.id) as i2) as containing,
		tags.tags,
		array_remove(array_agg(owners2item.userid),NULL) as owned, -- array of owners. needs GROUPBY at bottom. possibly replaceable by view but unclear if that would be smart
		array_remove(array_agg(itemvisible2users.userid),NULL) as visible2, --  array of users this is visible for. needs GROUPBY at bottom. possibly replaceable by view but unclear if that would be smart
		array_remove(array_agg(item2possessors.userid),NULL) as possessedBy,
		p.id "location.id",
		p.name "location.placename",
		ST_AsGeoJSON(p.geography) "location.geoJSON",
		p.geography "location.geography"
from items i
full join item2place b
	on b.itemid = i.id 
left join places p -- left to exclude places that have no items attached to them
	on b.placeid = p.id
left join owners2item
	on owners2item.itemid = i.id
left join 
	itemvisible2users
	on itemvisible2users.itemid = i.id
left join
	item2possessors
	on item2possessors.itemid = i.id
left join
	tags_by_items tags
	on tags.itemid = i.id
	
GROUP BY i.id, p.id, p.name, p.geography, tags.tags

-----------------------------------

