-- get all items either owned by and/or visible to user by userid

-- see getAllItemsIncludingLocationByUserVisibility.sql for combined item list

-- todo: add recursion for groups that users are members of (backlog)
-- todo: add recursion for groups that users are inControl of (backlog)

select items.id as itemid, items.name itemname, items.insideof, 
owners2item.userid as owned, itemvisible2users.userid as visible2



from items
left join owners2item
	on owners2item.itemid = items.id
left join 
	itemvisible2users
	on itemvisible2users.itemid = items.id
	
where 
owners2item.userid='uuid_philip'
OR
itemvisible2users.userid='uuid_philip'

-- todo: add recursion for groups that users are members of (backlog)
-- todo: add recursion for groups that users are inControl of (backlog)