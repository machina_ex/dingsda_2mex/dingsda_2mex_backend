SELECT DISTINCT t.tag as tag
FROM tags t

INNER JOIN tags2item
	ON tags2item.tagid = t.id
LEFT JOIN items
	ON items.id = tags2item.itemid
	
GROUP BY t.tag,items.id