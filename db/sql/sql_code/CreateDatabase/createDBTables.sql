CREATE EXTENSION IF NOT EXISTS postgis;
-- DROP TABLE IF EXISTS "users" cascade ;
-- DROP TABLE IF EXISTS "items" cascade ;
-- DROP TABLE IF EXISTS "owners2item" cascade ;
-- DROP TABLE IF EXISTS "item2possessors" cascade ;
-- DROP TABLE IF EXISTS "places" cascade ;
-- DROP TABLE IF EXISTS "users2knownplaces" cascade ;
-- DROP TABLE IF EXISTS "item2place" cascade ;
-- DROP TABLE IF EXISTS "itemvisible2users" cascade ;
-- DROP TABLE IF EXISTS "member2group" cascade ;

-- DROP TABLE IF EXISTS "tags" cascade;
-- DROP TABLE IF EXISTS "tags2item" cascade;
-- DROP TABLE IF EXISTS "pending_handovers" cascade;
--DROP TABLE IF EXISTS "tagsvisible2users" cascade;
--DROP TABLE IF EXISTS "sizes" cascade;
--DROP TABLE IF EXISTS "sizes2item" cascade;



CREATE TABLE "users" (
"id" TEXT PRIMARY KEY NOT NULL UNIQUE,
"email" TEXT NOT NULL,
"name" TEXT NOT NULL,
"hashedpassword" TEXT NOT NULL,
"public_contact_details" TEXT
);

CREATE TABLE "items" (
"id" TEXT PRIMARY KEY NOT NULL UNIQUE,
"name" TEXT NOT NULL,
"description" TEXT ,
"count" INT NOT NULL DEFAULT 1,
"weight" FLOAT NOT NULL DEFAULT 0,
"insideof" TEXT CHECK (insideof != id), -- TODO: add cte to check that id is also nowhere in the insideofpath
"lastmodified" TIMESTAMPTZ,
"lastmodifiedby" TEXT,
"pictures" TEXT[],
"value" MONEY NOT NULL DEFAULT 0,
"price" MONEY NOT NULL DEFAULT 0,
"length" INT NOT NULL DEFAULT 0,
"width" INT NOT NULL DEFAULT 0,
"depth" INT NOT NULL DEFAULT 0,
"countryoforigin" TEXT,
"billingreference" TEXT,
"invoicedate" TEXT
);

CREATE TABLE "owners2item" (
"userid" TEXT NOT NULL,
"itemid" TEXT NOT NULL);

CREATE TABLE "item2possessors" (
"userid" TEXT NOT NULL,
"itemid" TEXT NOT NULL);

CREATE TABLE "places" (
"id" TEXT PRIMARY KEY NOT NULL,
"geography" GEOGRAPHY NOT NULL,
"name" TEXT NOT NULL);

CREATE TABLE "users2knownplaces" (
"userid" TEXT NOT NULL,
"placeid" TEXT NOT NULL);

CREATE TABLE "item2place" (
"itemid" TEXT NOT NULL UNIQUE,
"placeid" TEXT NOT NULL);

CREATE TABLE "itemvisible2users" (
"itemid" TEXT NOT NULL,
"userid" TEXT NOT NULL);

CREATE TABLE "member2group" (
"userid" TEXT NOT NULL,
"groupid" TEXT NOT NULL);

CREATE TABLE "tags" (
"id" TEXT PRIMARY KEY NOT NULL,
"tag" TEXT UNIQUE NOT NULL);

CREATE TABLE "tags2item" (
"tagid" TEXT NOT NULL,
"itemid" TEXT NOT NULL,
UNIQUE (tagid, itemid) -- make combo of tagid and itemid unique.
);

CREATE TABLE "pending_handovers" (
"id" TEXT PRIMARY KEY NOT NULL UNIQUE,
"item_id" TEXT NOT NULL UNIQUE,
"from" TEXT NOT NULL,
"to" TEXT NOT NULL,
"timestamp" TIMESTAMPTZ
);

ALTER TABLE "pending_handovers" ADD CONSTRAINT "pending_handovers_item_id-items_id" FOREIGN KEY ("item_id") REFERENCES "items"("id");
ALTER TABLE "pending_handovers" ADD CONSTRAINT "pending_handovers_from-user_id" FOREIGN KEY ("from") REFERENCES "users"("id");
ALTER TABLE "pending_handovers" ADD CONSTRAINT "pending_handovers_to-user_id" FOREIGN KEY ("to") REFERENCES "users"("id");


--- ADD RESERVE / BOOKING / AVAILABILITY -----
CREATE EXTENSION if not exists btree_gist;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- DROP table if exists reservations;

CREATE TABLE "reservations" (
	"id" TEXT UNIQUE DEFAULT uuid_generate_v4(),
	"notes" TEXT,
	"reservedfor" TEXT,
	"name" TEXT,
	"confirmedby" TEXT NOT NULL, -- TODO: ADD CHECK IF owner (on nodejs level not here)
	"reserved" TSRANGE NOT NULL
);

ALTER TABLE reservations ADD CONSTRAINT "reservations_reservedFor-users_id" FOREIGN KEY ("reservedfor") REFERENCES "users"("id");
ALTER TABLE reservations ADD CONSTRAINT "reservations_confirmedBy-users_id" FOREIGN KEY ("confirmedby") REFERENCES "users"("id");

-- DROP table if exists item2reservation;

CREATE TABLE "item2reservation" ( -- connects items and reservations while also reserving timespans on item level
	"itemid" TEXT NOT NULL, 
---	"reserved" TSRANGE NOT NULL, -- moved to reservations
-- 	"reservedfor" TEXT,
--	"confirmedby" TEXT NOT NULL, -- moved to reservations
-- 	"notes" TEXT,
-- 	"id" UUID DEFAULT uuid_generate_v4(),
	"reservationid" TEXT NOT NULL,
	"created" TIMESTAMP DEFAULT NOW(),
	UNIQUE (itemid, reservationid)
);

ALTER TABLE item2reservation ADD CONSTRAINT "item2reservation_reservationid-reservations_id" FOREIGN KEY ("reservationid") REFERENCES "reservations"("id");
ALTER TABLE item2reservation ADD CONSTRAINT "item2reservation_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
-- ALTER TABLE item2reservation ADD CONSTRAINT "item2reservation_reservedFor-users_id" FOREIGN KEY ("reservedfor") REFERENCES "users"("id");
ALTER TABLE item2reservation ADD CONSTRAINT "unique_itemid-reservationid" UNIQUE (itemid,reservationid);

--- / END RESERVE / BOOKING / AVAILABILITY -----

---CREATE TABLE "sizes" (
---"id" TEXT PRIMARY KEY NOT NULL,
---"size" TEXT UNIQUE NOT NULL
---);

---CREATE TABLE "sizes2item" (
---"sizeid" TEXT NOT NULL,
---"itemid" TEXT NOT NULL
---);

ALTER TABLE "items" ADD CONSTRAINT "items_insideof-items_id" FOREIGN KEY ("insideof") REFERENCES "items"("id");
ALTER TABLE "items" ADD CONSTRAINT "items_lastmodifiedby-users_id" FOREIGN KEY ("lastmodifiedby") REFERENCES "users"("id");
ALTER TABLE "owners2item" ADD CONSTRAINT "owners2item_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "owners2item" ADD CONSTRAINT "owners2item_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "owners2item" ADD CONSTRAINT "owners2item_user_not_public" CHECK(userid != '*');
ALTER TABLE "item2possessors" ADD CONSTRAINT "item2possessors_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "item2possessors" ADD CONSTRAINT "item2possessors_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "item2possessors" ADD CONSTRAINT "item2possessors_user_not_public" CHECK(userid != '*');
ALTER TABLE "users2knownplaces" ADD CONSTRAINT "users2knownplaces_idUsers-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "users2knownplaces" ADD CONSTRAINT "users2knownplaces_idPlace-places_id" FOREIGN KEY ("placeid") REFERENCES "places"("id");
ALTER TABLE "item2place" ADD CONSTRAINT "item2place_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "item2place" ADD CONSTRAINT "item2place_placeid-places_id" FOREIGN KEY ("placeid") REFERENCES "places"("id");
ALTER TABLE "itemvisible2users" ADD CONSTRAINT "itemvisible2users_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "itemvisible2users" ADD CONSTRAINT "itemvisible2users_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "member2group" ADD CONSTRAINT "member2group_groupid-users_id" FOREIGN KEY ("groupid") REFERENCES "users"("id");
ALTER TABLE "member2group" ADD CONSTRAINT "member2group_userid-users_id" FOREIGN KEY ("userid") REFERENCES "users"("id");
ALTER TABLE "tags2item" ADD CONSTRAINT "tags2item_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
ALTER TABLE "tags2item" ADD CONSTRAINT "tags2item_tagid-tags_id" FOREIGN KEY ("tagid") REFERENCES "tags"("id");
--ALTER TABLE "sizes2item" ADD CONSTRAINT "sizes2item_itemid-items_id" FOREIGN KEY ("itemid") REFERENCES "items"("id");
--ALTER TABLE "sizes2item" ADD CONSTRAINT "sizes2item_sizeid-sizes_id" FOREIGN KEY ("sizeid") REFERENCES "sizes"("id");

---------- PRINTERS --------

--DROP TABLE IF EXISTS "printers";
--DROP TABLE IF EXISTS "printers2users";

CREATE TABLE "printers" (
"id" TEXT PRIMARY KEY NOT NULL UNIQUE,
"prints" TEXT[] DEFAULT ARRAY[]::text[]
);

CREATE TABLE "printers2users" (
"printerid" TEXT NOT NULL,
"userid" TEXT NOT NULL
);

----------- VIEWS ------------


--DROP VIEW IF EXISTS tags_by_items;

CREATE VIEW tags_by_items
AS
----------------------------------

SELECT items.id as itemid, array_agg(t.tag) as tags
FROM tags t

INNER JOIN tags2item
	ON tags2item.tagid = t.id
LEFT JOIN items
	ON items.id = tags2item.itemid
	
GROUP BY items.id
;
-----------------------------------
-----------------------------------

--DROP VIEW IF EXISTS all_items;

CREATE 
--MATERIALIZED 
VIEW all_items
AS
----------------------------------
	
	
select distinct
		i.*,
		(SELECT array_agg(itemcontainer) FROM getContainerArray(i.id) WHERE itemcontainer IS NOT NULL) as "location.insideofArray",
--		getContainerPath(i.id)"location.insideofPath",
--		(SELECT array_agg(itemid) FROM getAllItemsInsideOf(i.id) WHERE itemid != i.id) as containingTotal,
--		(SELECT array_agg(id) FROM (SELECT * FROM items WHERE items.insideof = i.id) as i2) as containing,
		(SELECT EXISTS(SELECT id FROM items WHERE items.insideof = i.id)) as isContainer,
		tags.tags,
		array_remove(array_agg(distinct owners2item.userid),NULL) as owned, -- array of owners. needs GROUPBY at bottom. possibly replaceable by view but unclear if that would be smart
		array_remove(array_agg(distinct itemvisible2users.userid),NULL) as visible2, --  array of users this is visible for. needs GROUPBY at bottom. possibly replaceable by view but unclear if that would be smart
		array_remove(array_agg(distinct item2possessors.userid),NULL) as possessedBy,
		p.name "location.placename",
		p.id "location.id",
		ST_AsGeoJSON(p.geography) "location.geoJSON",
		p.geography "location.geography"
from items i
full join item2place b
	on b.itemid = i.id 
left join places p -- left to exclude places that have no items attached to them
	on b.placeid = p.id
left join owners2item
	on owners2item.itemid = i.id
left join 
	itemvisible2users
	on itemvisible2users.itemid = i.id
left join
	item2possessors
	on item2possessors.itemid = i.id
left join
	tags_by_items tags
	on tags.itemid = i.id
	
GROUP BY i.id, p.name, p.id, p.geography, tags.tags;

-----------------------------------
-----------------------------------
-- WITH NO DATA;


-- SELECT IN VIEW SUMMARIZING RESERVATIONS AND ITEMS AS BEFORE SELECT * FROM availability would have done:
DROP VIEW IF EXISTS all_reservations;
CREATE VIEW all_reservations
AS
	SELECT * FROM reservations
	JOIN item2reservation ON reservations.id = item2reservation.reservationid
;
-- SIMPLE BUT EFFECTIVE COMPLETE RESERVATIONS VIEW
CREATE OR REPLACE VIEW complete_reservations_with_itemidArray AS
	SELECT r.*,ARRAY_AGG(a.itemid) as items FROM reservations r
	JOIN item2reservation a ON (a.reservationid = r.id)
	GROUP BY r.id, r.reserved, r.name, r.notes, r.reservedfor, r.confirmedby
;
--- COMPLETE RESERVATIONS VIEW WITH FULL ITEM INFOS IN JSON ARRAY:
CREATE OR REPLACE VIEW complete_reservations_with_itemsJSON AS
	SELECT r.*,JSON_AGG(i) as items FROM reservations r
	JOIN item2reservation a ON (a.reservationid = r.id)
	JOIN all_items i ON (a.itemid = i.id)
	GROUP BY r.id, r.reserved, r.name, r.notes, r.reservedfor, r.confirmedby
;

-- TODO: create view availability with layout of old availability
CREATE OR REPLACE VIEW availability AS 
SELECT i.itemid, r.reserved, i.reservationid, i.created FROM item2reservation i JOIN reservations r ON i.reservationid = r.id
;




----------------------------------------------------------------------------
-- ADD PUBLIC USERGROUP with your admin password ---

-- INSERT INTO users VALUES ('*','','PUBLIC',crypt('YOURADMINPASSWORD', gen_salt('bf', 10)));
-- INSERT INTO users VALUES ('ADMIN','','ADMIN',crypt('YOURADMINPASSWORD', gen_salt('bf', 10)))



---- RESERVATION / AVAILABILITY FUNCTIONS AND TRIGGER

CREATE OR REPLACE FUNCTION deleteReservation_fn()
  RETURNS trigger AS '
BEGIN
  DELETE FROM item2reservation WHERE reservationid=OLD.reservationid;
  DELETE FROM reservations WHERE id=OLD.reservationid;
  RETURN NULL;
END; ' language plpgsql;

-- create trigger to let us delete reservations via the VIEW:
create trigger DELETE_RESERVATION INSTEAD OF DELETE
  on all_reservations
  for each row
  execute procedure deleteReservation_fn();


-- ADD function/trigger for updates on reservations: Check if all items of this reservation available for update time, otherwise block.
CREATE OR REPLACE FUNCTION check_all_reservation_items_available()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
declare
    blockingReservations TEXT[] = ARRAY[''];
BEGIN
	IF -- DOES A ROW IN AVAILABILITY EXIST THAT BLOCKS ANY OF THE ITEMS BEING ASSOCIATED WITH THIS RESERVATION?
		EXISTS(
			SELECT * FROM (SELECT a.itemid FROM availability a WHERE a.reservationid = NEW.id) as a1 -- GET ALL itemids of reservation to be updated
			JOIN availability a2 ON (a2.itemid = a1.itemid) -- check all availabilities for those items
			WHERE NEW.id != a2.reservationid -- WHERE its not the RESERVATION TO BE UPDATED BUT...
			AND NEW.reserved && a2.reserved -- ...it OVERLAPS WITH IT's TIMEFRAME
		)
	THEN
		blockingReservations = (SELECT array_agg('reservation: '||a2.reservationid || ',   item:'|| a2.itemid) FROM (SELECT a.itemid FROM availability a WHERE a.reservationid = NEW.id) as a1 -- GET ALL itemids of reservation to be updated
			JOIN availability a2 ON (a2.itemid = a1.itemid) -- check all availabilities for those items
			WHERE NEW.id != a2.reservationid -- WHERE its not the RESERVATION TO BE UPDATED BUT...
			AND NEW.reserved && a2.reserved -- ...it OVERLAPS WITH IT's TIMEFRAME
						);
		RAISE EXCEPTION 'some items are already reserved in this timeframe by reservations:' USING DETAIL=blockingReservations;
	END IF;
	RETURN NEW;	
END;
$$
;
-- add UPDATE/INSERT Trigger Blocking item2reservation adding if item is already reserved in same timeframe.
CREATE OR REPLACE FUNCTION item_already_reserved()
  RETURNS TRIGGER 
  LANGUAGE PLPGSQL
  AS
$$
declare
    blockingTime TSRANGE = tsrange('1/1/9999, 9:12:00 AM','1/1/9999, 9:12:01 AM','[]');
BEGIN
	IF
	-- IS THE NEW ITEM RESERVED ALREADY RESERVED IN THE GIVEN TIMEFRAME?
		(SELECT EXISTS (
			SELECT a.reservationid,a.itemid, a.reserved from availability a -- using availability VIEW
			WHERE (
				a.itemid = NEW.itemid
				AND a.reservationid != NEW.reservationid
			)
			AND (SELECT reserved FROM reservations WHERE id = NEW.reservationid) && a.reserved
		))
	THEN
		-- FIXME: REDUNDANT!!!! this should be done only once and used in IF clause as well CTE? other way?
		blockingTime = (SELECT reserved FROM availability a WHERE (a.itemid = NEW.itemid
				AND a.reservationid != NEW.reservationid
			)
			AND (SELECT reserved FROM reservations WHERE id = NEW.reservationid) && a.reserved  LIMIT 1); 
	
		RAISE EXCEPTION 'item % already reserved',NEW.itemid USING DETAIL='['||blockingTime||']';
	END IF;

	RETURN NEW;
END;
$$
;
CREATE OR REPLACE TRIGGER TR_itemAlreadyReserved BEFORE INSERT OR UPDATE
  ON item2reservation
  FOR EACH ROW
  EXECUTE PROCEDURE item_already_reserved()
;

CREATE OR REPLACE TRIGGER TR_checkAllReservationItemsAvailable BEFORE UPDATE
  ON reservations
  FOR EACH ROW
  EXECUTE PROCEDURE check_all_reservation_items_available()
;

---- END RESERVATION / AVAILABILITY FUNCTIONS AND TRIGGER
----------------------------------------------------------



---- ALL_ITEMS TRIGGER TO MAKE VIEW UPDATEABLE INSERTABLE AND DELETABLE -------


CREATE OR REPLACE FUNCTION insertItem() 
	RETURNS TRIGGER 
	LANGUAGE PLPGSQL
AS 
$$
DECLARE owner TEXT;
DECLARE tagtext TEXT;
DECLARE tagid TEXT;
DECLARE possessor TEXT;
DECLARE visible TEXT;
BEGIN
   -- trigger logic
   IF TG_OP = 'INSERT' THEN
   	IF NEW.owned IS NULL THEN RAISE EXCEPTION 'cannot create item without owners (must be array in column owned)'; END IF;
	IF (NEW.insideof IS NULL AND NEW."location.id" IS NULL) THEN RAISE EXCEPTION 'cannot create item without location or insideof'; END IF;
	
	IF (NEW.count IS NULL) THEN NEW.count = 1;END IF;
	IF (NEW.weight IS NULL) THEN NEW.weight = 0;END IF;
	IF (NEW.value IS NULL) THEN NEW.value = 0;END IF;
	IF (NEW.price IS NULL) THEN NEW.price = 0;END IF;
	IF (NEW.length IS NULL) THEN NEW.length = 0;END IF;
	IF (NEW.width IS NULL) THEN NEW.width = 0;END IF;
	IF (NEW.depth IS NULL) THEN NEW.depth = 0;END IF;
	
	INSERT INTO 
	items (id,name,description,count,weight,insideof,lastmodifiedby,pictures,value,price,length,width,depth,countryoforigin,billingreference,invoicedate) 
	VALUES (NEW.id,NEW.name,NEW.description,NEW.count,NEW.weight,NEW.insideof,NEW.lastmodifiedby,NEW.pictures,NEW.value,NEW.price,NEW.length,NEW.width,NEW.depth,NEW.countryoforigin,NEW.billingreference,NEW.invoicedate);

	IF (NEW.insideof IS NULL AND NEW."location.id" IS NOT NULL AND NEW."location.id" <> '') 
	THEN 
		INSERT INTO item2place (itemid,placeid) VALUES (NEW.id,NEW."location.id");
	END IF;
	
	FOREACH owner IN ARRAY NEW.owned LOOP
		INSERT INTO owners2item (userid,itemid) VALUES (owner,NEW.id);
	END LOOP;
	
	IF (NEW.possessedby IS NOT NULL) THEN
		FOREACH possessor IN ARRAY NEW.possessedby LOOP
			INSERT INTO item2possessors (userid,itemid) VALUES (possessor,NEW.id);
		END LOOP;
	END IF;
	
	IF (NEW.visible2 IS NOT NULL) THEN
		FOREACH visible IN ARRAY NEW.visible2 LOOP
			INSERT INTO itemvisible2users (userid,itemid) VALUES (visible,NEW.id);
		END LOOP;
	END IF;
	
	IF (NEW.tags IS NOT NULL) THEN
		FOREACH tagtext IN ARRAY NEW.tags LOOP
			tagid = (SELECT id FROM tags WHERE tags.tag = tagtext);
			IF (tagid IS NULL) THEN 
				tagid = uuid_generate_v4();
				INSERT INTO tags (id,tag) VALUES (tagid,tagtext);
			END IF;
			
			INSERT INTO tags2item (tagid,itemid) VALUES (tagid,NEW.id);
		END LOOP;	
	END IF;
	
	RETURN NEW;
	
  ELSIF TG_OP = 'UPDATE' THEN
  
	  UPDATE items SET 
	  	name = COALESCE(NEW.name,OLD.name),
		insideof = COALESCE(NEW.insideof,OLD.insideof),
		count = COALESCE(NEW.count,OLD.count),
		weight = COALESCE(NEW.weight,OLD.weight),
		price = COALESCE(NEW.price,OLD.price),
		value = COALESCE(NEW.value,OLD.value),
		description = COALESCE(NEW.description,OLD.description),
		lastmodified = CURRENT_TIMESTAMP,
		lastmodifiedby = COALESCE(NEW.lastmodifiedby,OLD.lastmodifiedby),
		pictures = COALESCE(NEW.pictures,OLD.pictures),
		length = COALESCE(NEW.length,OLD.length),
		width = COALESCE(NEW.width,OLD.width),
		depth = COALESCE(NEW.depth,OLD.depth),
		countryoforigin = COALESCE(NEW.countryoforigin,OLD.countryoforigin),
		billingreference = COALESCE(NEW.billingreference,OLD.billingreference),
		invoicedate = COALESCE(NEW.invoicedate,OLD.invoicedate)
	  WHERE id = NEW.id;
	  
	  IF (NEW.possessedby IS NOT NULL) THEN
	  	-- remove all old possessors
		DELETE FROM item2possessors WHERE itemid = NEW.id;
		-- add new possessors
		FOREACH possessor IN ARRAY NEW.possessedby LOOP
			INSERT INTO item2possessors (userid,itemid) VALUES (possessor,NEW.id);
		END LOOP;
	  END IF;
	  
	  IF (NEW.visible2 IS NOT NULL) THEN
	  	-- remove all old visible permits
		DELETE FROM itemvisible2users WHERE itemid = NEW.id;
		-- add new visible permits
		FOREACH visible IN ARRAY NEW.visible2 LOOP
			INSERT INTO itemvisible2users (userid,itemid) VALUES (visible,NEW.id);
		END LOOP;
	  END IF;
	  
	  IF (NEW.owned IS NOT NULL) THEN
	  	IF (cardinality(NEW.owned)=0) THEN 
			RAISE EXCEPTION 'owned cannot be empty';
		END IF;
	  	-- remove all old owners
		DELETE FROM owners2item WHERE itemid = NEW.id;
		-- add new owners
		FOREACH owner IN ARRAY NEW.owned LOOP
			INSERT INTO owners2item (userid,itemid) VALUES (owner,NEW.id);
		END LOOP;
	  END IF;
	  
	  IF (NEW.tags IS NOT NULL) THEN
	  	-- remove all old tags
		DELETE FROM tags2item WHERE itemid = NEW.id;
		-- add new tags
		FOREACH tagtext IN ARRAY NEW.tags LOOP
			tagid = (SELECT id FROM tags WHERE tags.tag = tagtext);
			IF (tagid IS NULL) THEN 
				tagid = uuid_generate_v4();
				INSERT INTO tags (id,tag) VALUES (tagid,tagtext);
			END IF;

			INSERT INTO tags2item (tagid,itemid) VALUES (tagid,NEW.id);
		END LOOP;
	  END IF;
	  
   RETURN NEW;
   
  ELSIF TG_OP = 'DELETE' THEN
  	DELETE FROM item2place WHERE itemid = OLD.id;
	DELETE FROM owners2item WHERE itemid = OLD.id;
	DELETE FROM item2possessors WHERE itemid = OLD.id;
	DELETE FROM item2place WHERE itemid = OLD.id;
	DELETE FROM tags2item WHERE itemid = OLD.id;
	DELETE FROM itemvisible2users WHERE itemid = OLD.id;
	DELETE FROM items WHERE id= OLD.id;
   RETURN NULL;
  END IF;
  
  RETURN NEW;
  
END;
$$
;

CREATE OR REPLACE TRIGGER trigger_all_items_insert 
	INSTEAD OF 
		INSERT OR UPDATE OR DELETE
	ON all_items
		FOR EACH ROW 
	EXECUTE PROCEDURE insertItem()
;

----------------------------------------------------------
---- END ALL_ITEMS UPDATE, INSERT, DELETE TRIGGER
----------------------------------------------------------
