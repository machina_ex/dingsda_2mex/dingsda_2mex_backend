CREATE EXTENSION IF NOT EXISTS plpgsql;
CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp"; -- for uuid_generate_v4()
-- CREATE EXTENSION IF NOT EXISTS postgis_raster; -- OPTIONAL
-- CREATE EXTENSION IF NOT EXISTS postgis_topology; -- OPTIONAL
CREATE EXTENSION IF NOT EXISTS pgcrypto;

--DROP FUNCTION getContainerArray(startitem text);

CREATE OR REPLACE FUNCTION getContainerArray(startitem text)
--RETURNS text
RETURNS table (
		depth int,
		itemid text,
		itemcontainer text
) 

AS $$
DECLARE ret TEXT; -- var to be returned later
BEGIN
	-- QUERY: get all parent items of startitem (reverse tree) and return one value string in path form
	RETURN QUERY 
	WITH RECURSIVE cte_name(_depth,_itemid,_itemcontainer) AS(
		SELECT 0, items.id, items.insideof -- non-recursive term
			FROM items
			WHERE items.id = startitem -- start item
		UNION
		SELECT _depth+1,items.id, items.insideof -- recursive term
		 FROM cte_name, items
		WHERE items.id=_itemcontainer
		AND _depth < 50 -- limits depth on request but should rather be done on CREATE/UPDATE
	) 
	SELECT * FROM cte_name as c
	ORDER BY c._depth DESC
	;
END;

$$
LANGUAGE plpgsql
;

CREATE OR REPLACE FUNCTION getContainerPath(startitem text)
RETURNS text

AS $$
DECLARE ret TEXT; -- var to be returned later
BEGIN
	-- QUERY: get all parent items of startitem (reverse tree) and return one value string in path form
	WITH RECURSIVE cte_name(depth,itemid,itemcontainer,path) AS(
		SELECT 0, items.id, items.insideof, '' -- non-recursive term
			FROM items
			WHERE items.id = startitem -- start item
		UNION
		SELECT depth+1,items.id, items.insideof, CONCAT_WS('/',itemcontainer,path) -- recursive term
		 FROM cte_name, items
		WHERE items.id=itemcontainer
		AND depth < 50 -- limits depth on request but should rather be done on CREATE/UPDATE
	) 
	SELECT path FROM cte_name -- only path
	INTO ret -- output var to be filled
	WHERE itemcontainer IS NULL -- only last path
	;
	RETURN ret; -- return from function
END;

$$
LANGUAGE plpgsql
;

--DROP FUNCTION getallitemsinsideof(text);
CREATE OR REPLACE FUNCTION getAllItemsInsideOf(startitem text)
RETURNS table (
		depth int,
		itemId text,
		itemName text,
		itemInsideOf text
	) 

AS $$
DECLARE ret TEXT; -- var to be returned later
BEGIN
	-- QUERY: get all items inside of Item with id of the first container (tree traversal forward)
	RETURN QUERY -- return from function as table
	WITH RECURSIVE cte_name(_depth,_itemid,_itemname,_itemInsideOf) AS(
		SELECT 0, items.id, items.name, items.insideof -- non-recursive term
			FROM items
			WHERE items.id = startitem -- start item
		UNION
		SELECT _depth+1,items.id, items.name,items.insideof -- recursive term
		 FROM cte_name, items
		WHERE items.insideof=_itemid
		AND _depth < 50 -- limits depth on request but should rather be done on CREATE/UPDATE
	) SELECT * FROM cte_name;

END;

$$
LANGUAGE plpgsql
;
--DROP FUNCTION getuseraliases(text);
CREATE OR REPLACE FUNCTION getUserAliases(inputuserid text)
RETURNS table (
		id text
	) 
AS $$
BEGIN	
	RETURN QUERY -- return from function as table
	 SELECT u.id from users u
		WHERE u.id = inputuserid
	 UNION
	 SELECT g.groupid from member2group g
		WHERE g.userid = inputuserid;

END;
$$
LANGUAGE plpgsql
;
------------------------------------

--DROP FUNCTION findItemWithinRadius(centerpoint geography, radius int);
CREATE OR REPLACE FUNCTION findItemWithinRadius(centerpoint geography, radius int)
RETURNS table (
		id text
	) 
AS $$
BEGIN	
	RETURN QUERY -- return from function as table
	 WITH RECURSIVE cte_name(entryid) AS(
		-- get item records from geography -- non-recursive term
	select i.id
	from items i
	full join item2place b
	on b.itemid = i.id
	left join places p -- left to exclude places that have no items attached to them
	on b.placeid = p.id
	WHERE ST_DWithin(
		p.geography, 
		centerpoint, -- center of search radius
		radius -- radius in meters
	)
      -- recursive term
	UNION
	SELECT itemid FROM cte_name, getAllItemsInsideOf(entryid)
) SELECT * FROM cte_name;

-- TODO: get full item records as in getItemRecordsIncludingLocation.sql

END;
$$
LANGUAGE plpgsql
;
------------------------------------

--DROP FUNCTION findItemWithinRadius(centerpoint geography, radius int);
CREATE OR REPLACE FUNCTION findItemsWithIntersectingGeography(searcharea geography)
RETURNS table (
		id text
	) 
AS $$
BEGIN	
	RETURN QUERY -- return from function as table
	 WITH RECURSIVE cte_name(entryid) AS(
		-- get item records from geography -- non-recursive term
	select i.id
	from items i
	full join item2place b
	on b.itemid = i.id
	left join places p -- left to exclude places that have no items attached to them
	on b.placeid = p.id
	WHERE ST_Intersects(
		p.geography, 
		searcharea -- center of search radius
	)
      -- recursive term
	UNION
	SELECT itemid FROM cte_name, getAllItemsInsideOf(entryid)
) SELECT * FROM cte_name;

-- TODO: get full item records as in getItemRecordsIncludingLocation.sql

END;
$$
LANGUAGE plpgsql
;