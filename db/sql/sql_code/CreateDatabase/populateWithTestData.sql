-- ALL USER PASSWORDS ARE HASHES OF 'test'
INSERT INTO users
VALUES ('uuid_philip','philip@machinaex.de','philip','$2b$10$WMxrMyHrNkJXdNlCHPJMg.a1jMKafFr/CcE9HfY4sRhFoGHzijjHW','philip@machinaex.de'),
('uuid_lasse','lasse@machinaex.de','lasse','$2b$10$vtkWwlmM/6tclLUnD/M73uCKjvggFYnjA2/TylwjwRwc8cDSqv3le',null),
('uuid_bene','bene@machinaex.de','bene','$2b$10$pL0mAxz9C.gSWjmZ8AmT3OSzSASHfc.s96qj21BBYwpOoPVnVHURm',null),
('uuid_elisa','elisa@machinaex.de','elisa','$2b$10$pL0mAxz9C.gSWjmZ8AmT3OSzSASHfc.s96qj21BBYwpOoPVnVHURm',null),
('uuid_machina_ex','','machina ex','$2b$10$GuhkOxK2wGURJdjNLdhi0O.1cBMz0HwnhK9no4uSEsTVxSEuAXcGS','commons@machinaex.de')
;
INSERT INTO member2group
VALUES ('uuid_philip','uuid_machina_ex'),
('uuid_lasse','uuid_machina_ex'),
('uuid_philip','*'),
('uuid_lasse','*'),
('uuid_bene','*'),
('uuid_machina_ex','*')
;
INSERT INTO items
VALUES
(1,'graue box',null,1,0.2,5,CURRENT_TIMESTAMP,null,null,0,6,60,60,40),
(2,'rote box',null,1,0.2,1,CURRENT_TIMESTAMP,null,null,4.50,5.00,60,60,40),
(3,'kabel blau xlr',null,1,0.3,2,CURRENT_TIMESTAMP,null,ARRAY['https://images.unsplash.com/photo-1595756630797-e3c9ee1a9002?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80'],0,0,250,0,0),
(4,'keller',null,1,0,null,CURRENT_TIMESTAMP,null,null,0,0,0,0,0),
(5,'Regal metall',null,1,0,4,CURRENT_TIMESTAMP,null,null,0,0,0,0,0),
(6,'pappkarton',null,1,0.1,5,CURRENT_TIMESTAMP,null,null,0,0,30,30,30),
(7,'Hose grau',null,1,0.5,null,CURRENT_TIMESTAMP,null,null,0,0,0,0,0),				
(8,'DMX Kabel inside of 6','This file is licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license. By DJSparky',1,0.25,6,CURRENT_TIMESTAMP,null,ARRAY['https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/DMX_Cable.jpg/450px-DMX_Cable.jpg'],0,0,1000,0,0),
(9,'Mantel inside 5',null,1,1.2,5,CURRENT_TIMESTAMP,null,null,0,0,0,0,0),
(10,'Schuhe inside 6',null,1,0.4,6,CURRENT_TIMESTAMP,null,null,0,0,0,0,0),
(11,'eine andere kiste in 4',null,1,0.3,4,CURRENT_TIMESTAMP,null,null,0,0,30,30,20),
(12,'Schuko Kabel in 11',null,1,0.3,11,CURRENT_TIMESTAMP,null,ARRAY['https://images.pexels.com/photos/5089126/pexels-photo-5089126.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'],0,0,500,0,0),
(13,'Schuko Kabel #2','This file is licensed under the Creative Commons Attribution-Share Alike 4.0 International license.	Attribution: Santeri Viinamäki',1,0.2,11,CURRENT_TIMESTAMP,null,ARRAY['https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Schuko_Power_Strip.jpg/800px-Schuko_Power_Strip.jpg'],0,0,100,0,0),
(14,'Schuko Kabel #3',null,1,0.3,11,CURRENT_TIMESTAMP,null,ARRAY['https://images.pexels.com/photos/5089126/pexels-photo-5089126.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'],0,0,50,0,0),
(15,'Schuko Kabel #4','This file is licensed under the Creative Commons Attribution-Share Alike 4.0 International license.	Attribution: Santeri Viinamäki',1,0.2,11,CURRENT_TIMESTAMP,null,ARRAY['https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Schuko_Power_Strip.jpg/800px-Schuko_Power_Strip.jpg'],0,0,200,0,0),
(16,'Schuko Kabel #5',null,1,0,11,CURRENT_TIMESTAMP,null,ARRAY['https://images.pexels.com/photos/5089126/pexels-photo-5089126.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'],0,0,120,0,0),
(17,'Schuko Kabel #6','This file is licensed under the Creative Commons Attribution-Share Alike 4.0 International license.	By: Aunkrig',1,0.2,11,CURRENT_TIMESTAMP,null,ARRAY['https://upload.wikimedia.org/wikipedia/commons/1/19/IP44-Schuko-Kupplung_und_-Stecker.jpg'],0,0,0,0,0),
(18,'Schuko Kabel #7','This file is licensed under the Creative Commons Attribution-Share Alike 4.0 International license.	Attribution: Santeri Viinamäki',1,1.0,11,CURRENT_TIMESTAMP,null,ARRAY['https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Schuko_Power_Strip.jpg/800px-Schuko_Power_Strip.jpg'],3.10,5,200,0,0)
;
INSERT INTO owners2item
VALUES
('uuid_philip',1),('uuid_philip',2),('uuid_philip',7),('uuid_philip',8),
('uuid_lasse',2),('uuid_lasse',3),('uuid_machina_ex',18),
('uuid_bene',4),('uuid_bene',5),('uuid_bene',6),('uuid_bene',9),
('uuid_machina_ex',10),('uuid_machina_ex',11),('uuid_machina_ex',12),('uuid_machina_ex',1),('uuid_machina_ex',13),('uuid_machina_ex',14),('uuid_machina_ex',15),('uuid_machina_ex',16),('uuid_machina_ex',17)
;
INSERT INTO places
VALUES
(1,ST_Buffer(ST_MakePoint(13.443541727580785, 52.59586689100561)::geography, 10),'Berlin, home philip'),
(2,ST_Buffer(ST_MakePoint(9.195806991415235, 48.904612588045254)::geography, 10),'Ludwigsburg, home bene'),
(3,ST_Buffer(ST_MakePoint(13.284265623589162, 52.36101940987931)::geography, 50),'Berlin, Lager Gelände')
;
INSERT INTO users2knownplaces
VALUES
('uuid_philip',1),
('uuid_bene',2),
('uuid_philip',3)
;
INSERT INTO item2place
VALUES
(4,3),
(7,3)
;
INSERT INTO itemvisible2users
VALUES
(4,'uuid_philip'),
(5,'uuid_philip'),
(3,'uuid_philip'),
(3,'*')
;
INSERT INTO item2possessors
VALUES
('uuid_philip',6),
('uuid_machina_ex',3),
('uuid_lasse',1)
;
INSERT INTO tags
VALUES
(1,'ontour'),
(2,'ontour:lol'),
(3,'missing'),
(4,'broken'),
(5,'cable'),
(6,'XLR'),
(7,'DMX'),
(8,'female-female'),
(9,'female-male'),
(10,'male-male'),
(11,'male-female'),
(12,'costume'),
(13,'props'),
(14,'set design'),
(15,'size:43cm'),
(16,'size:98cm'),
(17,'size:22cm'),
(18,'color:black'),
(19,'color:white')
;
INSERT INTO tags2item
VALUES
(5,8),
(5,3),
(5,12),
(6,8),
(7,3),
(10,8),
(11,3),
(18,3),
(15,7),
(19,18),(9,18),(15,18)
;

------ PRINTERS ----------

INSERT INTO printers
VALUES 
('testid',ARRAY['www.machinaex.com']),
('machinaprinter',ARRAY['www.commons.com']),
('benesprinter',ARRAY['www.bene.com'])
;

INSERT INTO printers2users
VALUES 
('testid','uuid_philip'),
('machinaprinter','uuid_machina_ex'),
('benesprinter','uuid_bene')
;