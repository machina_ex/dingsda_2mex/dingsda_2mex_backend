# POSTGRESQL DATABASE

At the moment dingsda 2 is based on a postgreSQL database. postgres is a relational database. That makes some things very easy, and other things hard. In the future we might have to think about swap the relational database for another type of database. Graph databases seem to cover some of dingsda 2's usecases better, but at the time of this writing they were still too complex of a topic for us to choose them over SQL. Issues of readability and compatibilty were also a factor. 

In the future we might consider a combined approach, where some data (like item-container relationships) are accessed and stored in a graph database while the main data (simpler relationships, views, ownership etc) is still stored in SQL. (contributions welcome!)

To see the database architecture, you can

- read `createDB.sql` and `createDBFunctions.sql` which are the two sql queries needed to create a fresh Database.

or alternatively (not guaranteed to be always up to date, but more human friendly)

- take a look at `diagram.pgerd` with <a href="https://www.pgadmin.org/docs/pgadmin4/development/erd_tool.html">pgAdmin's ERD Tool</a>

# Database Structure

to get the following dependencies between entities in the database

```mermaid
graph LR;
    users-- are members of -->users;
    users-- own --->items;
    users-- see --->items;
    users-- possess --->items;

    items-- inside of/contained by ---items;
    items-- at ---places

    items-- have ---tags
```

the SQL database is structured as follows (boxes are tables, lines are FK constraints):

```mermaid
graph LR;
    users-- id / userid --- member2group-- id / groupid -->users;
    users-- id / userid --- owners2item-- itemid / id ---items;
    users-- id / userid  --- itemvisible2users -- itemid / id ---items;
    users-- id / memberid --- item2possessors -- itemid / id ---items;
    

    items-- insideof / id---items;
    items-- id / itemid ---item2place-- id / placeid ---places;

    items-- id / itemid ---tags2item-- tagid / id ---tags;

```

other relationships (like users2knownplaces and pending_handovers) are prepared in the DB tables, but are not implemented in the server's API because at the current moment they are not needed (yet?)

## TABLES and VIEWS

Checkout all tables and views in the `createDB.sql`.

the most relevant view is all_items which compiles nearly all relevant data to describe every item in the database.
Among others, it creates a column/field named `insideofarray` which contains an ordered array describing the path of containers from the item upwards. This requires a tree traversing

# Create fresh database

If you want to create a fresh dingsda2mex database, install postgresql on your machine (there are plenty of ways to do it), make sure to include/install the `PostGIS` plugins (for geographical datatypes) and the plugin `pgcrypto`, change the ADMINPASSWORD in the last lines of `createDB.sql` to your admin password and uncomment them (remove the leading `--`).

```sql
INSERT INTO users
VALUES ('*','','PUBLIC',crypt('YOURADMINPASSWORD', gen_salt('bf', 10)));
INSERT INTO users 
VALUES ('ADMIN','','ADMIN',crypt('YOURADMINPASSWORD', gen_salt('bf', 10)));
```

and then run:

1. `createDB.sql` and 
2. after that run `createDBFunctions.sql`

This will create an empty but functioning database that the client can connect to.

## The public usergroup

The Read-Write-Update-Move permissions are based on the tables `users`, `member2group`,`owners2item`, `item2possessors` and `itemvisible2users`. 

Users and Groups are both located in `users` but the client and the API expect at least one admin controlled pseudo-user with id `*` inside of `users`. 
This is the PUBLIC GROUP and every new user will normally be associated with the PUBLIC GROUP on signup (see addUser() in `db/postgres.sql`). Users can associate `*` for the viewing permission types of their items, to make an item publicly visible (`itemvisible2users`), but will not be able to add `*` to editing or possession permissions (`item2possessors` and `itemvisible2users`).

```sql
INSERT INTO users 
VALUES ('*','','PUBLIC',crypt('YOURADMINPASSWORD', gen_salt('bf', 10)));
```

> Note: If you install via docker-compose, the `entrypoint.sh` script will take your **PGPASSWORD** and use it as passphrase to your **PUBLIC** usergroup

## The ADMIN user

Some actions in postgres.sql only work when executed as ADMIN user. For that this user has to exist. You need to also create that ADMIN user in the database:

```sql
INSERT INTO users 
VALUES ('ADMIN','','ADMIN',crypt('YOURADMINPASSWORD', gen_salt('bf', 10)));
```

> Note: If you install via docker-compose, the `entrypoint.sh` script will take your **PGPASSWORD** and use it as passphrase to your **ADMIN** user

# Mock and example data

In case you need mockdata or example data for development purposes, or to see everything in action, you can use one of the following scripts:

`node populateDB.js` will run a complex script creating random mock data for most tables. This is good if you want to demo the system or work on performance, timing etc. It can create several thousand data entries and is useful to test the scaling of the system.

`populateWithTestData.sql` is a minimal populate script, creating a bare minimum of entries into all relevant tables. It is probably the best if you want to development new functionality that without loosing oversight over the data.
