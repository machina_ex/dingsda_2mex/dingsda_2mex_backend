const SQL = require("../postgresql.js");
const sql = new SQL({
  pgdatabase:"test"
});

const Log = require('../../logging');
const log = new Log();
log.init("trace","./")
global.log = log;

let client;

sql.pool.connect()
  .then((c)=>{
    client = c;
    return client
  })
  .then(async ()=>{
    //let item = await sql.getItem({id:"955f961f-fd4c-4544-a072-59f983951b43",client:client,username:"ADMIN"});
    let item = await sql.searchAndReplacePictureURLs("%uuid_%",/uuid_/,"");
    log.info("item",item)
    return item
  })
  .then((res)=>{
    log.info(res.name)
    client.release();
    sql.quit();
        
  })
;



