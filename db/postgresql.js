/* eslint-disable no-undef */
const dotenv = require('dotenv');
dotenv.config();
const sql = require('sql-template-strings');
const { Pool } = require('pg');
const format = require('pg-format');
const { v4: uuidv4 } = require('uuid');
const errors = require("../errors.js")
const bcrypt = require("bcrypt");
var cloneDeep = require('lodash.clonedeep');
const union = require('lodash.union')
const helpers = require('../helpers.js');
var { from : copyFrom } = require('pg-copy-streams');
const { pipeline } = require('node:stream/promises');
const fs = require('node:fs');
const { parse } = require('csv-parse');
const { stringify } = require('csv-stringify');
const { transform } = require('stream-transform');
module.exports = class {
  
  constructor(configoverwrite) {
    this.pool = new Pool({
      user: configoverwrite?.pguser || process.env.PGUSER,
      host: configoverwrite?.pghost || process.env.PGHOST,
      database: configoverwrite?.pgdatabase || process.env.PGDATABASE,
      password: configoverwrite?.pgpassword || process.env.PGPASSWORD,
      port: 5432,
    });
    console.log("postgresql.js","postgres pool connection with",{
      user: configoverwrite?.pguser || process.env.PGUSER || 'default',
      host: configoverwrite?.pghost || process.env.PGHOST || 'default',
      database: configoverwrite?.pgdatabase || process.env.PGDATABASE || 'default',
      password: configoverwrite?.pgpassword || process.env.PGPASSWORD || 'default',
      port: 5432,
    })

    // the pool will emit an error on behalf of any idle clients
    // it contains if a backend error or network partition happens
    this.pool.on('error', (err) => {
      log.error('Unexpected error on idle client', err);
      // eslint-disable-next-line no-process-exit
      process.exit(-1);
    });
  }

  start() {
    log.info('database', 'running');
  }

  quit() {
    const prom = this.pool.end();
    return prom.then(() => {
      log.info('pool idle:', this.pool.idleCount);
    });
  }

  // endpoint to upload csv for creation of items. only works if every single item is permitted to be added.
  // uses all_items update Trigger
  async importCSVfile(csvfile,userRequesting){

    let headers;

    const allowedKeys = ['id','name','description','count','value',
    'price','length','width','height','billingreference','invoidedate',
    'countryoforigin','insideof',
    'pictures','tags','owned','visible2',
    'possessedBy'
    ]

    const arrayKeys = ['pictures','tags','owned','visible2','possessedBy']

    let filterOutForbiddenColumnsTransformer = transform((input) => {
      console.log("transforming...");
    
      for (let key of Object.keys(input)){
        if (
          !allowedKeys.includes(key)
          //key.startsWith('location.')
          ){
          delete input[key]
        }
        // make sure arrays are importable and with {} around them
        if (arrayKeys.includes(key)){
          input[key]=`{${input[key]}}` // adds {} for psql array literal
        }
      }
      //console.log("after filters:",input);
      //console.log("headers are:",Object.keys(input));
      headers = Object.keys(input);
      return input;
    })

    const client = await this.pool.connect()
    try {
      // get headers
      let count = 0;
      let plannedOwners = []; // CONTINUE HERE: TODO: rebuild plannedOwners Censorship (look at old commit)

       const parsed = fs.createReadStream(csvfile).pipe(parse({columns:true}));
       for await (const record of parsed){
         if (count < 1){
           count++;
           headers = Object.keys(record);
         }
         plannedOwners = union(plannedOwners,record.owned.replace('{','').replace('}','').split(','))
         // collect all owners, check every of them for this user, used and forbid if record tries to set owner that is not allowed
         let allowedUsers = await this.getUserGroupsByUser(userRequesting)
         console.log("allowed usergroups: ",allowedUsers)
         allowedUsers.push(userRequesting)
         for (let plannedOwner of plannedOwners){
           let plannedOwnerIsAllowed = allowedUsers.some((allowedUser)=>{
             return plannedOwner == allowedUser
           })
           if (!plannedOwnerIsAllowed) throw new errors.ForbiddenError(`item ${record.name} cannot be added: contains owner that is not allowed for you to add.`)
         }
         // could this happen in the stream?
       }     

      headers = headers.filter((h)=>allowedKeys.includes(h));
      
      /// parse and write the csv into db
      const ingestStream = client.query(copyFrom(`
      COPY all_items(${headers.toString()}) FROM STDIN CSV 
      `))

      await pipeline(
        fs.createReadStream(csvfile),
          parse({columns:true}),
          filterOutForbiddenColumnsTransformer,
          stringify(),
          ingestStream
        )
    } catch(e){
      log.error('SQL','importCSVfile','could not import CSV',e)
      throw new errors.DingsdaError(`could not import CSV: ${e.message}`)
    }
    finally {
      client.release();
      await fs.promises.rm(csvfile).catch((e)=>log.error("SQL","importCSVfile could not delete temp file",csvfile))
    }
  }

  async getItemsInsideItem(id,deep=false){
    log.info('SQL','getItemsInsideItem',id,deep,deep == 'true' )
    const res = await this.pool.query(
      deep != 'true' ?
        sql`
      SELECT id, name FROM items 
      WHERE items.insideof = ${id}
      ` :
        sql`
      SELECT itemid as id, itemname as name FROM getAllItemsInsideOf(${id})
      WHERE itemid != ${id}
      `
    ).catch((e)=>{throw new errors.DingsdaError("error in getItemsInsideItem",e)});
  
    if (!res.rows ){
      throw new errors.DingsdaError("error in getItemsInsideItem, no rows returned")
    }
    return res.rows
  }

  async getUserPasswordHashByUsername(username){
    log.info('SQL','getUserPasswordHashByUsername',username)
    const res = await this.pool.query(
      sql`
      select hashedpassword from users
      WHERE id = ${username}
    `
    ).catch((e)=>{throw new errors.InvalidError("error in getUserPasswordHashByUsername",e)});
    
    log.trace('SQL','getUserPasswordHashByUsername',"result",res)
    
    if (!res.rows || res.rowCount < 1 || ! res.rows[0].hashedpassword){
      log.error("SQL","res was:",res.rows)
      throw new errors.InvalidError("user does not exist")
    }
    return res.rows[0].hashedpassword
  }

  async getLocation({id}){
    log.info('SQL','getLocation',id);
    if (!id) throw new errors.InvalidError("getLocation needs id")
    const client = await this.pool.connect();
    try{
      let res;   
      res = await client.query(sql`select id, ST_AsGeoJSON(geography) as geography, name from places p WHERE p.id = ${id}`);
      return res.rows[0] || {};
    }
    finally {
      client.release();
    }
  }

  async getLocations({latitude,longitude,radius,user}){ 
    if (user) log.info('SQL', 'getting locations for user', user);
    else log.info('SQL', 'getting all locations because user undefined')
    if (!latitude || !longitude) throw new errors.InvalidError("needs at least latitude and longitude parameters");
    if (!radius) radius = 1000;
    const client = await this.pool.connect();
    try{
      let res;   
      res = await client.query(sql`
      select id, ST_AsGeoJSON(geography) as geography, name from places p

        WHERE ST_DWithin(
          p.geography, 
          ST_MakePoint( ${latitude},${longitude})::geography, -- center of search radius
          ${radius} -- radius in meters
        )
        `);
      res.rows.map((row)=>{
        row.geography = JSON.parse(row.geography);
        return row
      })
      return res;
    }
    finally {
      client.release();
    }
  }

  async getContactDetailsFromOwners(itemid,username){ 
    
    
    let item = await this.getItemIfVisible2UserNoClient(itemid,username) // CONTINUE HERE: why does user * not have access to public items?
    if (!item){
      throw new errors.ForbiddenError("item does not exits or user is not authorized to view this item.");
    }
    
 
    let res = await this.pool.query(
      sql`
      SELECT userid, public_contact_details FROM users

      INNER JOIN owners2item ON owners2item.userid = users.id
      
      WHERE itemid = $1
      `,[itemid]
    ).catch((e)=> {throw e});
    return res
  }

  async handoverItems({items,owner,receivers}){
    if (!items || !owner || !receivers){
      throw new errors.InvalidError("handover items needs valid user as receiver (to), a valid item and a user with FULL owner rights to the items");
    }
    log.debug("SQL", 'handoveritems',items, owner, receivers);
    let promises = [];
    for (let item of items){ // FIXME: this should be its own sql TRANSACTION and ROLLBACK on error with verbose message
      promises.push(this.handoverItem({item,owner,receivers}))
    }
    await Promise.all(promises).catch((e)=>{
      log.error("could not hand over all items",e);
      throw new errors.InvalidError("could not hand over all items. Please doublecheck!\n\n"+ e.message);
    })
    return {handover:true}
  }

  async handoverItem({item,owner,receivers}){
    if (!item || !owner || !receivers){
      throw new errors.InvalidError("handover needs valid user as receiver (to), a valid item and a user with FULL owner rights to the item");
    }
    log.debug("SQL",item, owner, receivers)
    let DBres = await this.editItem({id:item,inPossessionOf:receivers},owner)
    log.debug("SQL","handoverItem",DBres)
    if (!DBres.rows){return}
    return {handover:true}
  }

  async getRootContainerFromItem(itemid,user){
    let res = await this.pool.query(
      sql`
      SELECT * FROM all_items

      WHERE id = (
        SELECT "location.insideofArray"[1] from all_items
        WHERE id = $1
      )
      ;
      `,[itemid]
    )
      .catch((e)=> {throw e});
    if (res.rows && res.rows.length === 0){
      res = await this.pool.query(
        sql`
        SELECT * FROM all_items WHERE id = $1 ;
        `,[itemid]
      )
        .catch((e)=> {throw e});
    }
    return res.rows
  }

  async getAllPossibleRootContainersFromUser(user){
    log.info('SQL', 'getting all possible root containers for user', user);
    const res = await this.pool.query(
      sql`
      -- get all possible rootContainers
      SELECT * FROM all_items --might be a lot faster using items and location tables instead
      
      WHERE insideof is null
      -- AND "location.geoJSON" is not null -- TODO: enforce on INSERT/UPDATE. not here.
      AND 
      (owned && (SELECT array_agg(id) FROM getUserAliases($1))
        OR possessedby && (SELECT array_agg(id) FROM getUserAliases($1))
        -- OR visible2 && (SELECT array_agg(id) FROM getUserAliases($1))
       )
      `,[user]
    )
      .catch((e)=> {throw e});
    return res
  }

  async getAllItems(){
    const items = await this.pool.query(`
    SELECT * FROM all_items
    `)
    return items.rows
  }

  async getItems(user,lastItemOnPage,showAllVisibleItems) {
    const client = await this.pool.connect();
    log.trace('SQL', 'getting items for user', user);
    try {
      let paginationQuery = this.getPaginationQuery(lastItemOnPage);

      let query;
      let res;
      if (showAllVisibleItems){
        res = await client.query(sql`
          select * from all_items i -- this is a view!
          where (
            owned && (SELECT array_agg(id) FROM getUserAliases($1)) -- check if owned by user or group user is part of      
            OR possessedby && (SELECT array_agg(id) FROM getUserAliases($1)) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
            OR visible2 && (SELECT array_agg(id) FROM getUserAliases($1)) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
          )
                `, [user]);
      }
      else{
        query = format(`
        select * from all_items i -- this is a view!
        
        where (
          owned && (SELECT array_agg(id) FROM getUserAliases('%s')) -- check if owned by user or group user is part of      
          OR possessedby && (SELECT array_agg(id) FROM getUserAliases('%s')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
        )
        %s
      -- TODO: check if smarter to do a join here first with array_agg(getUserAliases()) 
      -------- might be more efficient for querying getUserAliases() several times seems expensive.
              `,user,user,paginationQuery);
        res = await client.query(query);
      }

      log.trace("++++++++ QUERY ++++++++++");
      console.log(query)

      let { items, tags } = this.filterItemsAndTagsConsideringPagination(res,lastItemOnPage);
      //return res;
      return {items:items,tags}
    } finally {
      client.release();
    }
  }

  filterItemsAndTagsConsideringPagination(res,lastItemOnPage) {
    let tags = [];
    let items = res.rows;
    if (!lastItemOnPage) {
      tags = this.getTagsFromAllItemsList(res); //get tags to all items not just the LIMIT 50
    }
    if (!lastItemOnPage) {
      items = res.rows.slice(0, 50);
    }
    return { items, tags };
  }

  getPaginationQuery(lastItemOnPage) {
    let notFirstPageQuery = '';
    if (lastItemOnPage) {
      notFirstPageQuery = `AND i.id > '${lastItemOnPage}' -- where this id is is the end of the last page.`;
    }
    let paginationQuery = `
    ${notFirstPageQuery}
    ORDER BY i.id
    ${lastItemOnPage ? 'LIMIT 50' : ''};
    `;
    return paginationQuery;
  }

  /**
   * 
  * Add a new tag.
 * @param {Object} tagEntry - The databaseEntry of the tag to be added
 * @param {string} tagEntry.id - id of tag
 * @param {string} tagEntry.tag - text of tag
 * @param {pg-client} tagEntry.tag - text of tag
 */
  async addTag({ tag, client}) {
    let id;
    tag = tag.trim();
    // get id of tag if tag exists already
    let idRes = await client.query(sql`
    SELECT id from tags WHERE tag = ${tag};
    `);
    if (idRes.rows[0]){
      id = idRes.rows[0].id
    }
    if (!id){ // if tag does not exist, make an id
      if (process.env.DIRECT_TAG_ADDING_ALLOWED != 'true') {
        throw new errors.ForbiddenError("your server does not allow adding new tags like this.");
      }
        id = uuidv4();
    }
    await client.query(sql`
    INSERT INTO tags
    VALUES
    (${id},${tag})
    ON CONFLICT DO NOTHING;
    ;
    `).catch((e) => {
      log.error('sql', 'ERROR creating tag', e.detail);
      throw e;
    });
    return id;
  }

  async linkTagToItem({tag,itemId,client}){
    let id;
    // get id of tag if tag exists already
    let idRes = await client.query(sql`
    SELECT id from tags WHERE tag = ${tag};
    `);
    if (idRes.rows[0]){
      id = idRes.rows[0].id
    }
    if (!id){ // if tag does not exist, throw error
      log.error("SQL",`could not link tag to item because tag "${tag}"" does not exist yet.`)
      throw new errors.InvalidError("tag does not exists")
    }
    await client.query(sql`
    INSERT INTO tags2item
    VALUES
    (${id},${itemId})
    ON CONFLICT DO NOTHING;
    ;
    `).catch((e) => {
      log.error('sql', 'ERROR linking tag to item', e.detail);
      throw e;
    });
    return id
  }

  async findDataByTextSearch({tablename,columnname,searchquery,limit}){
    if (!tablename || !columnname || !searchquery) throw new errors.InvalidError("findDataByTextSearch needs tablename, columnname, searchquery as parameters")
    if (!limit) limit = 10; log.debug('sql',"setting result limit to 10")
    const querytext = format(`
      SELECT * FROM %I
      WHERE 
      to_tsvector(%s)
      @@ to_tsquery('%s')
      LIMIT %s
      ;
      `,tablename,columnname,searchquery,limit);
    const res = await this.pool.query(querytext)
      .catch((e)=> {throw e});
    return res.rows
  }

  async findDataByPattern({tablename,columnname,pattern,limit}){
    if (!limit) limit = 10;
    if (!tablename || !columnname || !pattern) throw new errors.InvalidError("findDataByPattern needs tablename, columnname, pattern as parameters")
    log.info("SQL",`findDataByPattern ${pattern} with limit ${limit} inside ${tablename}/${columnname}`)
    const querytext = format(`
      SELECT * FROM %I
      WHERE %s
      ILIKE
      '%s'
      LIMIT %s
      `,tablename,columnname,pattern,limit);
    const res = await this.pool.query(querytext)
      .catch((e)=> {throw e});
    return res.rows
  }

  async addUserToGroup(userid,groupid){
    const client = await this.pool.connect();
    try {
      await client.query(sql`
        INSERT INTO member2group
        VALUES (${userid},${groupid})
        ;
        `);
    } catch (e){
      log.error("SQL","addUserToGroup sql error",e);
      return null
    } finally {
      client.release();
    }
    return {userid,groupid}
  }

  async removeUserFromGroup({userid,groupid}){
    log.debug("SQL","removeUserFromGroup",userid,groupid)
    const client = await this.pool.connect();
    try {
      await client.query(sql`
        DELETE FROM member2group
        WHERE userid = ${userid}
        AND groupid = ${groupid}
        ;
        `);
    } catch (e){
      log.error("SQL","addUserToGroup sql error",e);
      return null
    } finally {
      client.release();
    }
    return true
  }

  async getUserConfig(user){
    const client = await this.pool.connect();
    try {
      let config = await client.query(sql`
        SELECT name, email, public_contact_details FROM users
        WHERE id =${user}
        ;
        `);
      return config.rows[0]
    } catch (e){
      log.error("SQL","updateUserConfig sql error",e);
      return null
    } finally {
      client.release();
    }
  }

  async updateUserConfig(config, user){
    const client = await this.pool.connect();
    try {
      let oldconfig = await client.query(sql`
      SELECT name, email, public_contact_details FROM users
      WHERE id = ${user}
      ;
      `);

      log.trace("SQL",'updateUserConfig','oldconfig',oldconfig.rows[0])
      config = Object.assign(oldconfig.rows[0],config);
      log.trace("SQL",'updateUserConfig','newconfig',config)

      await client.query(sql`
        UPDATE users
        SET 
          name = ${config.name},
          email = ${config.email},
          public_contact_details = ${config.public_contact_details}
        WHERE id = ${user}
        ;
        `);
    } catch (e){
      log.error("SQL","updateUserConfig sql error",e);
      return null
    } finally {
      client.release();
    }
    return true
  }


  async changeUserPassword(userRequesting,newpassword){
    if (!newpassword || newpassword.length < 8){
      throw new errors.InvalidError('password needs to be at least 8 characters long');
    }
    const salt = await bcrypt.genSalt(parseInt(process.env.SALTROUNDS)) // do this client side?
      .catch((e)=> {
        log.error("SQL","changeUserPassword could not create salt",e)
        throw new errors.DingsdaError('could not changeUserPassword while salting');
    })
    const hashedpassword = await bcrypt.hash(newpassword, salt)
      .catch((e)=> {
        log.error("SQL","changeUserPassword could not create hash",e)
        throw new errors.DingsdaError('could not changeUserPassword while hashing');
    })
    try{
      const res = await this.pool.query(`
      UPDATE users
      SET hashedpassword = '${hashedpassword}'
      WHERE id = '${userRequesting}'
      ;
      `);
      log.trace("SQL","changeUserpassword return",res);
      if (res) return true
    } catch(e){
      log.error('SQL','changeUserPassword','could not change password:',e)
      throw new errors.DingsdaError('could not changeUserPassword');
    }  
  }

  async addUser({ id, email, name, password }) {
    const client = await this.pool.connect();
    if (!password || password.length < 8){
      throw new errors.InvalidError('password needs to be at least 8 characters long');
    }
    
    let salt = await bcrypt.genSalt(parseInt(process.env.SALTROUNDS)) // do this client side?
      .catch((e)=> {
        log.error("SQL","addUser could not create salt",e)
        throw new errors.DingsdaError('could not create User while hashing');
      })
    let hashedpassword = await bcrypt.hash(password, salt)
      .catch((e)=> {
        log.error("SQL","addUser could not create hash",e)
        throw new errors.DingsdaError('could not create User while hashing');
      })
    try {
      await client.query(sql`
        INSERT INTO users
        VALUES (${id},${email},${name},${hashedpassword})
        ;
        `);
    } catch (e){
      log.error("SQL","addUser sql error",e);
      return null
    } finally {
      client.release();
    }
    await this.addUserToGroup(id,'*').catch((e)=>{
      log.error('SQL','error adding User to group in signup',e);
    }); 
    return {id,email,name}
  }

  
  async addLocation({
    id, name, geojson, client,
  }) {
    let releaseClientAfterQueryReturn = false;
    if (!id) { id = uuidv4(); }
    if (!client) {
      client = await this.pool.connect();
      releaseClientAfterQueryReturn = true;
    }

    if (!name || !geojson) throw new errors.InvalidError('cannot add location without at least name and geojson');
    log.debug("SQL", "addLocation geoJSON",geojson,"name",name);
    let parsedGeoJSON = JSON.parse(geojson);
    if (parsedGeoJSON.type != "Feature") throw new errors.InvalidError(`only geojson of Type Feature are allowed. ${parsedGeoJSON.type} is not allowed.`)
    let geometry = parsedGeoJSON.geometry
    if (!geometry) throw new errors.InvalidError('geojson provided does not have a geometry.')
    await client.query(sql`
            INSERT INTO places
            VALUES
            (${id},ST_GeomFromGeoJSON(${geometry}),${name})
            ;
        `)
      .catch((e) => {
        log.error('sql', 'ERROR creating location', e.detail);
        throw e;
      })
      .finally(()=>{
        if (releaseClientAfterQueryReturn){
          client.release()
        }
      });
    return id;
  }

  async getItemIfVisible2UserNoClient(id,user) {
    const client = await this.pool.connect();
    try {
      const res = await this.getItem({ id, client, username:user});
      log.debug("SQL","getItemIfVisible2UserNoClient",res)
      return res;
    }catch(e){
      log.error("SQL","getItemIfVisible2UserNoClient error",e)
    }
    finally {
      client.release();
    }
  }

  async getItemNoClient(id) { // CHECK IF NEEDED, USED at all
    const client = await this.pool.connect();
    try {
      const res = await this.getItem({ id, client, username:"ADMIN"});
      return res;
    } finally {
      client.release();
    }
  }

  async getItem({ id, client, username}) {
    log.debug("sql",'getITEM',username)
    if (!client) throw new errors.InvalidError('need client to get item');
    let res;
    if ( username === "ADMIN"){
      log.trace('SQL','getItem as ADMIN')
      res = await client.query(sql`
      select * from all_items -- this is a view!

      where id = $1
      `, [id])
        .catch((e) => {
          log.error('ERROR getting item as ADMIN', e);
          throw e;
        });
    }
    else if (username){
      log.info("SQL",`selecting item ${id} if visible to user ${username}`)
      // TODO: change this by fetching NOT via all_items, but fetch item data starting from items table
      res = await client.query(sql`
      select * from all_items -- this is a view!
      
      where 
      id = $1 
      AND
      ( owned && (SELECT array_agg(id) FROM getUserAliases($2)) -- check if owned by user or group user is part of
        OR visible2 && (SELECT array_agg(id) FROM getUserAliases($2)) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
        OR possessedby && (SELECT array_agg(id) FROM getUserAliases($2)) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
      )
      `,[id,username])
        .catch((e) => {
          log.error('ERROR getting item', e);
          throw e;
        });
    }
    else{
      res = await client.query(sql`
      select * from all_items -- this is a view!
      
      where id = $1
      AND
      ( 
        visible2 && ARRAY['*'] -- item is public
      )
            `, [id])
        .catch((e) => {
          log.error('ERROR getting item', e);
          throw e;
        });
    }

    log.trace("SQL","getItem","got item",res.rows[0])
   
    return res.rows[0];
  }

  async deleteItem({id,user,deleteReservations=true}){
    if (!id) throw new errors.InvalidError('cannot delete item without id');
    if (!user) throw new errors.InvalidError('cannot delete item without user');

    await this.checkItemPermissionsAndDenyOrEdit({id},user,true);

    const client = await this.pool.connect().catch((e)=>log.error('SQL','error fetching pool',e));
    try {
      await client.query('BEGIN');
      if(deleteReservations){
        await client.query('DELETE FROM item2reservation WHERE itemid = $1',[id]);
      }
      // TODO: CHECK first if item is container of other items. If so: throw error or delete those items too
      await client.query('DELETE FROM item2place WHERE itemid = $1', [id]);
      await client.query('DELETE FROM owners2item WHERE itemid = $1', [id]);
      await client.query('DELETE FROM item2possessors WHERE itemid = $1', [id]);
      await client.query('DELETE FROM item2place WHERE itemid = $1', [id]);
      await client.query('DELETE FROM tags2item WHERE itemid = $1', [id]);
      //await client.query('DELETE FROM sizes2item WHERE itemid = $1', [id]);
      await client.query('DELETE FROM itemvisible2users WHERE itemid = $1', [id]);

      await client.query('DELETE FROM items WHERE id = $1', [id]);
      await client.query('COMMIT');
      log.info("SQL",`item deleted:`,id)
      return id
    } catch (e) {
      await client.query('ROLLBACK');
      if (e.code == '23503'){
        log.warn("SQL", "cant delete item because foreign-key relationship blocks deletion:",e.constraint);
        if (e.constraint == "items_insideof-items_id"){
          throw new errors.ForeignKeyRelationError("there a still items inside of this item. cannot delete filled containers")
        }
        if (e.constraint == "item2reservation_itemid-items_id"){
          throw new errors.ForeignKeyRelationError("there a still reservations on this item. cannot delete until those are cleared or deleteReservations flag set to true")
        }
        throw new errors.InvalidError("foreign key constraint")
      }
      log.info("SQL","could not delete item",e)
      throw e
    } finally {
      client.release()
    }
  }

  async moveItems(items,lastmodifiedby){
    let moved = [];
    for (let item of items){
      let result = await this.moveItem(item,lastmodifiedby).catch((err)=>{
        log.error("API","items/_move",`could not move item with id ${item.id}`,err.name,err.message)
        return err
      });
      if (!(result instanceof Error)){
        result = { id:item.id, moved: true};
      }
      else{
        result = { id:item.id, moved: false, reason: {error:result.name ,message: result.message}}
      }
      moved.push(result);
    }
    return moved
  }

  async moveItem({id,atLocation,insideof},lastmodifiedby){
    return await this.editItem({id,atLocation,insideof},lastmodifiedby)
  }
  
  async editItems(items,lastmodifiedby){
    let updated = [];
    for (let item of items){
      let result = await this.editItem(item,lastmodifiedby).catch((err)=>{
        log.error("API","items/_update",`could not update item with id ${item.id}`,err.name,err.message)
        return err
      });
      if (!(result instanceof Error)){
        result = { id:item.id, updated: true};
      }
      else{
        result = { id:item.id, updated: false, reason: {error:result.name ,message: result.message}}
      }
      updated.push(result);
    }
    return updated
  }

  async itemEditableByUser(itemid,user){
    log.debug("SQL","itemEditableByUser",itemid,user);
    if (user === "ADMIN") return "FULL"
    const res = await this.pool.query(sql`
      SELECT 
      (
        CASE 
          WHEN 
          (
            owned && (SELECT array_agg(id) FROM getUserAliases($1))
          ) 
          THEN 
          'FULL' 
          WHEN
          (
            possessedby && (SELECT array_agg(id) FROM getUserAliases($1))
          )
          THEN
          'MOVE'
          ELSE
          'NONE' 
        END) 
      AS
      role from all_items 
      WHERE id = $2
    `,[user,itemid])
      .catch((e)=> {
        log.error("COULD NOT GET ROLE, ERROR IN SQL",e)
        throw e
      });
    log.debug("SQL","GETROLE",res.rows)
    return res.rows[0].role
  }

  /**
   * SQL Item Middleware Function: Checks permissions for item and limit/edit item accordingly to permissions given, 
   * unless denyInsteadOfEdit flag is set.
   * In that case: will check and throw Forbidden Error in case of item includes key-value pairs not explicitly allowed for this role
   * @param {*} item 
   * @param {*} username 
   * @param {*} denyInsteadOfEdit 
   * @returns {*} editedItem
   */
  async checkItemPermissionsAndDenyOrEdit(item,username,denyInsteadOfEdit=false) {
    log.trace("CHECKING ITEM PERMISSIONS");
    let role = await this.itemEditableByUser(item.id, username);
    if (!role)
      throw 'role/permissions could not be clarified.';
    
    let permittedForMove = ["id", "insideof", "atLocation"];
    
    log.debug("SQL", "ROLE", role);
    
    let newitem = {};

    if (role !== 'FULL') {
      if (role == "NONE") {
        throw new errors.ForbiddenError("user is not authorized to edit/update at all. Contact item's owners to change that");
      }
      // If not every key of inputitem is permitted for role "MOVE" deny:
      else if (role == "MOVE" 
      //&& !Object.keys(item).every((e) => permittedForMove.includes(e)) //CONTINUE HERE: why does this not work for move?
      ) {
        log.debug("all keys of item",Object,"");
        if (!denyInsteadOfEdit){
          permittedForMove.forEach((key)=>{
            newitem[key] = item[key];
          })
          log.debug("FILTERED ITEM TO BE USED FOR UPDATE",newitem);
          return newitem
        }
        throw new errors.ForbiddenError("user is not authorized to edit/update item but only MOVE. Contact item's owners to change that");
      }
      else {
        throw new errors.ForbiddenError("UNKNOWN USER ROLE/PERMISSION.");
      }
    }

    return item
  }

  /**
   * SQL Item Middleware Function: Checks permissions for reservation and limits/edits reservation accordingly to permissions given, 
   * unless denyInsteadOfEdit flag is set.
   * In that case: will check and throw Forbidden Error in case of reservation including key-value pairs not explicitly allowed for this role
   * 
   * @param {*} reservation 
   * @param {*} username 
   * @param {*} denyInsteadOfEdit 
   * @returns {*} editedReservation
   */
  async checkReservationPermissionsAndDenyOrEdit(reservation,username,denyInsteadOfEdit=false){
    log.trace("CHECKING RESERVATIONS PERMISSIONS");
    let newReservation = reservation
    try{
      // first: get diff between reservation as is and new reservation.
      let oldReservation = await this.getReservationById(reservation.id,username);
      if (oldReservation.length < 1) throw new errors.ForbiddenError(`reservation ${reservation.id} does not exist`);
      oldReservation = oldReservation[0];
      log.trace('SQL','checkReservationPermissionsAndDenyOrEdit', `oldReservation:`,oldReservation);
      const datesChanged = oldReservation.reserved[0] != newReservation.reserved[0] || oldReservation.reserved[1] != newReservation.reserved[1];
      // if: diff contains dates: check if reservation owned by user (confirmedby) or group user is part of
      log.trace('SQL','checkReservationPermissionsAndDenyOrEdit', `datesChanged:`,datesChanged);
      // if: diff contains items: check for every item if user has rights to edit items.
      return newReservation
    }catch(e){
      log.error('SQL','checkReservationPermissionsAndDenyOrEdit',e)
      throw new errors.ForbiddenError(`Not allowed. Reservation ${reservation.id} cannot be edited by this user.`)
    }
    
  }


  /**
   * SQL Item Middleware Function: Checks if given user is equal or is member of all usergroups/users given in userIds Array.
   * Will throw ForbiddenError if does not pass check. otherwise returns true
   * 
   * This is intended to prevent people to (accidentally or maliciously) add 3rd parties as owners. It is not the best system.
   * The only way with this to add other users as owners is to add/edit the item as the user of a group not as the group.
   * 
   * @param {array} userIds
   * @param {*} userToBeChecked 
   * @param {*} 
   * @returns {boolean} 
   */
  async checkUserGroupPermissionsOrDeny({userIds,userToBeChecked, forbiddenIds}) {
    log.debug("SQL","CHECKING IF USER IS PART OF GROUPS",userIds,userToBeChecked);
    
    const res = await this.pool.query( sql`
      SELECT * FROM getUserAliases(${userToBeChecked}) 
    `).catch((e)=> {throw e});

    if (!res.rows){ throw new errors.DingsdaError("sth went wrong while getting userAliases in checkUserGroupPermissionsOrDeny")}

    let userAliasesOfUserChecked = res.rows.map((a)=>a.id);
    
    let isInAllGroups = true;

    for (let id of userIds){
      //log.trace("SQL","checkUserGroup....",id, userAliasesOfUserChecked)
      if (forbiddenIds && forbiddenIds.includes(id)){ 
        throw new errors.ForbiddenError(`id ${id} is forbidden in this case`);
      }
      
      if (id !== userToBeChecked && !userAliasesOfUserChecked.includes(id)){
        isInAllGroups = false;
        if (!isInAllGroups) {
          throw new errors.ForbiddenError("user '"+id+`' is not permitted because ${userToBeChecked} is not part of ${id}. If ${id} is part of ${userToBeChecked} :Try log in as '${id}' to add yourself as owner!`);
        }
        break;
      }
    }    
    return true //passed test
  }

  // TODO: check if pictures were updated and need to be deleted from file server
  // TODO: check if numbers are numbers else throw good error
  // TODO: ADD Permissions Check to AddItem as well but with denial flag set true
  async editItem(
    inputitem,
    lastmodifiedby) {

    log.debug("SQL","editItem",inputitem, lastmodifiedby)

    if (!inputitem.id) throw new errors.InvalidError('cannot edit item without id');
  
    inputitem = await this.checkItemPermissionsAndDenyOrEdit(inputitem,lastmodifiedby);
    
    log.debug("SQL","editItem after permissionCheck",inputitem)

    let { 
      id, name, owners, insideof, atLocation, visible2, inPossessionOf, count, weight, description, tags, price, value, pictures, width, depth, length, countryoforigin, billingreference, invoicedate
    } = inputitem;

    if (atLocation && insideof) throw new errors.InvalidError('item can only have location OR insideof')
    if (insideof === null && !atLocation) throw new errors.InvalidError('item MUST have either location OR insideof')
    const client = await this.pool.connect();
    try {
      await client.query('BEGIN');
      log.trace("SQL","getting old item with id:",id);
      const item = await this.getItem({ id, client,username:"ADMIN" }); // get original item from all_items view as ADMIN
      const olditem = cloneDeep(item);
      log.trace("SQL","old item:",item);
      
      if (name || insideof !== undefined || count || weight || description || price || value || length || depth || width || pictures) {
        log.debug("SQL","merging...",length,width,depth)
        Object.assign(item,  // merge to be updated fields into item
          name && {name}, // if name not null nor false nor undefined, merge in // TODO: check if this is safe
          insideof && {insideof},
          count !== null && count !== undefined && {count},
          weight !== null && count !== undefined && {weight},
          {description},
          lastmodifiedby && {lastmodifiedby},
          price !== null && count !== undefined && {price},
          value !== null && count !== undefined && {value},
          pictures && {pictures},
          length !== null && count !== undefined && {length},
          depth !== null && count !== undefined && {depth},
          width !== null && count !== undefined && {width},
          {countryoforigin},//countryoforigin && {countryoforigin},
          {billingreference},
          {invoicedate},
        );
        log.debug('SQL','merge step 1 done.',item)
        if (insideof === null){
          log.info('SQL','+++++ clearing insideof for item ++++++', id)
          item.insideof = null;
        }
        log.info("SQL","new item:",item);
        // update items table with merged updated item
        await client.query(sql`
                UPDATE items
                SET 
                    name = ${item.name},
                    insideof = ${item.insideof},
                    count = ${item.count},
                    weight = ${item.weight},
                    price = ${item.price},
                    value = ${item.value},
                    description = ${item.description},
                    lastmodified = CURRENT_TIMESTAMP,
                    lastmodifiedby = ${item.lastmodifiedby},
                    pictures = ${item.pictures},
                    length = ${item.length},
                    width = ${item.width},
                    depth = ${item.depth},
                    countryoforigin = ${item.countryoforigin},
                    billingreference = ${item.billingreference},
                    invoicedate = ${invoicedate}
                WHERE
                    id = ${id}
                `).catch((e) => {
          log.error('sql', 'ERROR updating item. items table could not be updated', e);
          throw e;
        });

        if (insideof && item.insideof != null){ // unlink from geolocations for item is now inside of another item
          await client.query(sql`
            DELETE FROM item2place
              WHERE  
              itemid = ${id}
            ;
            `).catch((e) => {
            log.error('sql', 'ERROR emptying item after setting insideof. table item2place could not be updated', e);throw e})
        }
      }
      
      console.log("before owners");
      if (owners) {
        console.log("has owners");
        if (!Array.isArray(owners)) { owners = [owners]; }
        if (owners.length == 0) throw new errors.ForbiddenError("items MUST have at least one owner")

        let addedOwners = owners;
        let oldowners = olditem.owned;
        log.trace("addedOwners",owners);
        log.trace("oldowners",oldowners)
        addedOwners = addedOwners.filter((o)=>!oldowners.includes(o)); // filter out old owners
        if (owners.length > 0){
          // check if owners are controlledByUser so that nobody assigns strangers as owners.
          await this.checkUserGroupPermissionsOrDeny({userIds:addedOwners,userToBeChecked:lastmodifiedby,forbiddenIds:["*"]});

          // delete all entries of this item in owners2item table
          // then write new owners into it
          log.trace("deleting owners and replacing with:",owners)
          await client.query(`
                  DELETE FROM owners2item
                  WHERE
                      itemid = '${id}'
                  ;
                  INSERT INTO owners2item
                  ${this.users2itemQueryFromOwnerArray(owners, id)}
                  `).catch((e) => {
            log.error('sql', 'ERROR updating item. users2item table could not be updated', e);
            throw e;
          });
        }

      }

      if (atLocation) {
        
        // update item2place table AND/OR create place
        if (atLocation && insideof) throw new errors.InvalidError('only atLocation OR insideof are allowed');
        await this.setItemToLocation(id, atLocation, client).catch((e) => {
          log.error('sql', 'ERROR updating item. location table could not be updated', e);
          throw e;
        });

        if (item.insideof && atLocation){ // empty insideof after new location was associated with item
          await client.query(sql`
          UPDATE items
          SET 
              insideof = NULL
          WHERE
              id = ${id}
          ;
          `).catch((e) => {
            log.error('sql', 'ERROR emptying item after setting to new location. items table could not be updated', e); throw e})
        }
      }

      if (inPossessionOf) {
        // update item2possessors table
        await this.setItemToPossessors({ id, possessors: inPossessionOf, client }).catch((e) => {
          log.error('sql', 'ERROR updating item. item2possessors table could not be updated', e);
          throw e;
        });
      }

      if (visible2) {
        log.debug("SQL","EDITITEM visible2",visible2)
        // update itemsvisible2users table
        await this.makeItemVisibleToUsers({ itemId: id, users: visible2, client });
      }

      if (tags) { 
        // delete all links from item to tags
        await client.query(sql`
          DELETE FROM tags2item
          WHERE
            itemid = ${id}
          ;
          `).catch((e) => {
          log.error('sql', 'ERROR emptying tags before setting new tags. table tags2item could not be updated', e); throw e})
        for (let tag of tags){
          await this.addTag({tag,client}).catch((e)=>{
            log.error("SQL","could not add tag because error",e)
            throw e
          });
          await this.linkTagToItem({itemId:id, tag:tag, client}).catch((e)=>{
            log.error("SQL","could not LINK tag to item because error",e)
            throw e
          });
            
        }   
      }

      return await client.query('COMMIT');
    } catch (e) {
      await client.query('ROLLBACK');
      log.error('sql', `${'#'.repeat(20)}\nCOULD NOT EDIT ITEM\n${'#'.repeat(20)}`);
      log.error('sql','editItem error',e)
      throw e;
    } finally {
      client.release();
    }

  }


  async addItem({
    name, owners, insideof, atLocation, visible2, inPossessionOf, count, weight, description, tags, pictures, value, price, length, width, depth, countryoforigin, billingreference, invoicedate
  },lastmodifiedby) {
    const client = await this.pool.connect();
    try {
      await client.query('BEGIN');

      if (!name){
        throw new errors.InvalidError('cannot create item without name');
      }
      if (!owners || owners.length < 1){
        throw new errors.InvalidError('cannot create item without owners');
      }
      if (!insideof && !atLocation){
        throw new errors.InvalidError('cannot create item without location or storage');
      }

      const id = uuidv4();

      if (!count) {count = 1}
      if (!weight) {weight = 0}
      if (!length) {length = 0}
      if (!width) {width = 0}
      if (!depth) {depth = 0}
      if (!value) {value = 0}
      if (!price) {price = 0}
      
      // CREATING item
      await client.query(sql`
                INSERT INTO items
                VALUES (${id},${name},${description},${count},${weight},${insideof},CURRENT_TIMESTAMP,${lastmodifiedby},${pictures},${value},${price},${length},${width},${depth},${countryoforigin},${billingreference},${invoicedate})
                ;
            `).catch((e) => {
        log.error('sql', 'ERROR creating item to begin with', e.detail);
        throw new errors.InvalidError(e);
      });

      // check if owners are controlledByUser so that nobody assigns strangers as owners.
      await this.checkUserGroupPermissionsOrDeny({userIds:owners,userToBeChecked:lastmodifiedby,forbiddenIds:["*"]});

      await this.linkItemToOwners({ itemId: id, ownerIds: owners, client });

      if (atLocation && insideof) throw new Error('only atLocation OR insideof are allowed');

      if (atLocation) {
        await this.setItemToLocation(id, atLocation, client);
      }

      if (visible2) {
        await this.makeItemVisibleToUsers({ itemId: id, users: visible2, client });
      }

      if (inPossessionOf) {
        await this.setItemToPossessors({ id, possessors: inPossessionOf, client });
      }

      if (tags) { // CONTINUE HERE: check if tags can be created or only linked by ENV
        for (let tag of tags){
          if (tag === null){continue}
          await this.addTag({tag,client}).catch((e)=>{
            log.error("SQL","could not add tag because error",e)
            throw e
          });
          await this.linkTagToItem({itemId:id, tag:tag, client}).catch((e)=>{
            log.error("SQL","could not LINK tag to item because error",e)
            throw e
          });
        }    
      }

      await client.query('COMMIT');
      return {success:"ok",id}
    } catch (e) {
      await client.query('ROLLBACK');
      log.error('sql', `${'#'.repeat(20)}\nCOULD NOT ADD ITEM\n${'#'.repeat(20)}`,e);
      throw e;
    } finally {
      client.release();
    }
  }

  // FIXME: if new location: check if old location associated with item and if so delete unless other items are associated with it as well?
  // ALTERNATIVE to FIXME: have server do regular tidy up routines deleting unlinked locations.
  async setItemToLocation(id, atLocation, client) {
    if (atLocation) {
      if (atLocation.id) {
        return await this.linkItemToLocation({ itemId: id, locationId: atLocation.id, client });
      }
      if (atLocation.new) {
        // create Location
        let newlocationId;
        
        newlocationId = await this.addLocation({ name: atLocation.new.name || await helpers.getLocationNameFromGeoJSON(atLocation.new.geography) || `location of item ${id}`, geojson: JSON.stringify(atLocation.new.geography), client });      
        // afterwards link location via place_uuid (item2place entry)
        return await this.linkItemToLocation({ itemId: id, locationId: newlocationId, client });
        //  QUESTION: do we do this in js or better in SQL cte?
      }

      throw new errors.InvalidError('atLocation invalid');
    }
  }

  ////// LINK ITEMS //////

  async linkItemToLocation({ itemId, locationId, client }) {
    await client.query(sql`
            DELETE FROM item2place
            WHERE itemid = ${itemId}
            ;
        `).catch((e) => {
      log.error('sql', 'ERROR linkingItemToLocation. Could not clean up item2place', e.detail);
      throw e;
    });
    const res = await client.query(sql`
            INSERT INTO item2place
            VALUES (${itemId},${locationId})
            ;
        `).catch((e) => {
      log.error('sql', 'ERROR linkingItemToLocation', e.detail);
      throw e;
    });
    return res;
  }

  async linkItemToOwners({ itemId, ownerIds, client }) {
    if (!Array.isArray(ownerIds)) { ownerIds = [ownerIds]; }

    const res = await client.query(`
            INSERT INTO owners2item
            ${this.users2itemQueryFromOwnerArray(ownerIds, itemId)}
            ;`).catch((e) => {
      log.error('sql', 'ERROR linking Item to Owners', e);
      throw new Error(e.detail);
    });
    return res;
  }

  async makeItemVisibleToUsers({ itemId, users, client }) {
    if (!Array.isArray(users)) { users = [users]; }
    log.debug("SQL","makeItemVisibleToUsers",users,itemId)

    await client.query(sql`
            DELETE FROM itemvisible2users
            WHERE
            itemid = ${itemId}
            ;
        `).catch((e) => {
      log.error('sql', 'ERROR cleaning visible2users table', e);
      throw new Error(e.detail);
    });
    if (users.length == 0) {
      log.trace('SQL','makeItemVisibleToUsers','users empty. will not add new links')
      return null
    }
    function v(users, itemId) {
      log.debug("SQL","V start",users,itemId)
      let output = 'VALUES ';
      for (const i in users) {
        output += `('${itemId}','${users[i]}')`;
        if (i < users.length - 1) output += ',\n';
      }
      log.debug("SQL","V",output)
      return output;
    }

    const res = await client.query(`
            INSERT INTO itemvisible2users
            ${v(users, itemId)}
            ;`).catch((e) => {
      log.error('sql', 'ERROR linking Item to Visiblitiy of users', e);
      if (e.code == '23503'){
        throw new errors.ForbiddenError(e.detail)
      }
      throw new Error(e.detail);
    });
    return res;
  }

  async linkItemTopossessors({ itemId, users, client }) {
    if (!Array.isArray(users)) { users = [users]; }
    if (users.length == 0){
      return true
    }
    return await client.query(`
            INSERT INTO item2possessors
            ${this.users2itemQueryFromOwnerArray(users, itemId)}
            ;`).catch((e) => {
      log.error('sql', 'ERROR linking item to possessors', e);
      throw e;
    });
  }

  async setItemToPossessors({ id, possessors, client }) {
    // delete all links to possessors
    // then add new possessors
    await client.query(`
            DELETE FROM item2possessors
            WHERE
                itemid = '${id}'
            ;`)
      .catch((e) => {
        log.error('sql', 'ERROR updating item. items2possessors table could not be cleaned up', e);
        throw e;
      });
    await this.linkItemTopossessors({ itemId: id, users: possessors, client })
      .catch((e) => {
        log.error('sql', 'ERROR updating item. items2possessors table could not be updated', e);
        throw e;
      });
  }

  async findItemsWithinRadius(latitude,longitude,radius,user){
    // TODO: implement based on cte of same name but use geojson, get also contained items and optimize query time.
    // query times might be optimizable by doing sequential sql queries (first location search, then filter?) (maybe not all_items but its own view?)
    // another optimization for query time might be to set the use_spheroid flag to false: http://postgis.net/docs/ST_DWithin.html
    log.info("SQL",`geosearch for lat: ${latitude} long: ${longitude} with radius ${radius} meters for user ${user}`)
    let res;
    if(user !== "*"){
      res = await this.pool.query( sql`
        SELECT i.* FROM findItemWithinRadius(ST_MakePoint( ${latitude},${longitude})::geography,${radius}) as found
        
        INNER JOIN (
          SELECT * FROM all_items
            WHERE 
            ( owned && (SELECT array_agg(id) FROM getUserAliases(${user})) -- check if owned by user or group user is part of
              -- OR visible2 && (SELECT array_agg(id) FROM getUserAliases()) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
              OR possessedby && (SELECT array_agg(id) FROM getUserAliases(${user})) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
            )
          ) as i
        ON i.id = found.id
        `
      ).catch((e)=> {throw e});
    }
    else{
      res = await this.pool.query( sql`
      SELECT i.* FROM findItemWithinRadius(ST_MakePoint( ${latitude},${longitude})::geography,${radius}) as found
      
      INNER JOIN (
        SELECT * FROM all_items
          WHERE 
           visible2 && (SELECT array_agg(id) FROM getUserAliases(${user})) 
          
        ) as i
      ON i.id = found.id
      `
      ).catch((e)=> {throw e});
    }
    
    return res.rows
  }

  async getItemIdsAndTagsByName({
    name,tags,containers,area,user,
    max_weight,min_weight,
    max_length,min_length,
    max_width,min_width,
    max_depth,min_depth,
    max_value,min_value,
    max_price,min_price,
    availableFrom,availableTill,
    lastItemOnPage
  }){ 
    if (!name) throw new error.InvalidError("need name to search to getItemIdsAndTagsByName");

    let searchArea = null;
    if (area){
      if (area.type != "Feature") throw new errors.InvalidError(`only geojson of Type Feature are allowed. ${parsedGeoJSON.type} is not allowed.`);
      let geometry = area.geometry
      if (!geometry) throw new errors.InvalidError('geojson provided does not have a geometry.')
      searchArea = geometry;
    }
    log.trace('SQL',"searchArea geographic",searchArea)

    let paginationQuery = this.getPaginationQuery(lastItemOnPage);

    let whereTagsQuery = null;
    if(tags){
      log.trace("tags present",tags)
      whereTagsQuery = format(`
      AND 
      ARRAY[%L] <@ tags
      `,tags)
      log.trace("whereTagsQuery",whereTagsQuery)
    }

    let whereDimensionQuery = null; 
    if (min_depth || max_depth || min_length || max_length || min_width || max_width){
      whereDimensionQuery = `
      AND 
      DEPTH BETWEEN ${min_depth ? min_depth : 0} AND ${max_depth ? max_depth : 2147483647}
      AND
      LENGTH BETWEEN ${min_length ? min_length : 0} AND ${max_length ? max_length : 2147483647}
      AND
      WIDTH BETWEEN ${min_depth ? min_depth : 0} AND ${max_depth ? max_depth : 2147483647}
      `
      log.trace('SQL','getItemIdsAndTagsByName dimension filter query:',whereDimensionQuery)
    }

    let whereExtraFilterQuery = null;
    if (min_weight || max_weight || min_price || max_price || min_value || max_value){
      whereExtraFilterQuery = `
      AND 
      WEIGHT BETWEEN ${min_weight ? min_weight : 0} AND ${max_weight ? max_weight : 2147483647}
      AND
      PRICE::numeric BETWEEN ${min_price ? min_price : 0} AND ${max_price ? max_price : 2147483647}
      AND
      VALUE::numeric BETWEEN ${min_value ? min_value : 0} AND ${max_value ? max_value : 2147483647}
      `
      log.trace('SQL','getItemIdsAndTagsByName other filters query:',whereExtraFilterQuery)
    }

    // TODO: FORBID searchAreas larger than 50km
    let whereLocationsQuery = null;
    if(containers){
      log.trace("location filter present",containers)
      whereLocationsQuery = format(`
      AND 
      ARRAY[%L] && "location.insideofArray"
      `,containers)
      log.trace("whereLocationsQuery",whereLocationsQuery)
    }
    // first: get items by name and user 
    let querytext = format(`
      SELECT DISTINCT i.* FROM all_items i
      WHERE name
      ILIKE
      '%s'
      %s
      %s
      %s
      %s
      AND
      %s
      `,name,whereTagsQuery,whereLocationsQuery,whereDimensionQuery,whereExtraFilterQuery,this.visibleTo(user));

    // if searchArea is defined wrap the whole SQL query into a join with to look only for items inside of the area
    if (searchArea){
      log.info('SQL','getItemIdsAndTagsByName','has geographic searchArea defined and will search only in its bounds')
      querytext = format(`
      SELECT i.* FROM findItemsWithIntersectingGeography(ST_GeomFromGeoJSON(
        '
        %s
        '
        )) as found
        
      INNER JOIN (
        ${querytext}
      )  as i
      ON i.id = found.id
      `,searchArea)
    }

    querytext = format(`
      %s
      
      %s
      `,querytext,paginationQuery)

    log.trace('SQL','getItemIdsAndTagsByName query',querytext)
    
    const res = await this.pool.query(querytext)
      .catch((e)=> {throw e});
    
    // if availableFrom or availableTill are present: get a list of all item ids that are reserved in that timeframe:
    let whereAvailableQuery = null;
    if (availableFrom || availableTill){
      log.trace("availability filters present",availableFrom, availableTill);
      whereAvailableQuery = format(`
      SELECT i.* FROM all_items i
      LEFT JOIN availability a ON a.itemid=i.id -- ONLY IF AVAILABILTY FILTER PRESENT
      WHERE
      (
        a.reserved IS NOT NULL -- ONLY IF AVAILABILTY FILTER PRESENT
        AND 
        (
          	a.reserved && '[${availableFrom ? availableFrom : '1970-01-01'},${availableTill ? availableTill : '2999-12-30'})'::tsrange -- reservation is overlap with filtered range ==> items are reserved
        --  NOT a.reserved && '[${availableFrom ? availableFrom : '1970-01-01'},${availableTill ? availableTill : '2999-12-30'})'::tsrange -- reservation is NOT overlapping with filtered range ==> items are available
        )
        AND -- ONLY IF EXCLUDE PAST RESERVATIONS
        (
          upper(a.reserved)::Date >= NOW() - INTERVAL '1 day' -- exclude past reservations
        )
      )
      `);

      let reservedItems = await this.pool.query(whereAvailableQuery)
      .catch((e)=> {throw e});
      console.log("reservedItems:",reservedItems.rows);
      
      res.rows = res.rows.filter((foundItem)=>{// if el.id found in any of the reservedItems: item will be sorted out
        console.log(foundItem.id,foundItem.name);
        return !reservedItems.rows.some((blockedItem)=>{
          return blockedItem.id == foundItem.id
        })
      })
      log.trace("items filtered from reservations",res.rows);
    }

    let { items, tags:tagsFound } = this.filterItemsAndTagsConsideringPagination(res,lastItemOnPage);

    return {items:items, tags:tagsFound}
  }

  async removeItemsFromReservation({reservationid,items,confirmedby}){
    log.trace('SQL','removeItemsFromReservation',confirmedby);
    if (!reservationid || !items || !confirmedby) throw new errors.ForbiddenError("params missing");
    if (!Array.isArray(items)) items = [items];
    for (let item of items){
      console.log(item.id);
      if (!await this.isOwnerOfItem(item.id,confirmedby,true)){
        throw new errors.ForbiddenError("user is not authorized to edit item's reservations.")
      }
    }
    for (let item of items){
      const res = await this.pool.query(format(`
      DELETE FROM item2reservation
      WHERE itemid = '${item.id}';
    `))
    }

    return {removed: items.length}
  }

  async addItemsToReservation({reservationid,items,confirmedby}){
    log.trace('SQL','addItemsToReservation',confirmedby);
    if (!reservationid || !items || !confirmedby) throw new errors.ForbiddenError("params missing");
    if (!Array.isArray(items)) items = [items];
    for (let item of items){
      console.log(item.id);
      if (!await this.isOwnerOfItem(item.id,confirmedby,true)){
        throw new errors.ForbiddenError("user is not authorized to add this item.")
      }
    }
    for (let item of items){
      try{
        const res = await this.pool.query(format(`
        INSERT INTO item2reservation
        VALUES ('${item.id}','${reservationid}')
      `))
      }
      catch(e){
        log.debug('SQL','addItemsToReservation','could not add item',item.id,e)
        throw new errors.ForbiddenError(`cannot add item "${item.name}"\n\nreason: ${e.message}"`)
      }
    }

    return {added: items.length}
  }

  async editReservation({reservationid,reservation,confirmedby}){
    log.info("SQL","editReservation","by",confirmedby);
    log.trace("SQL","new reservation:",reservationid,reservation);

    reservation = await this.checkReservationPermissionsAndDenyOrEdit(reservation,confirmedby);

    const client = await this.pool.connect();
    // CONTINUE HERE: test permission check (in DB atm)

    try {
      await client.query('BEGIN');
      // update reservation notes and by etc
      const queryreservations = format(`
        UPDATE reservations
        SET 
        notes = '${reservation.notes}',
        confirmedby = '${reservation.confirmedby}',
        reserved = tsrange('${reservation.reserved[0]}','${reservation.reserved[1]}','[]')
        ${reservation.reservedfor ? ",reservedfor = '"+reservation.reservedfor+"'":''}
        WHERE id = '${reservationid}';
      `)
      log.trace('SQL','editReservation queryreservation',queryreservations);
      await client.query(queryreservations)
      // update all item reservations linked to reservation in availability
      // let res = await client.query(format(`
      // UPDATE availability
      // SET
      // reserved = tsrange('${reservation.reserved[0]}','${reservation.reserved[1]}','[]')
      // WHERE reservationid = '${reservationid}'
      // RETURNING *;
      // `))
      await client.query('COMMIT');
    }
    catch(e){
      await client.query('ROLLBACK');
      if (e.message?.includes('some items are already reserved')){
        console.log(e.name,e.message,e.detail);
        throw new errors.ForbiddenError(`${e.message}\n${e.detail.replace(/{/g,'').replace(/}/g,'').replace(/,/g,',\n')}`)
      }
      log.error('SQL', `could not edit reservation`,e);
      throw e;
    } finally {
      client.release();
    }
    
    return {updated: true}
  }

  async getUserGroupsByUser(userid){
    const res = await this.pool.query(sql`
      SELECT array_agg(distinct groupid) FROM member2group
        WHERE userid = ${userid}
        AND
        groupid != '*'
    `,[userid])
      .catch((e)=> {
        log.error("COULD NOT GET GROUPS, ERROR IN SQL",e)
        throw e
      });
    
    return res.rows[0]["array_agg"]
  }

  async getMembersByGroup(userid){
    const res = await this.pool.query(sql`
      SELECT array_agg(distinct userid) FROM member2group
        WHERE groupid = ${userid}
    `,[userid])
      .catch((e)=> {
        log.error("COULD NOT GET GROUPS, ERROR IN SQL",e)
        throw e
      });
    
    return res.rows[0]["array_agg"]
  }


  ////////// SEARCH AND REPLACE /////////
  // CONTINUE HERE: TODO: refactor and document!!!! 
  // TODO: move out of postgresql into admin sql plugin to postgresql.

  /**
   * searches as ADMIN user (must exist in userDB and can do everything, so beware!) for 
   * a text pattern (Postgres Search Pattern as used with LIKE command) and then will do 
   * a replacement of a regex pattern that should be replaced with the replacer.
   * 
   * usually used in the rare case that a user changed 
   * @param {*} urlpattern url pattern as postgres text pattern as used with LIKE 
   * @param {*} urlReplacePattern url replace pattern as regex
   * @param {*} replacer the text to replace the urlReplacePattern with
   * @returns 
   */
  async searchAndReplacePictureURLs(urlpattern,urlReplacePattern,replacer){
    //log.info("searchAndReplacePictureURLs","urlpattern",urlpattern)
    const res = await this.pool.query(format(`
    SELECT id, pictures, picSubQuery.pic
    FROM (
        SELECT * ,unnest(pictures) pic
        FROM items) AS picSubQuery
    WHERE picSubQuery.pic ILIKE '${urlpattern}'
    `))
      .catch((e)=> {
        log.error("sql","error in searchAndReplacePictureURLs",e)
        throw e
      });

    for (let item of res.rows){
      //console.log(item);
      let newpictures = [];
      newpictures = item.pictures.map((url)=>{
        if(url === item.pic){ 
          return url.replace(urlReplacePattern,replacer) 
        }
        return url
      })
      let newitem = {
        id: item.id,
        pictures: newpictures
      }
      //console.log(newitem)
      let done = await this.editItem(newitem,"ADMIN");
      //log.info("done",done)
    }
    
    return true//res.rows
  }

  /////////////////////////////
  ///////// PRINTER ///////////

  async createPrinter(id,user){
    const client = await this.pool.connect();
    try {
      await client.query('BEGIN');
      
      // CREATING printer
      await client.query(sql`
        INSERT INTO printers
        VALUES
        (${id});
            `).catch((e) => {
        log.error('sql', 'ERROR creating printer to begin with', e.detail);
        throw new errors.InvalidError(e);
      });

      await client.query(sql`
        INSERT INTO printers2users
        VALUES
        (${id},${user});
            `).catch((e) => {
        log.error('sql', 'ERROR linking printer to users', e.detail);
        throw new errors.InvalidError(e);
      });

      await client.query('COMMIT');
      return {success:"ok",id}
    } catch (e) {
      await client.query('ROLLBACK');
      log.error('sql', `${'#'.repeat(20)}\nCOULD NOT ADD PRINTER\n${'#'.repeat(20)}`,e);
      throw e;
    } finally {
      client.release();
    }
  }

  async deletePrinter(id){
    const client = await this.pool.connect();
    try {
      await client.query('BEGIN');
      
      // DELETING printer
      await client.query(sql`
        DELETE FROM printers
        WHERE id = ${id}
            `).catch((e) => {
        log.error('sql', 'ERROR deleting printer to begin with', e.detail);
        throw new errors.InvalidError(e);
      });

      await client.query(sql`
        DELETE FROM printers2users
        WHERE printerid = ${id}
            `).catch((e) => {
        log.error('sql', 'ERROR deleting printer to users entry', e.detail);
        throw new errors.InvalidError(e);
      });

      await client.query('COMMIT');
      return {success:"ok",id}
    } catch (e) {
      await client.query('ROLLBACK');
      log.error('sql', `${'#'.repeat(20)}\nCOULD NOT DELETE PRINTER\n${'#'.repeat(20)}`,e);
      throw e;
    } finally {
      client.release();
    }
  }

  async getPrintsByPrinterId(id,user){
    const res = await this.pool.query(sql`
      SELECT prints FROM printers

      JOIN printers2users 
        ON printerid = printers.id
      
      INNER JOIN (SELECT *  FROM getUserAliases(${user}) ) AS alias 
        ON alias.id = printers2users.userid

      WHERE printers.id = ${id}
      ;
    `)
      .catch((e)=> {
        log.error("SQL","COULD NOT GET GROUPS, ERROR IN SQL",e)
        throw e
      });
    
    return res.rows[0]
  }

  async setPrinterQueue(id,prints){
    const res = await this.pool.query(sql`
      UPDATE printers
      SET prints = ${prints}
      WHERE id = ${id}
      ;
    `)
      .catch((e)=> {
        log.error("SQL","COULD NOT SET PRINT QUEUE",e)
        throw e
      });
    
    return res
  }

  async reserveItems({items,from,till,reservedFor,notes,reservationid,name},confirmedBy){
    if (!reservationid) reservationid=uuidv4();
    let reservationsMade = []
    const client = await this.pool.connect().catch((e)=>{
      log.error('SQL','error fetching pool',e);
      client.release()
    });
    log.debug("SQL","reservation","created client",this.pool)
    try {
      await client.query('BEGIN');
      for (let item of items){
        log.debug("SQL","reserveItems","item no.",reservationsMade.length+1)
        let reservation = await this.reserveItem({item,from,till,reservedFor,notes,reservationid,name},confirmedBy,client);
        reservationsMade.push(reservation);
      }
      await client.query("COMMIT");
    }
    catch (e) {
      await client.query('ROLLBACK');
      log.error("SQL","reserveItems","could not reserveItems",e);
      throw e;
    }
    finally {
      client.release()
    }
    return reservationsMade;
  }

  async reserveItem({reservationid,item,from,till,reservedFor,notes,name},confirmedBy,client) {
    log.debug('SQL','reserveItem request',item,from,till,reservedFor,confirmedBy,notes)
    
    if (!await this.isOwnerOfItem(item.id,confirmedBy)){
      throw new errors.ForbiddenError("item does not exits or user is not authorized to reserve item.")
    }
    // TODO: check format of timestamps and correct if necessary

    let createReservationQuery = '';
    const existsRes = await this.pool.query(`SELECT id FROM reservations WHERE id='${reservationid}'`)
    if (!existsRes?.rowCount > 0){
      createReservationQuery = format(`
      INSERT into reservations VALUES
      (${reservationid ? "'"+reservationid+"'" : ''},'${notes}',${reservedFor ? "'"+reservedFor+"'" : 'null'},${name ? "'"+name+"'" : 'null'},${confirmedBy ? "'"+confirmedBy+"'" : 'null'},tsrange('${from}','${till}','[]'));
    `)
    }
    const query = format(`
    ${createReservationQuery}
    INSERT into item2reservation VALUES 
    ('${item.id}'${reservationid ? ",'"+reservationid+"'" : ''})
    ;
    `);
    log.trace('SQL','reserveItem query',query)
    
    if(!client){
      client = await this.pool.connect().catch((e)=>log.error('SQL','error fetching pool',e));
    }
    
    await this.pool.query(query)
      .catch((e)=> {
        log.error("SQL","error in reserveItem",e,"error detail:",e.detail,"e.message:",e.message)
        if (e.message.includes("already reserved")){
          throw new errors.ForbiddenError(
            `item ${item.name || item.id} is already reserved 
between ${e.detail?.split('[')[2].split(']')[0].replace(',',' and ').replace(/"/g,'')}. 
Please double check your reservation dates or delete the active reservation.`
          )
        }
        if (e.message?.includes("range lower bound must be less than or equal to range upper bound")){
          throw new errors.ForbiddenError(
            `"from" cannot be after "till"`
          )
        }
        throw e
      });
    let res = await this.pool.query(`SELECT * FROM all_reservations WHERE reservationid='${reservationid}' AND itemid='${item.id}'`)
    log.trace("SQL","reserveItem","reservation made:",query,res)
    if (!res || !res.rows || res.rows.length < 1){
      throw new errors.DingsdaError("could not reserve item for unknown reason");
    }
    return res.rows[0]
  }

  async getReservationById(id,userRequesting){
    log.debug('SQL','getReservationById',id,userRequesting)
    let query = format(`
      SELECT * FROM complete_reservations_with_itemidArray
      WHERE id = '${id}'
      AND ARRAY [confirmedby] && (SELECT array_agg(id) FROM getUserAliases('${userRequesting}'))
    `);
    const res = await this.pool.query(query)
      .catch((e)=> {
        log.error("SQL","error in getReservationById",e);
        throw e
    });
    if (res.rows){
      for (let i in res.rows){
        try{ res.rows[i].reserved = JSON.parse(res.rows[i].reserved) }
        catch(e){log.error("SQL","getReservations",`could not parse reserved for ${JSON.stringify(res.rows[i])}`)}
      }
      return res.rows
    }
    throw new errors.DingsdaError("unknown error in getReservationById")
  }
  
  async getReservations(
    userRequesting,
    {onlyOwnedItems,excludePastReservations,searchReservationsText,searchReservationsName,onlyOwnedReservations}
    ){
      log.trace("SQL","getReservations",{ userRequesting,
        onlyOwnedItems,excludePastReservations,searchReservationsText,searchReservationsName,onlyOwnedReservations})
    let onlyOwnedItemsQueryPart = this.visibleTo(userRequesting,'i')
    let onlyOwnedReservationsQuery = `AND ARRAY[c.confirmedby] && (SELECT array_agg(id) FROM getUserAliases('${userRequesting}'))`
    
    // TODO LATER: add more options. Add timesearch
    // CONTINUE HERE: TODO: test onlyOwnedItems, onlyOwnedReservations, searchReservationsName, searchReservationsText, excludePastReservations in all combinations
    let query = format(`
    SELECT DISTINCT c.*, ARRAY[to_char(lower(a.reserved),'YYYY-MM-DD HH24:MI:SS'),to_char(upper(a.reserved),'YYYY-MM-DD HH24:MI:SS')] reserved FROM complete_reservations_with_itemidArray c
    JOIN availability a ON (c.id = a.reservationid)

    ${onlyOwnedItems == true ? `
    JOIN all_items i ON (
      i.id = a.itemid
      AND `
      +onlyOwnedItemsQueryPart+
    `
    ) -- IF onlyOwnedItems 
    `  : ''}

    WHERE	
    ( 
      TRUE
      ${onlyOwnedReservations ? onlyOwnedReservationsQuery : ''} 
    )
    ${excludePastReservations ? "AND upper(a.reserved)::Date >= NOW() - INTERVAL '1 day'": ''}
    ${searchReservationsText ? 'AND c.notes ILIKE \'%'+searchReservationsText+'%\' OR c.name ILIKE \''+searchReservationsText+'%\''+' OR c.id = \''+searchReservationsText+'\'' : ''}
    ${searchReservationsName ? 'AND c.name ILIKE \'%'+searchReservationsName+'%\'' : ''}
    `)
    log.trace("GETRESERVATIONS query",query)
    const res = await this.pool.query(query)
      .catch((e)=> {
        log.error("SQL","error in getReservations",e)
        throw e
      });
    //res.rows.forEach((r)=>r.reserved = JSON.parse(r.reserved)) // FIXME: this should happen in sql query
    return res.rows
    throw new errors.DingsdaError("unknown error in getReservationsByItem")
  }

  async getReservationsByItem(itemId,userRequesting,excludePastReservations=true){
    log.trace('SQL','getReservations for item',itemId);
    let fields = '*';
    if (!await this.isOwnerOfItem(itemId,userRequesting)){
      fields = 'id, itemid, reserved, created'
    }
    // let query = format(`
    // SELECT ${fields}
    // FROM availability WHERE itemid = '${itemId}';
    // `

    let query = format(` 
    SELECT ${fields} 
    FROM(
        -- get AllReservations of an item with all days per reservation aggregated into array days:

        SELECT itemid, id, array_agg(days) as days, reserved, notes, created, reservedfor,confirmedby FROM (

          SELECT id, (generate_series(ft.fro,ft.till,'1 day'::interval)) as days, itemid, reserved, notes, created, reservedfor,confirmedby from(
            SELECT id,lower(reserved) fro, upper(reserved) till, itemid, reserved, notes, created, reservedfor,confirmedby FROM all_reservations av
            WHERE itemid = '${itemId}'
            ${excludePastReservations ? "AND upper(reserved)::Date >= NOW() - INTERVAL '1 day' -- TODO: TEST!!!": ''}
            GROUP BY id, itemid, reserved, fro, till, notes, created, reservedfor,confirmedby
          ) as ft
        GROUP BY id, fro, till, itemid, reserved, notes, created, reservedfor,confirmedby
        ) as ft2
        GROUP BY id, itemid, reserved, notes, created, reservedfor,confirmedby
    ) as ft3
    `
    )
    const res = await this.pool.query(query)
      .catch((e)=> {
        log.error("SQL","error in getReservations",e)
        throw e
      });
    if (res.rows){
      for (let i in res.rows){
        try{ res.rows[i].reserved = JSON.parse(res.rows[i].reserved) }
        catch(e){log.error("SQL","getReservations",`could not parse reserved for ${JSON.stringify(res.rows[i])}`)}
      }
      return res.rows
    }
    throw new errors.DingsdaError("unknown error in getReservationsByItem")
  }

  async deleteReservation(reservationId,confirmedBy){
    log.debug('SQL','deleteReservation request',reservationId,confirmedBy);

    if (!await this.isOwnerOfReservation(reservationId,confirmedBy)){
      throw new errors.ForbiddenError("User is not authorized to delete reservation.\n\nHINT: If you own items inside this reservation you can remove them.")
    }

    let query = format(`
      DELETE FROM item2reservation WHERE reservationid = '${reservationId}';
      DELETE FROM reservations WHERE id = '${reservationId}';
    `)
    const res = await this.pool.query(query)
      .catch((e)=> {
        log.error("SQL","error in deleteReservation",e)
        throw e
      });
    log.debug("SQL","deleteReservation","reservation deleted:",query,res)
    if (res.rows){
      return {success:"ok",reservationId,rowsDeleted:res.rowCount}
    }
    return {success:"false",reservationId}
  }

  async getAllDaysOfReservation({itemid},perRerservation=true){
    log.debug('SQL','getAllDaysOfReservation',itemid);
    
    let query = format(`
      SELECT id, array_agg(days) as days FROM (

        SELECT id, (generate_series(ft.fro,ft.till,'1 day'::interval)) as days from(
          SELECT id,lower(reserved) fro, upper(reserved) till  FROM availability av
          WHERE itemid = '0364108b-bc15-47b6-b4b3-bf49924865a1'
          GROUP BY id, reserved, fro, till
        ) as ft
      GROUP BY id, fro, till
      ) as ft2
      GROUP BY id
    `);
    
    if (!perRerservation){
      query = format(`
      SELECT array_agg(generate_series) days FROM (

        SELECT (generate_series(ft.fro,ft.till,'1 day'::interval)) from(
          SELECT lower(reserved) fro, upper(reserved) till  FROM availability av
          WHERE itemid = '${itemid}'
        ) as ft
      
      ) as ft2
      `);
    }
    
    const res = await this.pool.query(query)
      .catch((e)=> {
        log.error("SQL","error in getAllDaysOfReservation",e)
        throw e
      });
    if (res.rows){
      return res.rows
    }
  }

  async removeUnusedPlaces(){
    const res = await this.pool.query(sql`
    -- DELETE places that are not linked to any items
    DELETE FROM places WHERE id NOT IN (
      SELECT placeid from item2place
    )
    `)
    return res
  }

  //////// HELPER /////////////

  async isOwnerOfItem(id,user){
    let isowner = await this.checkItemPermissionsAndDenyOrEdit({id},user,true)
      .catch((e)=>{log.warn('SQL','isOwnerOfItem check not succesful:',e);});
    log.debug('SQL','ISOWNEROFITEM',isowner);
    if (isowner){
      return true
    }
    return false
  }

  async isOwnerOfReservation(id,user){
    let query = format(`
    SELECT EXISTS(
      SELECT * FROM reservations WHERE id = '${id}' AND ARRAY[confirmedby] && (SELECT array_agg(id) FROM getUserAliases('${user}'))
    )
    `)
    const res = await this.pool.query(query)
      .catch((e)=> {
        log.error("SQL","error in isOwnerOfReservation",e)
        throw e
      });
    console.log("isOwnerOfReservation",user,res.rows[0]);
    if (res.rows[0] && res.rows[0].exists == true ){
      return true
    }
    return false
  }

  users2itemQueryFromOwnerArray(ownerIds, itemId) {
    let output = 'VALUES ';
    for (const i in ownerIds) {
      output += `('${ownerIds[i]}','${itemId}')`;
      if (i < ownerIds.length - 1) output += ',\n';
    }
    return output;
  }

  visibleTo(user,itemTableAlias=''){
    if (itemTableAlias) itemTableAlias = itemTableAlias+'.';
    return `
    ( 
      ${itemTableAlias}owned && (SELECT array_agg(id) FROM getUserAliases('${user}')) -- check if owned by user or group user is part of
      OR ${itemTableAlias}visible2 && (SELECT array_agg(id) FROM getUserAliases('${user}')) -- optional if allvisible should be shown. usually not relevant unless in combination with searches
      OR ${itemTableAlias}possessedby && (SELECT array_agg(id) FROM getUserAliases('${user}')) -- optional if borrowed items should be shown as well. usually wanted to be included. but might be excluded 
    )
    `
  }

  getTagsFromAllItemsList(res) {
    let tags = res.rows.reduce((result, item) => {
      if (item.tags) { 
        result = result.concat(item.tags); 
        result = [...new Set(result)]; // remove duplicates
      }
      //console.log(result);
      return result;
    }, []).map((tag) => { return { tag: tag }; });
    log.info('SQL', 'getItems tags', tags);
    return tags;
  }

};
