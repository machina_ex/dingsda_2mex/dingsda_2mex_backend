const sql = require('sql-template-strings');
const { Pool } = require('pg');

let pool = new Pool({
  user: process.env.pguser,
  host: process.env.pghost,
  database: 'test',//process.env.pgdatabase,
  password: process.env.pgpassword,
  port: 5432,
});

pool.on('error', (err) => {
  console.log('Unexpected error on idle client', err);
  process.exit(-1);
});


async function updateItems(){
  const res = await pool.query(
    sql`
    SELECT name FROM items
    `
  ).catch((e)=>{throw ("error",e)});

  if (!res.rows ){
    throw ("error, no rows returned")
  }
  return res.rows
}

(async () => {
  let output = await updateItems();
  console.log(output);
})()