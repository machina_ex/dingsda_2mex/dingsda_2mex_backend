
class DingsdaError extends Error {
  constructor(...params) {
    super(...params)
    this.name = this.constructor.name
  
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DingsdaError)
    }
  }
}
  

/**
 * The json data provided does not validate, something is missing or the wrong type
 */
class InvalidError extends DingsdaError {
  constructor(...params) {
    super(...params)
      
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, InvalidError)
    }
  }
}

/**
 * The json data provided does not validate, something is missing or the wrong type
 */
class ForbiddenError extends DingsdaError {
  constructor(...params) {
    super(...params)
      
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ForbiddenError)
    }
  }
}

/**
 * The json data provided does not validate, something is missing or the wrong type
 */
class ForeignKeyRelationError extends DingsdaError {
  constructor(...params) {
    super(...params)
      
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ForeignKeyRelationError)
    }
  }
}


module.exports = {
  DingsdaError,
  InvalidError,
  ForbiddenError,
  ForeignKeyRelationError
}
  