const path = require("path")

// Expression; the class is anonymous but assigned to a variable
const pluginEventHandler = class {
    constructor(sql,pluginSources=[]) {
      this.pluginSources = pluginSources;
      console.log("this.pluginSources",this.pluginSources);
      this.plugins = [];
      this.sql = sql;
      for (const pluginSource of this.pluginSources){
        console.log("pluginEventHandler initializing plugin",pluginSource);
        const plugin = new pluginSource(null,this.sql);
        if (plugin.disabled){
            console.log(`plugin ${plugin.name} disabled`);
            continue 
        } 
        try{
            plugin.init()
            this.plugins.push(plugin)
        }
        catch(e){
            log.error('pluginHandler','could not initialize plugin',plugin.name,e)
        }
      }
    }

    triggerPluginEvent(event){
        try{
            log.trace("pluginEvent",event);
            for (const plugin of this.plugins){
                if (plugin.topic == event.domain){
                    plugin.handler(event)
                }
                else if (Array.isArray(plugin.topic) && plugin.topic.includes(event.domain) ){
                    plugin.handler(event)
                }
            }
        }catch(e){
            console.log("triggerPluginEvent","error",e);
        }
        
    }
}

module.exports = {
    pluginEventHandler
}