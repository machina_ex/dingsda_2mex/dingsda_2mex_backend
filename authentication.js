const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
dotenv.config();


class Auth {
  constructor() {

  }
}

class NoAuth extends Auth{
  constructor(default_user) {
    super()
    this.default_user = default_user
    global.log.info(0, "Authentication disabled")
  }


  authenticate(req, res, next) {
    req.user = this.default_user
    next()
  }
}

/**
 * @param {Object|Array} user - list of authorized users
 */
class TokenAuth extends Auth {
  constructor(db) {
    super()

    this.db = db;
    this.user = []
        
    global.log.info(0, "Token Authentication enabled", process.env.TOKEN_SECRET)
  }

  /**
     * authentication middleware function for express routing.
     * 
     * respond with www-authenticate necessity. Only pass if reqest header contains valid authentication
     * 
     * parse login and password from headers
     * match against user list or single login user
     * 
     * User example:
     * 
     *  "user": [
     *    {
     *      "login": "myuser"
     *    }
     *  ],
     * 
     * Verify if login and password are set and match credentials of an existing user
     * 
     * With help by this Post: https://stackoverflow.com/questions/23616371/basic-http-authentication-with-node-and-express-4/33905671#33905671 
     * 
     * @todo add Set-Cookie with Session Auth or create Session Token or similar
     * 
     * @param {Object} req - request Object
     * @param {Object} res - response Object
     * @param {function} next - express next function
     * @returns {undefined} Returns prematurely if authentication failed and skips next request handlings
     */
  async authenticate(req, res, next) {
    let auth = {}
    let token = null;

    let allowedRoutes = ["signup","uploaded","captcha","forgotpassword","tidyupImages","tidyupPlaces"]
    if (allowedRoutes.includes(req.path.split('/')[1]) ){ // TODO: check if needs more security checks
      global.log.trace("Authentication ","allowed route",req.path)
      req.user = auth
      return next()
    }

    function isPublicItemRequest(){
      if (req.method === "GET" && req.path.split('/')[1] === "items" && req.path.split('/')[2] && !req.path.split('/')[3]){
        // could be a request to a public item. check if item exists and is public:
        global.log.debug("authentication","authenticate will check if public item with id:",req.path.split('/')[2])
        return true
      }
    }
  
    const authHeader = req.headers['authorization'];

    if (req.query.user && req.query.password){
      let login = req.query.user
      let password = req.query.password
      // redundant (used in authHeader the same way.)  TODO: refactor!
      let hashedPassword = await this.db.getUserPasswordHashByUsername(login)
        .catch((e)=>{
          log.error("Authentication error",e,"user was:",login)
          res.status(401).send({name:'AuthenticationError',message:'user does not exist'})
        })

      let passwordCorrect = await bcrypt.compare(password, hashedPassword)
        .catch(()=>{log.warn("Authentication","password does not match hashes")})

      if (!login || !password || !passwordCorrect ) {
        log.warn(0, "Authentication passwordhash compare result",passwordCorrect)
        log.warn(0, `Authentication failed for ${req.method} request to ${req.originalUrl} with headers: 'xxx', user: '${login}' and pw: 'XXXXXXXXX'`)
        res.status(401).send({name:'AuthenticationError',message:'Authentification enabled. See Readme on how to change authentification for your dingsda server.'})
        return
      }

      auth.login = login;
    }
    else if(authHeader && authHeader.startsWith("Basic")){
      // it's Username and Password! 

      log.debug("Authentication attempt with Basic Auth using username and password");
      let b64auth = authHeader.split(' ')[1];
      //log.debug("Auth Headers",b64auth,Buffer.from(b64auth, 'base64').toString());
      const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')

      let hashedPassword = await this.db.getUserPasswordHashByUsername(login)
        .catch((e)=>{
          log.error("Authentication error",e,"user was:",login)
          res.status(401).send({name:'AuthenticationError',message:'user does not exist'})
          return
        })

      if (!hashedPassword) return

      let passwordCorrect = await bcrypt.compare(password, hashedPassword)
        .catch(()=>{log.warn("Authentication","password does not match hashes")})

      if (!login || !password || !passwordCorrect ) {
        log.warn(0, "Authentication passwordhash compare result",passwordCorrect)
        log.warn(0, `Authentication failed for ${req.method} request to ${req.originalUrl} with headers: 'xxx', user: '${login}' and pw: 'XXXXXXXXX'`)
        res.status(401).send({name:'AuthenticationError',message:'Authentification enabled. See Readme on how to change authentification for your dingsda server.'})
        return
      }

      auth.login = login;
    }
    else if(authHeader && authHeader.startsWith("Bearer")){
      // it's a TOKEN!
      log.debug("Authentication" ,"attempt with Token");
      token = authHeader.split(' ')[1];
      //log.debug("Authentication","Token",token)
      if(token == null){
        res.status(401).send({name:'AuthenticationError',message:'Authentification enabled. Use username and password, access token or see Readme on how to change authentification for your dingsda server.'})
        return
      }

      try {
        const decodedtoken = jwt.verify(token, process.env.TOKEN_SECRET);
        log.debug("Authentication","TOKEN USER VERIFIED",decodedtoken)
        auth.login = decodedtoken.login // token is valid and we can fill auth.user
      }
      catch (e) { 
        log.error('Authentication','Token Auth Error',e.message,e.name);
        if (e.name == "TokenExpiredError"){
          res.status(401).send({name:'AuthenticationError',message:'Auth Token expired.'})
          return
        }
        res.status(401).send({name:'AuthenticationError',message:'Authentification Auth Token invalid.'})
        return
      }     
    }
    else if (isPublicItemRequest()){
      req.user = {login:'*'};
      return next();
    }
    else {
      // No Auth Header Or missformed Auth Header
      res.status(401).send({name:'AuthenticationError',message:'No Auth Header Or missformed Auth Header'})
      return
    }

    let newtoken = jwt.sign(
      {login:auth.login},
      process.env.TOKEN_SECRET,
      {expiresIn:"3d"}
    )
    res.set('Authorization', "Bearer " + newtoken); // set Token into response header
    res.set('Access-Control-Expose-Headers', 'Authorization') // make header accessable
    //console.log("created new Bearer token:", newtoken);

    req.user = auth
    next()
  }
  
}

module.exports = {
  TokenAuth:TokenAuth,
  NoAuth:NoAuth
}