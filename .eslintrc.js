module.exports = /*{
  extends: ['airbnb', 'plugin:node/recommended'],
  rules: {
    camelcase: 'off',
    'no-use-before-define': ['error', { functions: false, classes: false }],
    'no-restricted-syntax': 'off',
    'class-methods-use-this': 'off',
  },
  ignorePatterns: [],
  env: {
    node: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 8,
  },
}; */
{
  root: true,
  env: {
    node: true,
    es6: true
  },
  extends: [
    "eslint:recommended",
  ],
  parserOptions: {
    "ecmaVersion": 2020
  },
  rules: {
    "indent": [2,2],
  },
  ignorePatterns: ["generated/**"],
};
